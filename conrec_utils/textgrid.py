import conrec_utils.spec as spec

class TextBlock:
    def width(self):
        raise NotImplementedError("width")

    def height(self):
        raise NotImplementedError("height")

def normalize(x):
    if isinstance(x, TextBlock):
        return x
    elif spec.str_spec.coll().is_valid(x):
        return LinesTextBlock(x)
    elif spec.str_spec.is_valid(x):
        return LinesTextBlock.from_string(x)
    else:
        return LinesTextBlock.from_string(str(x))

class LinesTextBlock(TextBlock):
    def __init__(self, lines):
        self._lines = lines
        self._width = 0
        self._height = len(lines)
        for line in lines:
            self._width = max(self._width, len(line))

    def width(self):
        return self._width

    def height(self):
        return self._height

    def get(self, cell_size, position):
        w, h = cell_size
        x, y = position
        if 0 <= y and y < len(self._lines):
            line = self._lines[y]
            if 0 <= x and x < len(line):
                return line[x]

    def from_string(s):
        return LinesTextBlock(s.split("\n"))

class PaddedTextBlock(TextBlock):
    def __init__(self, inner, paddings):
        padding = paddings.get("padding", 0)
        horizontal = paddings.get("horizontal", padding)
        vertical = paddings.get("vertical", padding)
        self._inner = inner
        self._left = paddings.get("left", horizontal)
        self._right = paddings.get("right", horizontal)
        self._top = paddings.get("top", vertical)
        self._bottom = paddings.get("bottom", vertical)

    def width(self):
        return self._left + self._inner.width() + self._right

    def height(self):
        return self._top + self._inner.height() + self._bottom

    def get(self, cell_size, position):
        w, h = cell_size
        x, y = position
        return self._inner.get(
            (w - self._left - self._right, h - self._top - self._bottom),
            (x - self._left, y - self._top))

class TextGrid:
    def __init__(self):
        self._cell_map = {}

    def set_string(self, x, y, s):
        assert(isinstance(s, str))
        self.set_block(x, y, LinesTextBlock.from_string(s))

    def set_lines(self, x, y, lines):
        self.set_block(x, y, LinesTextBlock(lines))

    def set_block(self, x, y, block):
        assert(isinstance(block, TextBlock))
        self._cell_map[(x, y)] = block

    def _debug_ks(self, ks):
        x = False
        for a in ks:
            for b in ks:
                try:
                    if a < b:
                        x = True
                except Exception:
                    print(f"Failed comparison a < b:")
                    print(f"a = {a}")
                    print(f"b = {b}")
        
    def render_string(self):
        Xsizes = {}
        Ysizes = {}

        def regmax(dst, k, n):
            dst[k] = max(dst.get(k, 0), n)
        for ((x, y), e) in self._cell_map.items():
            regmax(Xsizes, x, e.width())
            regmax(Ysizes, y, e.height())

        def compute_offsets(size_map):
            dst = {}
            at = 0
            for k in sorted(size_map.keys()):
                dst[k] = at
                at += size_map[k]
            return (at, dst)
        
        #self._debug_ks(Xsizes.keys())
        #self._debug_ks(Ysizes.keys())


        (x_size, Xoffsets) = compute_offsets(Xsizes)
        (y_size, Yoffsets) = compute_offsets(Ysizes)
        
        dst = [" "]*(x_size*y_size)
        def index(x, y):
            return x_size*y + x
        for ((xkey, ykey), e) in self._cell_map.items():
            x_offset = Xoffsets[xkey]
            y_offset = Yoffsets[ykey]
            w = Xsizes[xkey]
            h = Ysizes[ykey]
            for y in range(e.height()):
                for x in range(e.width()):
                    c = e.get((w, h), (x, y))
                    if c != None:
                        dst[index(x_offset + x, y_offset + y)] = c
        return "\n".join(["".join(dst[index(0, y):index(x_size, y)]) for y in range(y_size)])

if False: # Try it out
    grid = TextGrid()

    grid.set_string(0, 0, "Hej")
    grid.set_string(1, 0, "Mu")
    grid.set_string(1, 1, "August\n!!!")

    print(grid.render_string())
    

    

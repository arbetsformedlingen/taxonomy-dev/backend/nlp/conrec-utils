from conrec_utils import common

class Interval(common.ComparableByTuple):
    def __init__(self, lower, upper):
        self.lower = lower
        self.upper = max(upper, lower)

    def __len__(self):
        return self.upper - self.lower

    def __getitem__(self, i):
        return self.lower + i

    def __eq__(self, x):
        if isinstance(x, Interval):
            return (self.lower == x.lower) and (self.upper == x.upper)

    def width(self):
        return self.upper - self.lower

    def slice_object(self, x):
        return x[self.lower:self.upper]
        
    def __repr__(self):
        return "Interval({:s}, {:s})".format(str(self.lower), str(self.upper))

    def to_tuple(self):
        return (self.lower, self.upper)

    def from_tuple(x):
        (l, u) = x
        return Interval(l, u)

    def get_comparison_tuple(self):
        return self.to_tuple()

    def to_range(self):
        return range(self.lower, self.upper)

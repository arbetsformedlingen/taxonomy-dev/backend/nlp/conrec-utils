import importlib
import conrec_utils.dataset_common as dataset_common
import conrec_utils.common as common
import conrec_utils.eval_common as ec
import conrec_utils.spec as spec
import conrec_utils.job_ads.config as config
from conrec_utils.json_utils import write_json
from conrec_utils.env import Env
from conrec_utils.time_utils import ProgressReporter
from jobtech_taxonomy_library_python.generated.defs import taxonomy_version_map
import conrec_utils.metric_accumulator as ma
import csv
import os

# For the original text2ssyk algorithm, see
# `text2ssyk.joblinks_classify` in the `text-2-ssyk` repository.

def make_ssyk2concept_map(taxonomy_version_map):
    dst = {}
    for tax in list(taxonomy_version_map.taxonomy_history()):
        for c in tax.concept_list():
            if c.type() == "ssyk-level-4":
                dst[c.ssyk_code_2012()] = c
    return dst

class Text2SsykTabularRenderer(ec.BasicTabularMetricRenderer):
    def row_key(self, metric_item):
        return ec.LabeledKey.str_combine([metric_item.aggregation_key()])

    def col_key(self, metric_item):
        return ec.LabeledKey.str_combine([ec.LabeledKey(metric_item.algorithm_key().pretty_string()),
                                          ec.LabeledKey(metric_item.metric_key())])

def separate_common_results(samples):
    per_id = common.group_by(lambda sample: sample.sample_id(), samples)
    algorithm_keys = {sample.algorithm_key() for sample in samples}
    algo_count = len(algorithm_keys)

    def make_acc():
        return {"samples": [], "ids": []}
    
    excluded = make_acc()
    included = make_acc()
    all_data = make_acc()
    
    for (sample_id, samples_with_id) in per_id.items():
        per_algo_key = common.group_by(lambda sample: sample.algorithm_key(), samples_with_id)
        target = excluded if len(per_algo_key) < algo_count else included
        for acc in [target, all_data]:
            acc["ids"].append(sample_id)
            acc["samples"].extend(samples_with_id)
    return {"included": included, "excluded": excluded, "all": all_data}


class Text2SsykReporter(ec.MetricTableClassificationReporter):
    def aggregation_keys(self, sample):
        return [ec.LabeledKey("overall", "Overall"),
                ec.LabeledKey(sample.true_label())]

    def metric_renderers(self):
        return [Text2SsykTabularRenderer()]

class PerAlgorithmReporter(ec.ClassificationReporter):
    def produce_report_for_algorithm(self, algo_key, classified_samples):
        raise NotImplementedError("produce_report_for_algorithm")
    
    def produce_report(self, classified_samples):
        per_algo = common.group_by(lambda x: x.prediction().algorithm_key(), classified_samples)
        for (algo_key, samples) in per_algo.items():
            self.produce_report_for_algorithm(algo_key, samples)
    
class Text2SsykRawReporter(PerAlgorithmReporter):
    def __init__(self, setup):
        self._setup = setup
        
    def produce_report_for_algorithm(self, algo_key, samples):
        filename = self._setup.algorithm_report_path(algo_key).joinpath("raw_results.csv")
        os.makedirs(filename.parent, exist_ok=True)
        corr_count = 0
        counter = 0

        # Produce a file with all the classifications
        with open(filename, "w") as f:
            writer = csv.writer(f)
            writer.writerow(["id", "True class", "Predicted class", "Correct?", "Percentage correct"])
            for x in samples:
                corr = x.true_label() == x.predicted_label()
                counter += 1
                if corr:
                    corr_count += 1
                writer.writerow([x.sample().sample_id(),
                                 self._setup.class_name(x.true_label()),
                                 self._setup.class_name(x.predicted_label()),
                                 "1" if corr else "0",
                                 round(100.0*float(corr_count)/counter)])

def format_perc(x):
    try:
        return f"{round(100*float(x))}%"
    except ValueError:
        return ""

class Text2SsykConfusionReporter(PerAlgorithmReporter):
    def __init__(self, setup, topn=20):
        self._setup = setup
        self._topn = topn
        self._gap = 3

    def produce_report_for_algorithm(self, algo_key, samples):
        samples_per_class = common.group_by(lambda x: x.true_label(), samples)
        ordered_classes = sorted(samples_per_class.items(), key=lambda kv: -len(kv[1]))
        
        filename = self._setup.algorithm_report_path(algo_key).joinpath("confusion.csv")
        with open(filename, "w") as f:
            writer = csv.writer(f)
            header = ["True class", "Sample count", "Order", "Predicted class", "Percentage"]
            gap = ["" for x in header]
            writer.writerow(header)
            for cl, class_samples in ordered_classes:
                cl_count = len(class_samples)
                freqs = common.frequencies(lambda sample: sample.predicted_label(), class_samples)
                top_cands = [(i, pred, count)
                             for (i, (pred, count)) in enumerate(sorted(freqs.items(), key=lambda kv: -kv[1]))
                             if i < self._topn or pred == cl]
                for (i, pred, count) in top_cands:
                    perc = format_perc(count/cl_count)
                    def wrap(x):
                        if pred == cl:
                            return f"[{x}]"
                        else:
                            return x
                    row = ["", "", i+1, wrap(self._setup.class_name(pred)), wrap(perc)]
                    if i == 0:
                        row[0] = self._setup.class_name(cl)
                        row[1] = cl_count
                    writer.writerow(row)
                for i in range(self._gap):
                    writer.writerow(gap)
            

class MetricTarget:
    def algorithm_key(self):
        raise NotImplementedError("algorithm_key")
    
    def report_for_class(self, cl, label, value):
        raise NotImplementedError("report_for_class")

    def report_overall(self, label, value):
        raise NotImplementedError("report_overall")

class PercentageTarget:
    def __init__(self, inner):
        self._inner = inner

    def algorithm_key(self):
        return self._inner.algorithm_key()
        
    def report_for_class(self, cl, label, value):
        self._inner.report_for_class(cl, label, format_perc(value))

    def report_overall(self, label, value):
        self._inner.report_overall(label, format_perc(value))

class MetricReporter:
    def key(self):
        return type(self).__name__
    
    def report(self, all_samples, target):
        raise NotImplementedError("report")
    
class CommonMetricReporter(MetricReporter):
    def report(self, all_samples, target0):
        target = PercentageTarget(target0)
        acc = ma.PairAccumulator()
        pairs = [(x.true_label(), x.predicted_label()) for x in all_samples]
        acc.accumulate_pairs(pairs)
        matrix_map = acc.binary_classification_matrix_map()
        agg = ma.BinaryClassificationMatrix.aggregate_common_metrics(matrix_map.values(), normalize=True)

        for cl, matrix in matrix_map.items():
            target.report_for_class(cl, "Recall", matrix.recall())
            target.report_for_class(cl, "Precision", matrix.precision())
            target.report_for_class(cl, "F1", matrix.f1())
        target.report_overall("Accuracy", acc.accuracy())
        target.report_overall("Recall", agg["recall"])
        target.report_overall("Precision", agg["precision"])
        target.report_overall("F1", agg["f1"])

class AlgorithmInfoReporter(MetricReporter):
    def __init__(self, setup):
        self._setup = setup
        
    def report(self, all_samples, target):
        k = target.algorithm_key()

        target.report_overall("Algorithm", k.name())
        target.report_overall("Version", k.version())

        repo = self._setup.algorithm_repository(k)
        metadata = repo.load_metadata()
        ks = sorted(metadata.keys())
        for k in ks:
            v = str(metadata[k])
            target.report_overall(f"@{k}", v)

class Text2SsykComparisonReporter(ec.ClassificationReporter):    
    def __init__(self, setup):
        self._setup = setup

    def metric_reporters(self):
        return [CommonMetricReporter(),
                AlgorithmInfoReporter(self._setup)]

    def produce_report(self, classified_samples):
        id_to_true_class_map = {}
        mentioned_classes = set()
        for sample in classified_samples:
            id_to_true_class_map[sample.sample().sample_id()] = sample.true_label()
            mentioned_classes.add(sample.true_label())
            mentioned_classes.add(sample.predicted_label())
        count_per_class = {k:len(group)
                           for k, group in common.group_by(
                                   lambda kv: kv[1],
                                   id_to_true_class_map.items()).items()}
        per_algo = common.group_by(lambda x: x.prediction().algorithm_key(), classified_samples)
        algo_index_map = {k:i for i, k in enumerate(sorted(per_algo.keys()))}

        print(algo_index_map)

        mreps = self.metric_reporters()
        assert(0 < len(mreps))

        cells = {}

        header_row = (0, 0)
        overall_row = (2, 0)
        
        def class_row(class_code):
            return (1, -count_per_class.get(class_code, 0))

        algoinds = {}
        metricinds = {}
        
        def algo_col(algo_key, metric_key, label):
            return (2,
                    algo_index_map[algo_key], #common.assign_index(algoinds, algo_key),
                    common.assign_index(metricinds, (metric_key, label)))

        def overall_algo_col(algo_key, metric_key):
            return (2, algo_index_map[algo_key], 0)

        rowinds = {}
        def overall_row(metric_label):
            return (2, common.assign_index(rowinds, metric_label))
        

        label_col = (0, 0, 0)
        count_col = (1, 0, 0)

        cells[(header_row, label_col)] = "Class"
        cells[(header_row, count_col)] = "Count"
        total_sample_count = 0
        for (c, n) in count_per_class.items():
            cells[(class_row(c), label_col)] = self._setup.class_name(c)
            cells[(class_row(c), count_col)] = f"{n}"
            total_sample_count += n

        cells[(overall_row("Count"), label_col)] = "Count"
        cells[(overall_row("Count"), count_col)] = f"{total_sample_count}"

        algo_keys = list(per_algo.keys())
        algo_keys.sort(key=lambda k: k.get_comparison_tuple())
        for algo_key in algo_keys:
            algo_samples = per_algo[algo_key]
            for mrep in mreps:
                class SubTarget(MetricTarget):
                    def algorithm_key(self):
                        return algo_key
                    
                    def report_for_class(self, cl, label, value):
                        ci = algo_col(algo_key, mrep.key(), label)
                        cells[(header_row, ci)] = f"{algo_key.pretty_string()}\n{label}"
                        cells[(class_row(cl), ci)] = value
                    def report_overall(self, label, value):
                        row = overall_row(label)
                        cells[(row, label_col)] = label
                        cells[(row, overall_algo_col(algo_key, label))] = value
                mrep.report(algo_samples, SubTarget())

        # Write it
        with open(self._setup.report_root_path().joinpath("comparison.csv"), "w") as f:
            writer = csv.writer(f)
            for row in common.dense_from_sparse(cells):
                writer.writerow(row)

def pprint_concept(c):
    return f"{c.preferred_label()} <{c.type()} {c.id()}>"

def jobad_ssyk_codes(taxvm, jobad):
    occupation_id = jobad["occupation"]["concept_id"]
    occupation = None
    if occupation_id is None:
        return []
    occupation = common.first(taxvm.concept_history(occupation_id))
    if occupation is None:
        raise RuntimeError(f"No occupation with id {occupation_id}")

    occgroups = common.take(1, taxvm.concept_history(jobad["occupation_group"]["concept_id"]))
    pred = common.unique_pred()
    return  [code
             for group in [occupation.broader(), occgroups]
             for b in group
             if b.type() == "ssyk-level-4"
             for code in [b.ssyk_code_2012()]
             if pred(code)]

class EvaluationSetup(ec.EvaluationSetup):
    def __init__(self):
        self._ssyk2concept_map = make_ssyk2concept_map(taxonomy_version_map)

    def is_valid_label(self, label):
        return label in self._ssyk2concept_map
        
    def name(self):
        return "text2ssyk"
    
    def class_name(self, label):
        c = self._ssyk2concept_map.get(label)
        if c is None:
            return f"Missing ({label})"
        else:
            return f"{c.preferred_label()} ({label})"

    def load_taxonomy_version_map(self):
        return taxonomy_version_map
    
    def jobads_setup(self):
        return config.Setup(config.DefaultConfig(self.env()))

    def result_repository_at_path(self, path):
        return ec.CsvClassificationResultRepository(path.joinpath("results.csv"))

    def evaluation_splitkeys(self):
        return [dataset_common.test0.label()]

    def sample_from_jobad(self, jobad):
        tax = self.load_taxonomy_version_map()
        codes = jobad_ssyk_codes(tax, jobad)
        text = jobad["description"]["text"]
        issues = []
        
        def issue(t, msg):
            issues.append({"type": t, "message": msg, "jobad": jobad})
            return (None, issues)

        if text is None:
            return issue("text-is-none", "Text is none")
        elif not(isinstance(text, str)):
            return issue("text-wrong-type", "Text has wrong type")
        elif len(codes) == 0:
            return issue("no-ssyk", "Missing SSYK code")
        elif 1 < len(codes):
            return issue("multiple-ssyk", "Multiple SSYK codes")
        label = codes[0]
        assert(isinstance(label, str))
        return (ec.Sample(jobad["id"], text, label).with_source_data(jobad), issues)

    def random_access(self):
        return self.jobads_setup().random_access().with_item_mapper(
            lambda jobad: self.sample_from_jobad(jobad)[0])
    
    def visit_jobads(self, splitks, visitor, message="Visiting jobads"):
        spec.str_spec.coll().check(splitks)
        jobads_setup = self.jobads_setup()
        was_progress = False

        class VisitedItem:
            def __init__(self, index, splitkey, data, wp):
                self.index = index
                self.splitkey = splitkey
                self.data = data
                self._stopped = False
                self.was_progress = wp

            def stop(self):
                self._stopped = True

        counter = 0
        for splitkey in splitks:
            jobad_access = jobads_setup.access_dataset(splitkey)
            with jobad_access as jobads:
                prog = ProgressReporter(jobad_access.count())
                for jobad in jobads:
                    item = VisitedItem(counter, splitkey, jobad, was_progress)
                    visitor(item)
                    was_progress = prog.end_of_iteration_message(f"{message} (splitkey= {splitkey})")
                    if item._stopped:
                        print("Stopped!")
                        return
                    counter += 1

    def visit_samples(self, splitks, visitor, message="Visiting samples", batch_size=None):
        batch = []
        def visit_jobad(item):
            jobad = item.data
            (sample, issues) = self.sample_from_jobad(jobad)
            if sample is None:
                return
            item.data = sample
            if batch_size is None:
                visitor(item)
            else:
                batch.append(item)
                if batch_size <= len(batch):
                    visitor(batch.copy())
                    batch.clear()
        self.visit_jobads(splitks, visit_jobad, message=message)
        if 0 < len(batch):
            visitor(batch)

    def export_samples_as_json(self, dst_filename, splitks):
        dst = []
        def visitor(item):
            sample = item.data
            dst.append({"data": sample.source_data(),
                        "label": sample.label(),
                        "id": sample.sample_id()})
        self.visit_samples(splitks, visitor)
        write_json(dst_filename, dst)

    def get_sample_ids(self, splitks):
        ids = []
        def visit_sample(item):
            ids.append(item.data.sample_id())
        self.visit_samples(splitks, visit_sample, message="Collecting sample ids")
        return ids

    def analyze_samples(self):
        ks = self.jobads_setup().load_datasplit().splitkey_set()
        
        counter = 0
        sample_counter = 0
        issues = []
        def analyze(item):
            jobad = item.data
            nonlocal counter
            nonlocal sample_counter
            counter += 1
            (sample, jobad_issues) = self.sample_from_jobad(jobad)
            issues.extend(jobad_issues)
            if sample is not None:
                sample_counter += 1
        self.visit_jobads(ks, analyze, message="Analyzing samples")
        print(f"{len(issues)} issues of {counter} samples, {counter - sample_counter} missing samples.")
        return issues
        
    
    def evaluate(self, algorithm):
        assert(isinstance(algorithm, ec.ClassificationAlgorithm))
        key = algorithm.key()
        algorepo = self.algorithm_repository(key)
        algorepo.save_metadata(algorithm.metadata())
        results = []
        def classify_sample(item):
            sample = item.data
            label = algorithm.classify_sample(sample)
            if label is None:
                print(f"Missing result for jobad {sample.sample_id()}")
            elif not(self.is_valid_label(label)):
                raise RuntimeError(f"Invalid label '{label}'")
            else:
                result = ec.ClassificationResult(key, sample.sample_id(), label).at_now()
                results.append(result)
        self.visit_samples(self.evaluation_splitkeys(), classify_sample, message=f"Evaluate {key}")

        algorepo.result_repository().extend(results)
        print("Done evaluate.")

    def load_all_results(self):
        def is_included(repo):
            if repo.is_included_setting():
                return True
            else:
                print(f"********* Omitting results from {repo.algorithm_key()}")
                return False
        return (x
                for algo_key in self.algorithm_repository_keys()
                for algo_repo in [self.algorithm_repository(algo_key)]
                if algo_repo.is_included_setting()
                for x in algo_repo.result_repository().load())

    def classification_reporters(self):
        #return [Text2SsykReporter()]
        return [Text2SsykRawReporter(self),
                Text2SsykComparisonReporter(self),
                Text2SsykConfusionReporter(self)]

    def make_classified_samples(self, classified_results):
        result_map = common.group_by(lambda r: r.sample_id(), classified_results)
        dst = []
        def make_classified_sample(item):
            sample = item.data
            rsub = result_map.get(sample.sample_id(), [])
            for r in rsub:
                dst.append(ec.ClassifiedSample(sample, r))
        self.visit_samples(self.evaluation_splitkeys(), make_classified_sample, message="Load result")
        return dst

    def id_label_pairs(self, splitks):
        pairs = []
        def visit(item):
            sample = item.data
            pairs.append((sample.sample_id(), sample.label()))
        self.visit_samples(splitks, visit, message="Collecting id/label pairs...")
        return pairs


    def filter_results(self, results):
        print("Not filtering results")
        return results
    
    def load_classified_samples(self):
        results = self.filter_results(self.load_all_results())
        return self.make_classified_samples(results)


    def report_classified_samples(self, classified_samples):
        for reporter in self.classification_reporters():
            reporter.produce_report(classified_samples)
    
    def make_report(self):
        self.report_classified_samples(self.load_classified_samples())

class EvaluationSetupOnlyCommonSamples(EvaluationSetup):
    def report_name(self):
        return "only-common"
    
    def filter_results(self, results):
        sep = separate_common_results(results)
        m = len(sep["included"]["ids"])
        n = len(sep["all"]["ids"])
        print(f"Use {m} of {n} examples for evaluation")
        return sep["included"]["samples"]


def make_report():
    setup = EvaluationSetupOnlyCommonSamples()
    setup.make_report()
        
if False:
    print("Make setup")
    setup = EvaluationSetup()
    print("Analyze")
    issues = setup.analyze_samples()

if False:
    wrong_text = [issue for issue in issues if issue["type"] == "text-wrong-type"]
    print(f'WRONG TEXT: {wrong_text[0]["jobad"]["description"]}')

    none_text = [issue for issue in issues if issue["type"] == "text-is-none"]
    print(f'NONE TEXT: {none_text[0]["jobad"]["description"]}')
    

    
if False:
    setup.report_classified_samples(csamples)

if False:
    csamples = setup.load_classified_samples()

if False:
    setup = EvaluationSetup()

if False:
    setup.make_report()

if False:
    with setup.jobads_setup().access_dataset(dataset_common.train.label()) as ads:
        sample = setup.sample_from_jobad(next(ads))
        print(sample.features())

if False:
    ids = []
    def visit(item):
        ids.append(item.data["id"])
        assert(len(ids) < 10)
    setup.visit_jobads(["train"], visit)
    print(ids)

if False:
    with setup.random_access() as m:
        x = m['s0017-653836']
        print(x)

if False:
    splitkey = "test0"
    dst_path = setup.env().workspace_path.joinpath(f"text2ssyk_samples_{splitkey}.json")
    setup.export_samples_as_json(dst_path, [splitkey])

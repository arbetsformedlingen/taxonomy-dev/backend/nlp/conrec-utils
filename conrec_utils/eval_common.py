from conrec_utils.env import Env
import conrec_utils.csv_utils as csv_utils
import conrec_utils.common as common
import conrec_utils.spec as spec
import os
import time
import conrec_utils.json_utils as json_utils
from pathlib import Path
import conrec_utils.metric_accumulator as metacc
import conrec_utils.textgrid as tg

class AlgorithmKey(common.ComparableByTuple):
    def __init__(self, name, version=None):
        self._name = name
        self._version = version or "wip"

    def pretty_string(self):
        return f"{self.name()}/'{self.version()}'"

    def mangled_string(self):
        return f"{common.mangle_path(self.name())}_v{common.mangle_path(self.version())}"
        
    def __repr__(self):
        return f"AlgorithmKey(name={self._name}, version={self._version})"

    def name(self):
        return self._name

    def version(self):
        return self._version

    def get_comparison_tuple(self):
        return (self._name, self._version)

    def from_data(data):
        return AlgorithmKey(data["name"], data["version"])

    def data(self):
        return {"name": self._name, "version": self._version}

algorithm_key_spec = spec.TypeSpec(AlgorithmKey)
    
class ClassificationResult(common.ComparableByTuple):
    def __init__(self, algorithm_key, sample_id, label, timestamp_seconds_since_epoch=None):
        self._algorithm_key = algorithm_key_spec.check(algorithm_key)
        self._sample_id = spec.str_spec.check(sample_id)
        self._label = label
        self._timestamp_seconds_since_epoch = spec.float_spec.nullable().check(timestamp_seconds_since_epoch)

    def at_now(self):
        return ClassificationResult(self._algorithm_key, self._sample_id, self._label, time.time())
        
    def algorithm_key(self):
        return self._algorithm_key

    def sample_id(self):
        return self._sample_id

    def label(self):
        return self._label

    def timestamp_seconds_since_epoch(self):
        return self._timestamp_seconds_since_epoch

    def __repr__(self):
        return f"ClassificationResult(algo={self._algorithm_key}, id={self._sample_id}, label={self._label}, ts={self._timestamp_seconds_since_epoch})"

    def get_comparison_tuple(self):
        return (self._algorithm_key, self._sample_id, self._label, self._timestamp_seconds_since_epoch)

classification_result_spec = spec.TypeSpec(ClassificationResult)

class ResultRepository:
    pass

class CsvClassificationResultRepository(ResultRepository):
    csv_config = csv_utils.CsvConfig([
        csv_utils.string_column(["algorithm-name"]),
        csv_utils.string_column(["algorithm-version"]),
        csv_utils.string_column(["sample-id"]),
        csv_utils.string_column(["label"]),
        csv_utils.float_column(["timestamp-seconds-since-epoch"]).nullable()
    ])
    
    def __init__(self, filename):
        assert(isinstance(filename, Path))
        self._filename = filename

    def _deserialize(self, x):
        return ClassificationResult(
            AlgorithmKey(x["algorithm-name"], x["algorithm-version"]),
            x["sample-id"],
            x["label"],
            x["timestamp-seconds-since-epoch"])

    def _serialize(self, x):
        return {"algorithm-name": x.algorithm_key().name(),
                "algorithm-version": x.algorithm_key().version(),
                "sample-id": x.sample_id(),
                "label": x.label(),
                "timestamp-seconds-since-epoch": x.timestamp_seconds_since_epoch()}
    
    def load(self):
        if not(self._filename.exists()):
            return []
        return [self._deserialize(x)
                for x in self.csv_config.read_dicts(self._filename)]

    def save(self, results):
        classification_result_spec.coll().check(results)
        self.csv_config.write_dicts(self._filename, [self._serialize(x) for x in results])

    def extend(self, results):
        updated_id_set = {x.sample_id() for x in results}
        previous_results = [x
                            for x in self.load()
                            if not(x.sample_id() in updated_id_set)]
        all_results = [x
                       for group in [previous_results, results]
                       for x in group]
        self.save(all_results)

class Sample(common.ComparableByTuple):
    def __init__(self, sample_id, features, label, source_data=None):
        self._sample_id = sample_id
        self._features = features
        self._label = label
        self._source_data = source_data

    def with_source_data(self, data):
        return Sample(self._sample_id, self._features, self._label, data)
        
    def sample_id(self):
        return self._sample_id

    def features(self):
        return self._features

    def label(self):
        return self._label

    def get_comparison_tuple(self):
        return (self._sample_id, self._features, self._label)

    def source_data(self):
        return self._source_data
    
    def __repr__(self):
        return f"Sample({self._sample_id})"

sample_spec = spec.TypeSpec(Sample)
    
class ClassifiedSample:
    def __init__(self, sample, prediction):
        self._sample = sample_spec.check(sample)
        self._prediction = classification_result_spec.check(prediction)

    def true_label(self):
        return self._sample.label()

    def predicted_label(self):
        return self._prediction.label()

    def label_set(self):
        return {self.true_label(), self.predicted_label()}
        
    def __repr__(self):
        return f"ClassifiedSample({self._sample}, {self._prediction})"

    def sample(self):
        return self._sample

    def prediction(self):
        return self._prediction

classified_sample_spec = spec.TypeSpec(ClassifiedSample)

class IMetricItem:
    def algorithm_key(self):
        raise NotImplementedError("algorithm_key")
    
    def aggregation_key(self):
        raise NotImplementedError("aggregation_key")
    
    def metric_key(self):
        raise NotImplementedError("metric_key")

    def text(self):
        raise NotImplementedError("text")

    def __repr__(self):
        return f"IMetricItem(algo={self.algorithm_key()}, agg={self.aggregation_key()}, met={self.metric_key()}, value={self.text()})"

metric_item_spec = spec.TypeSpec(IMetricItem)
    
class BasicMetricItem(IMetricItem):
    def __init__(self, met_key, text):
        self._met_key = met_key
        self._text = text
        
    def metric_key(self):
        return self._met_key

    def text(self):
        return self._text
    
class DecoratedMetricItem(IMetricItem):
    def __init__(self, metric_item, aggregation_key=None, algorithm_key=None):
        self._metric_item = metric_item
        self._aggregation_key = aggregation_key
        self._algorithm_key = algorithm_key

    def algorithm_key(self):
        return self._algorithm_key
    
    def aggregation_key(self):
        return self._aggregation_key
    
    def metric_key(self):
        return self._metric_item.metric_key()

    def text(self):
        return self._metric_item.text()
        

class LabeledKey(common.ComparableByTuple):
    def __init__(self, key, label=None):
        self._key = key
        self._label = label or key

    def label(self):
        return self._label

    def key(self):
        return self._key
        
    def get_comparison_tuple(self):
        return (self._key)

    def __repr__(self):
        return f"LabeledKey({self._key})"

    def combine(ks):
        """Combines multiple keys and their labels."""
        labeled_key_spec.coll().check(ks)
        if len(ks) == 1:
            return ks[0]
        label = " / ".join([k.label() for k in ks])
        key = tuple([k.key() for k in ks])
        return LabeledKey(key , label)

    def str_combine(ks):
        """Like combine, but the underlying key is a string instead of a tuple."""
        labeled_key_spec.coll().check(ks)
        if len(ks) == 1:
            return ks[0]
        label = " / ".join([k.label() for k in ks])
        key = "/".join([k.key() for k in ks])
        return LabeledKey(key , label)
        

labeled_key_spec = spec.TypeSpec(LabeledKey)

class IMetricsProducer:
    def produce(self, aggregated_samples):
        raise NotImplementedError("produce")

class BinaryClassificationMetricsProducer(IMetricsProducer):
    def produce(self, aggregated_samples):
        acc = metacc.PairAccumulator()
        acc.accumulate_pairs([(sample.true_label(), sample.predicted_label())
                              for sample in aggregated_samples])
        label_set = {label
                     for sample in aggregated_samples
                     for label in sample.label_set()}
        matrices = acc.binary_classification_matrix_map().values()
        metrics = metacc.BinaryClassificationMatrix.aggregate_common_metrics(matrices)
        return [BasicMetricItem(k, str(v)) for k, v in metrics.items()]


class IMetricRenderer:
    def render_summary_text(self, metrics):
        raise NotImplementedError("render_summary_text")

class BasicTabularMetricRenderer(IMetricRenderer):
    def row_key(self, metric_item):
        return LabeledKey.str_combine([metric_item.aggregation_key(), LabeledKey(metric_item.metric_key())])

    def col_key(self, metric_item):
        ak = metric_item.algorithm_key()
        return LabeledKey(ak.pretty_string())
    
    def render_summary_text(self, metric_items):
        metric_item_spec.coll().check(metric_items)
        dst = tg.TextGrid()
        rows = set()
        cols = set()
        
        header = (0, LabeledKey(""))
        
        def pad(x):
            return tg.PaddedTextBlock(tg.normalize(x), {"horizontal": 1})
        
        for item in metric_items:
            row = self.row_key(item)
            col = self.col_key(item)
            if row is not None and col is not None:
                dst.set_block((1, col), (1, row), pad(item.text()))
                labeled_key_spec.check(row)
                labeled_key_spec.check(col)
                rows.add(row)
                cols.add(col)

        for row in rows:
            dst.set_block(header, (1, row), pad(row.label()))
        for col in cols:
            dst.set_block((1, col), header, pad(col.label()))

        return dst.render_string()
        

class ClassificationReporter:
    def produce_report(self, classified_samples):
        raise NotImplementedError("produce_report")
    
    
# Something that can have its methods overridden.    
class MetricTableClassificationReporter(ClassificationReporter):
    def aggregation_keys(self, sample):
        return [LabeledKey("overall", "Overall")]

    def metrics_producers(self):
        return [BinaryClassificationMetricsProducer()]

    def metric_renderers(self):
        return [BasicTabularMetricRenderer()]

    def render_summary_text(self, metric_items):
        return "\n\n".join([r.render_summary_text(metric_items) for r in self.metric_renderers()])
    
    def compute_metrics_for_algorithm(self, algo_key, classified_samples):
        dst = {}
        for sample in classified_samples:
            aggregation_keys = self.aggregation_keys(sample)
            for k in aggregation_keys:
                if not(k in dst):
                    dst[k] = []
                dst[k].append(sample)

        result = []
        return [DecoratedMetricItem(metric_item, aggregation_key=agg_key, algorithm_key=algo_key)
                for agg_key, agg_samples in dst.items()
                for producer in self.metrics_producers()
                for metric_item in producer.produce(agg_samples)]
    
    def compute_metrics(self, classified_samples):
        samples_per_algorithm = common.group_by(
            lambda s: s.prediction().algorithm_key(),
            classified_samples)
        return [result_item
                for k, algo_samples in samples_per_algorithm.items()
                for result_item in self.compute_metrics_for_algorithm(k, algo_samples)]

    def render_items(self, metric_items):
        print(self.render_summary_text(metric_items))

    def produce_report(self, classified_samples):
        items = self.compute_metrics(classified_samples)
        self.render_items(items)
    
class Algorithm:
    def key(self):
        raise NotImplementedError("Key")

    def metadata(self):
        return {}
    
    def __repr__(self):
        k = self.key()
        return f"Algorithm({k.name()}, v={k.version()})"

class ClassificationAlgorithm(Algorithm):
    def classify_sample(self, sample):
        raise NotImplementedError("classify_sample")

    def make_classified_sample(self, sample):
        return ClassifiedSample(
            sample,
            ClassificationResult(
                self.key(),
                sample.sample_id(),
                self.classify_sample(sample)).at_now())

def _read_json_or_default(p, default_value):
    if p.exists():
        return json_utils.read_json(p)
    else:
        return default_value
    
class AlgorithmRepository:
    def __init__(self, algo_key, path, result_repository):
        assert(isinstance(algo_key, AlgorithmKey))
        assert(isinstance(result_repository, ResultRepository))
        self._algorithm_key = algo_key
        self._path = path
        self._result_repository = result_repository

    def algorithm_key(self):
        return self._algorithm_key

    def _metadata_path(self):
        return self._path.joinpath("metadata.json")

    def _settings_path(self):
        return self._path.joinpath("settings.json")
    
    def save_metadata(self, metadata):
        assert(isinstance(metadata, dict))
        json_utils.write_json(self._metadata_path(), metadata)

    def load_metadata(self):
        return _read_json_or_default(self._metadata_path(), {})

    def load_settings(self):
        return _read_json_or_default(self._settings_path(), {})

    def is_included_setting(self):
        return self.load_settings().get("include", True)

    def path(self):
        return self._path

    def result_repository(self):
        return self._result_repository

def _at_algo_key(p, algo_key):
    return p.joinpath(algo_key.name(), algo_key.version())

    
class EvaluationSetup:
    def name(self):
        raise NotImplementedError("name")
    
    def env(self):
        return Env.default()

    def evaluation_root_path(self):
        return self.env().workspace_path.joinpath("conrec_evaluation", self.name())

    def algorithm_work_root_path(self):
        return self.env().workspace_path.joinpath("conrec_algorithm_work")

    def algorithm_evaluation_root_path(self):
        return self.evaluation_root_path().joinpath("algorithm")

    def report_name(self):
        return "default"
    
    def report_root_path(self):
        return self.evaluation_root_path().joinpath("report", self.report_name())

    def algorithm_report_path(self, algo_key):
        algorithm_key_spec.check(algo_key)

        return _at_algo_key(self.report_root_path().joinpath("algorithm"), algo_key)

    def algorithm_work_path(self, algo_key):
        return _at_algo_key(self.algorithm_work_root_path(), algo_key)

    def algorithm_paths(self, algo_key):
        algorithm_key_spec.check(algo_key)
        root_path = _at_algo_key(self.algorithm_evaluation_root_path(), algo_key)
        key_path = root_path.joinpath("algorithm_key.json")
        return {"root": root_path, "key": key_path}

    def result_repository_at_path(self, path):
        assert(isinstance(path, Path))
        raise NotImplementedError("result_repository_at_path")

    def algorithm_repository_keys(self):
        p = self.algorithm_evaluation_root_path()
        if not(p.exists()):
            return []
        dst = []
        for algo_name in os.listdir(p):
            pa = p.joinpath(algo_name)
            for version in os.listdir(pa):
                pav = pa.joinpath(version)
                kpath = pav.joinpath("algorithm_key.json")
                if kpath.exists():
                    dst.append(AlgorithmKey.from_data(json_utils.read_json(kpath)))
        return dst
    
    def algorithm_repository(self, algo_key):
        algorithm_key_spec.check(algo_key)
        paths = self.algorithm_paths(algo_key)
        os.makedirs(paths["root"], exist_ok=True)
        json_utils.write_json(paths["key"], algo_key.data())
        return AlgorithmRepository(
            algo_key, paths["root"],
            self.result_repository_at_path(paths["root"]))

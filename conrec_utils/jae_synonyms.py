from conrec_utils import json_utils
from conrec_utils import common
from conrec_utils.cached_path import cache

uppercase_types = ["COMPETENCE", "OCCUPATION", "TRAIT", "GEO"]
types = {t.lower() for t in uppercase_types}


def make_synonym_key(synonym_type, synonym_label):
    assert(synonym_type in types)
    return "{:s}/{:s}".format(synonym_type, synonym_label)

def flatten_raw_nested_synonym_data(raw_data):
    return [{**item, "synonym_type": synonym_type, "synonym_key": make_synonym_key(synonym_type, item["concept"])}
            for (synonym_type,data) in raw_data.items() for item in data["items"]]
    
def load_nested_raw_synonym_data(env):
    return {k: json_utils.read_json(cache.get(
        "https://jobad-enrichments-api.jobtechdev.se/synonymdictionary?type={:s}&spelling=BOTH".format(
            k.upper())))
            for k in types}

def load_flat_synonym_data(env):
    return flatten_raw_nested_synonym_data(load_nested_raw_synonym_data(env))

class Synonyms:
    def __init__(self, synonym_list):
        self.synonym_list = synonym_list
        self.type_groups = common.group_by("synonym_type", self.synonym_list)
        self.synonym_groups = common.group_by("synonym_key", self.synonym_list)
        self.term_synonym_map = {x["term"]:x["synonym_key"] for x in self.synonym_list}

    def filter_synonyms(self, pred):
        pred = common.normalize_pred(pred)
        return Synonyms([s for s in self.synonym_list if pred(s)])

    def filter_synonyms_by_type(self, pred):
        pred = common.normalize_pred(pred)
        return self.filter_synonyms(lambda s: pred(s["synonym_type"]))

    def __getitem__(self, i):
        return self.synonym_list[i]

    def __len__(self):
        return len(self.synonym_list)

    def __repr__(self):
        return "Synonyms({:s})".format(", ".join(["{:s}:{:d}".format(k, len(v)) for (k,v) in self.type_groups.items()]))

def load_synonyms(env):
    return Synonyms(load_flat_synonym_data(env))

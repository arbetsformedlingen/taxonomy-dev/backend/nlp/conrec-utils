from pathlib import Path
from conrec_utils import spec
from conrec_utils import filesystem_location

class Env:
    def __init__(self, cache_path, workspace_path, conrec_root_path):
        self.cache_path = cache_path
        self.workspace_path = workspace_path
        self.conrec_root_path = conrec_root_path

    def __repr__(self):
        return f"Env(cache={self.cache_path}, workspace={self.workspace_path})"

    def at_home(home):
        assert(isinstance(home, Path))
        return Env(
            home.joinpath(".jobtech_conrec_utils_cache"),
            home.joinpath("conrec_utils_workspace"),
            filesystem_location.resolve_root_path())

    def default():
        return Env.at_home(Path.home())

env_spec = spec.TypeSpec(Env)

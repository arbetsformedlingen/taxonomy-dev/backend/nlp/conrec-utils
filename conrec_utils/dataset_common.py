import conrec_utils.spec as spec

class SplitDef:
    def __init__(self, label, description):
        self._label = label
        self._description = description

    def label(self):
        return self._label

    def description(self):
        return self._description

train = SplitDef(
    "train",
    "Samples used for training.")

test = SplitDef(
    "test",
    "Samples used for assessing how well an algorithm performs in comparison to other algorithms.")

test0 = SplitDef(
    "test0",
    "Samples used for assessing how well an algorithm performs in comparison to other algorithms, while developing.")

validation = SplitDef(
    "validation",
    "Samples used during the development of an algorithm, e.g. for parameter tuning.")

label_spec = spec.str_spec

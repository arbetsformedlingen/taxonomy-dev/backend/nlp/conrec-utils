from conrec_utils import spec
from conrec_utils import common
from conrec_utils.jaccard import JaccardIndex, JaccardAnalysis, zero_jaccard_index
import conrec_utils.doc_renderer as docr

label_spec = spec.PredicateSpec(lambda x: x != None)

class MetricAccumulator:
    def accumulate(self, true_labels, pred_labels):
        raise NotImplementedError("accumulate")

    def accumulate_pairs(self, pairs):
        self.accumulate([p[0] for p in pairs], [p[1] for p in pairs])

    def get_metrics(self):
        raise NotImplementedError("get_metrics")

    def data_from_metrics(self, metrics):
        raise NotImplementedError("data_from_metrics")
    
    def metrics_from_data(self, metrics):
        raise NotImplementedError("metrics_from_data")

    def key(self):
        raise NotImplementedError("key")
    
    def add(self, metrics_a, metrics_b):
        return metrics_a + metrics_b

    def report_text(self, metrics):
        return str(metrics)

    def __repr__(self):
        return "MetricAccumulator({:s})".format(str(self.get_metrics()))

    def markdown_header_value_pairs(self, metrics):
        raise NotImplementedError("markdown_header_value_pairs")

class Frac:
    def __init__(self, num, denom):
        self._num = num
        self._denom = denom

    def __add__(self, other):
        return Frac(self._num + other._num, self._denom + other._denom)

    def value(self):
        if self._denom != 0:
            return self._num/self._denom

    def __float__(self):
        v = self.value()
        if v is None:
            raise ValueError("Cannot convert to float")
        return v
    
    def percentage_string(self):
        v = self.value()
        if v != None:
            return f"{round(100*v)}%"
        else:
            return "?"
    
    def __repr__(self):
        return f"Frac({self._num}/{self._denom}={self.percentage_string()})"

    def normalize(self):
        v = self.value()
        if v is None:
            return self
        else:
            return Frac(self.value(), 1)

    
class BinaryClassificationMatrix:
    def __init__(self, data):
        self._data = data

    def zero():
        return BinaryClassificationMatrix([0, 0, 0, 0])

    def index(self, a, b):
        assert(isinstance(a, bool))
        assert(isinstance(b, bool))
        return int(a)*2 + b

    def add(self, a, b, amount=1):
        self._data[self.index(a, b)] += amount

    def get(self, a, b):
        return self._data[self.index(a, b)]

    def true_negative(self):
        return self.get(False, False)

    def true_positive(self):
        return self.get(True, True)

    def false_positive(self):
        return self.get(False, True)

    def false_negative(self):
        return self.get(True, False)

    def row_data(self):
        return [[self.get(r, c) for c in [False, True]]
                for r in [False, True]]

    def precision(self):
        return Frac(self.true_positive(), self.true_positive() + self.false_positive())

    # Same as accuracy when we look at just the samples of a single class.
    def recall(self): 
        return Frac(self.true_positive(), self.true_positive() + self.false_negative())

    def correct_count(self):
        return (self.true_positive() + self.true_negative())

    def accuracy(self):
        return Frac(self.correct_count(), self.count())

    def f1(self):
        tp2 = 2.0*self.true_positive()
        fp = self.false_positive()
        fn = self.false_negative()
        return Frac(tp2, tp2 + fp + fn)

    def count(self):
        counter = 0
        for i in self._data:
            counter += i
        return counter

    def common_metrics(self, normalize=False):
        metrics = {"f1": self.f1(),
                   "precision": self.precision(),
                   "recall": self.recall()}
        if normalize:
            return {k:v.normalize() for k, v in metrics.items()}
        else:
            return metrics
    
    def aggregate_common_metrics(matrices, normalize=False):
        return common.add_maps(*[mat.common_metrics(normalize=normalize)
                                 for mat in matrices])
    
class PairAccumulator(MetricAccumulator):
    def __init__(self, pair_map=None):
        self._map = pair_map or {}

    def accumulate(self, true_labels, pred_labels):
        pairs = zip(true_labels, pred_labels)
        for pair in pairs:
            if not(pair in self._map):
                self._map[pair] = 0
            self._map[pair] += 1

    def get_metrics(self):
        return self

    def data_from_metrics(self, pair_accumulator):
        return self._map

    def key(self):
        return "PairAccumulator"
    
    def add(self, metrics_a, metrics_b):
        ma = metrics_a._map
        mb = metrics_b._map
        return PairAccumulator({k:(ma.get(k, 0) + mb.get(k, 0))
                                for m in [ma, mb]
                                for k in m.keys()})

    def binary_classification_matrix(self, cl):
        dst = BinaryClassificationMatrix.zero()
        for (a, b), n in self._map.items():
            dst.add(a == cl, b == cl, n)
        return dst

    def binary_classification_matrix_map(self):
        return {cl: self.binary_classification_matrix(cl)
                for cl in self.all_classes()}

    def report_text(self, metrics):
        return str(metrics)

    def __repr__(self):
        return "({:s})".format(str(self.get_metrics()))

    def markdown_header_value_pairs(self, metrics):
        raise NotImplementedError("markdown_header_value_pairs")

    def count_filtered(self, f):
        counter = 0
        for k, v in self._map.items():
            if f(k):
                counter += v
        return counter
        
    def count(self):
        return self.count_filtered(lambda x: True)

    def correct_classifications(self):
        return self.count_filtered(lambda x: x[0] == x[1])
    
    def accuracy(self):
        return float(self.correct_classifications())/self.count()
        
        
    
    def all_classes(self):
        return {p
                for k in self._map
                for p in k}
    




class BaseJaccardAccumulator(MetricAccumulator):
    def __init__(self, label, title):
        self.label = label
        self.title = title
    
    def data_from_metrics(self, jaccard_index):
        return jaccard_index.data()

    def metrics_from_data(self, data):
        return JaccardIndex.from_data(data)

    def full_title(self):
        return self.title if self.label == None else self.label + " " + self.title
        

def format_jaccard(jaccard):
    return [jaccard.rendered_fraction(),
            " = ",
            docr.bold(jaccard.rendered_percentage())]
    
class JaccardAccumulator(BaseJaccardAccumulator):
    def __init__(self, label=None):
        super().__init__(label, "Charwise Jaccard")
        self.acc = zero_jaccard_index
    
    def accumulate(self, true_labels, pred_labels):
        label_spec.coll().check(true_labels)
        label_spec.coll().check(pred_labels)
        self.acc = self.acc + JaccardAnalysis(true_labels, pred_labels).jaccard_index

    def key(self):
        return "JaccardAccumulator"
        
    def get_metrics(self):
        return self.acc

    def report_text(self, jaccard_index):
        return "{:s}: {:s}".format(self.full_title(), str(jaccard_index))

    def markdown_header_value_pairs(self, metrics):
        return [(docr.bold(self.full_title()), format_jaccard(metrics))]


class OverallJaccardAccumulator(BaseJaccardAccumulator):
    def __init__(self, label=None):
        super().__init__(label, "Overall Jaccard")
        self.true_labels = set()
        self.pred_labels = set()

    def accumulate(self, true_labels, pred_labels):
        self.true_labels.update(true_labels)
        self.pred_labels.update(pred_labels)
        
    def key(self):
        return "OverallJaccardAccumulator"

    def report_text(self, jaccard_index):
        return "{:s}: {:s}".format(self.full_title(), str(jaccard_index))

    def get_metrics(self):
        return JaccardAnalysis(self.true_labels, self.pred_labels).jaccard_index

    def markdown_header_value_pairs(self, metrics):
        return [(docr.bold(self.full_title()), format_jaccard(metrics))]
    

metric_accumulator_spec = spec.TypeSpec(MetricAccumulator)
metric_accumulator_map_spec = spec.MapOfSpec(spec.str_spec, metric_accumulator_spec)

    
class CompositeMetricAccumulator(MetricAccumulator):
    def __init__(self, accumulators):
        self.accumulators = metric_accumulator_spec.coll().check(accumulators)
        
    def accumulate(self, true_labels, pred_labels):
        for acc in self.accumulators:
            acc.accumulate(true_labels, pred_labels)

    def get_metrics(self):
        dst = {}
        for acc in self.accumulators:
            dst[acc.key()] = acc.get_metrics()
        return dst

    def metrics_from_data(self, src):
        assert(isinstance(src, dict))
        return {acc.key():acc.metrics_from_data(src[acc.key()])
                for acc in self.accumulators}

    def data_from_metrics(self, src):
        assert(isinstance(src, dict))
        return {acc.key():acc.data_from_metrics(src[acc.key()])
                for acc in self.accumulators}
                                               
    def key(self):
        return "CompositeMetricAccumulator"

    def add(self, a, b):
        dst = {}
        for acc in self.accumulators:
            k = acc.key()
            dst[k] = acc.add(a[k], b[k])
        assert(dst != None)
        return dst

    def report_text(self, metrics):
        return "\n".join([acc.report_text(metrics[acc.key()])
                          for acc in self.accumulators])

    def markdown_header_value_pairs(self, metrics):
        return [(header, value)
                for acc in self.accumulators
                for (header, value) in acc.markdown_header_value_pairs(metrics[acc.key()])]

        

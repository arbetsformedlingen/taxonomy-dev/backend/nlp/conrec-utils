from conrec_utils.env import Env
import conrec_utils.annotated_document_dataset as add
import conrec_utils.annotated_document_evaluation as ade
import conrec_utils.json_utils as json_utils
import conrec_utils.common as common
from jobtech_taxonomy_library_python.generated.defs import taxonomy
import argparse
import os
import sys
from pathlib import Path
import conrec_utils.doc_renderer as docr
import conrec_utils.cli_tools as cli_tools


class EvaluationTarget:
    def __init__(self, name, result_repo):
        self.name = name
        self.result_repo = result_repo

class AlgoTarget(EvaluationTarget):
    def __init__(self, name, result_repo, algo):
        super().__init__(name, result_repo)
        self.algo = algo

    def evaluate(self, datasets):
        ade.evaluate_algorithm(self.result_repo, datasets, self.algo, incremental=True, verbose=True)

def parse_target(data):
    t = data["type"]
    
    name = data["name"]
    repo = ade.ResultRepository(Path(data["result_repo"]))
    
    if t == "mentor_api_http_algorithm":
        algo = ade.MentorApiHttpAlgorithm(data["url"])
        return AlgoTarget(data["name"], repo, algo)

class EvaluateTask(cli_tools.Task):
    def __init__(self, data=None):
        self.data = {"splitclasses": ["test"]} if data == None else data

    def inherit_from(self, data):
        return EvaluateTask({**self.data, **data})

    def perform(self, setup):
        splitclasses = set(self.data["splitclasses"])
        print("Perform EvaluateTask on {:s}".format(str(splitclasses)))
        datasets = setup.get_datasets(lambda ds: ds.filter_items_by_splitclass(splitclasses))
        for k, target in setup.get_selected_targets().items():
            print("Evaluate " + k)
            target.evaluate(datasets)
            print("Evaluated.")

class GenerateNewIndicesTask(cli_tools.Task):
    def perform(self, setup):
        for (k, repodata) in add.known_repositories.items():
            print("Generate new repository index for {:s}".format(k))
            repo = add.load_repository(setup.env, repodata)
            repo.generate_new_index(add.default_split_proportions)
            print("Generated.")
            
class GenerateReportsTask(cli_tools.Task):
    def perform(self, setup):
        context = setup.reporting_context()
        for k, target in setup.get_selected_targets().items():
            report_path = setup.algo_report_path(k)
            writer = ade.MarkdownReportWriter(report_path)
            context.with_writer(writer).render_report(target.result_repo)

class DsResults:
    def __init__(self, dataset):
        self.dataset = dataset
        self.per_algorithm = {}

    def set(self, algo_key, ds_results):
        self.per_algorithm[algo_key] = ds_results
        
            
class ComparisonReportTask(cli_tools.Task):
    def perform(self, setup):
        targets = setup.get_selected_targets()
        context = setup.reporting_context()

        results_per_dataset = {}
        
        for k, target in targets.items():
            summary = context.summarize_results(target.result_repo)
            for ds_result in summary.sub_results:
                common.ObjectWalker(results_per_dataset).new_or_existing(
                    ds_result.key, DsResults(
                        ds_result.data["dataset"])).obj.set(k, ds_result)

        result_sections = []
        for ds_key, ds_results in results_per_dataset.items():
            title = ds_results.dataset.title()
            pairs = [(docr.link(targets[k].name).to(
                ade.relative_path(Path(setup.report_path), setup.algo_report_path(
                    k).joinpath("README.md"))), v)
                     for k, v in ds_results.per_algorithm.items()]
            result_sections.append(docr.section(
                title, ade.render_results_comparison(context, pairs)))
        full = docr.section("Comparison of Algorithms", result_sections)
        docr.render_markdown_to_file(Path(setup.report_path).joinpath("comparison.md"), full)
            
class Setup(cli_tools.Setup):
    def __init__(self, env):
        super().__init__("conrec-utils CLI",
                         {"evaluate": EvaluateTask(),
                          "generate-reports": GenerateReportsTask(),
                          "comparison-report": ComparisonReportTask(),
                          "generate-new-indices": GenerateNewIndicesTask()})
        self.parser.add_argument('--target', type=str)
        self.env = env
        self.delayed_repositories = []
        self.targets = {}        
        self.selected_targets = []
        

    def configure(self, config):
        self.report_path = config["report_path"]
        self.delayed_repositories = [repo_from_cfg(self, repo)
                                     for repo in config["repositories"]]
        for k, v in config["targets"].items():
            self.targets[k] = parse_target(v)
        

    def algo_report_path(self, k):
        return Path(self.report_path).joinpath(k)
        
    def reporting_context(self):
        return ade.ReportingContext(self.env, taxonomy)


    def get_repositories(self):
        return [r.get() for r in self.delayed_repositories]

    def get_datasets(self, mapping_fn=lambda x: x):
        return [mapping_fn(repo.full_dataset()) for repo in self.get_repositories()]
        
        
    def get_selected_targets(self):
        targets = list(self.targets.keys()) if len(self.selected_targets) == 0 else self.selected_targets
        for target_key in targets:
            if not(target_key in self.targets):
                parser.error("No such target '{:s}'".format(target_key))
        return common.select_keys(self.targets, targets)

def repo_from_cfg(setup, data):
    if isinstance(data, str):
        return common.Delay(lambda k: add.load_known_repository(setup.env, k), data)
            
def main(args):
    setup = Setup(Env.default())
    setup.run(args)
    setup.document_tasks("tasks.md")

if __name__ == "__main__":
    main(sys.argv[1:])

def test_it():
    main([None, "--config-file", "../config.json", "--run-task", "evaluate"])

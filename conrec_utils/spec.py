from conrec_utils import common

class Spec:
    def check(self, x, message=None):
        issues = new_issues()
        self.analyze(x, issues)
        if issues.has_issues():
            raise RuntimeError("Failed spec with {:s}{:s}".format(str(issues), ": " + message if message != None else ""))
        return x

    def analyze(self, x, issues):
        return
        
    def is_valid(self, x):
        issues = new_issues()
        self.analyze(x, issues)
        return not(issues.has_issues())

    def coll(self):
        return CollSpec(self)

    def nullable(self):
        return NullableSpec(self)

    def optional(self):
        return OptionalSpec(self)

    def and_spec(self, *others):
        all_specs = [x for coll in [[self], others] for x in coll]
        return AndSpec(all_specs)



def is_spec(x):
    return isinstance(x, Spec)

class Issues:
    def __init__(self, issues, path):
        self.issues = issues
        self.path = path

    def deeper(self, k):
        return Issues(self.issues, self.path + [k])
    
    def add_issue(self, spec, x, message):
        self.issues.append({"spec": spec, "path": self.path, "x": x, "message": message})
        
    def __len__(self):
        return len(self.issues)

    def __repr__(self):
        return "Issues({:s})".format(", ".join([str(common.remove_keys(x, ["x"]))
                                                for x in self.issues]))

    def has_issues(self):
        return 0 < len(self.issues)


def new_issues():
    return Issues([], [])

class AnySpec(Spec):
    def __repr__(self):
        return "AnySpec"

    def analyze(self, x, issues):
        return

class NullableSpec(Spec):
    def __init__(self, inner):
        self.inner = inner

    def __repr__(self):
        return "NullableSpec({:s})".format(str(self.inner))
        
    def analyze(self, x, issues):
        if x == None:
            return
        self.inner.analyze(x, issues)

class SetSpec(Spec):
    def __init__(self, coll):
        self.coll = set(coll)

    def __repr__(self):
        return "SetSpec({:s})".format(str(self.coll))

    def analyze(self, x, issues):
        if not(x in self.coll):
            issues.add_issue(self, x, )

class PredicateSpec(Spec):
    def __init__(self, pred):
        self.pred = common.normalize_pred(pred)

    def __repr__(self):
        return "PredicateSpec({:s})".format(str(self.pred))

    def analyze(self, x, issues):
        if not(self.pred(x)):
            issues.add_issue(self, x, "Predicate failed")

class OrSpec(Spec):
    def __init__(self, key_spec_pairs=None):
        self.key_spec_pairs = [] if key_spec_pairs == None else key_spec_pairs

    def add(self, k, sp=None):
        if sp == None:
            self.add(len(self.key_spec_pairs), k)
        else:
            self.key_spec_pairs.append((k, sp))
        return self

    def __repr__(self):
        return "OrSpec({:s})".format(", ".join([str(p) for p in self.key_spec_pairs]))

    def analyze(self, x, issues):
        if issues.has_issues():
            return
        for (k, v) in self.key_spec_pairs:
            if v.is_valid(x):
                return
        issues.add_issue(self, x, "No valid spec")

class AndSpec(Spec):
    def __init__(self, specs):
        assert(isinstance(specs, list))
        self.specs = specs

    def __repr__(self):
        return "AndSpec({:s})".format(", ".join([str(x) for x in self.specs]))

    def analyze(self, x, issues):
        if issues.has_issues():
            return
        for s in self.specs:
            s.analyze(x, issues)
            if issues.has_issues():
                return
            
class TypeSpec(Spec):
    def __init__(self, t):
        self.t = t

    def __repr__(self):
        return "TypeSpec({:s})".format(str(self.t))

    def analyze(self, x, issues):
        if not(isinstance(x, self.t)):
            issues.add_issue(self, x, "Wrong type")

any_spec = AnySpec()            
int_spec = TypeSpec(int)
str_spec = TypeSpec(str)
float_spec = TypeSpec(float)
bool_spec = TypeSpec(bool)

class OptionalSpec(Spec):
    def __init__(self, inner_spec):
        self.inner_spec = inner_spec

    def __repr__(self):
        return "OptionalSpec({:s})".format(str(self.inner_spec))

    def analyze(self, x, issues):
        self.inner_spec.analyze(x, issues)
    
class KeysSpec(Spec):
    def __init__(self, spec_map):
        assert(isinstance(spec_map, dict))
        for v in spec_map.values():
            assert(is_spec(v))
        
        self.spec_map = spec_map

    def __repr__(self):
        return "KeysSpec({:s})".format(str(self.spec_map))

    def analyze(self, x, issues):
        if not(isinstance(x, dict)):
            issues.add_issue(self, x, "Not a dict")
            
        for (k, v) in self.spec_map.items():
            if issues.has_issues():
                return
            
            subissues = issues.deeper(k)
            if k in x:
                v.analyze(x[k], subissues)
            elif not(isinstance(v, OptionalSpec)):
                subissues.add_issue(self, x, "Missing value at key")
                


class MapOfSpec(Spec):
    def __init__(self, key_spec, value_spec):
        self.key_spec = key_spec
        self.value_spec = value_spec

    def __repr__(self):
        return "MapOfSpec({:s}, {:s})".format(str(self.key_spec), str(self.value_spec))

    def analyze(self, x, issues):
        try:
            items = x.items()
        except Exception:
            issues.add_issue(self, x, "Not a map")
            return

        for (k, v) in items:
            if issues.has_issues():
                return
            subissues = issues.deeper(k)
            self.key_spec.analyze(k, subissues)
            self.value_spec.analyze(v, subissues)

class CollSpec(Spec):
    def __init__(self, element_spec):
        self.element_spec = element_spec

    def __repr__(self):
        return "CollSpec({:s})".format(str(self.element_spec))

    def analyze(self, x, issues):
        if common.is_common_iterable(x):
            for (i, element) in enumerate(x):
                if issues.has_issues():
                    return
                subissues = issues.deeper(i)
                self.element_spec.analyze(element, subissues)
        else:
            issues.add_issue(self, x, "Not iterable")

        

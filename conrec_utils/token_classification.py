import conrec_utils.annotated_document_evaluation as ade
import conrec_utils.spec as spec
import conrec_utils.metric_accumulator as ma
from conrec_utils import common
import conrec_utils.doc_renderer as docr

class AbstractTokenClassMapper:
    # Returns a string, or None, if there is no valid class
    def class_from_annotated_character(self, annotation):
        raise NotImplementedError("AbstractTokenClassMapper class_from_annotated_character")

    def class_info(self):
        return []

token_class_mapper_spec = spec.TypeSpec(AbstractTokenClassMapper)
    
class ConceptTypeTokenClassMapper(AbstractTokenClassMapper):
    def __init__(self, type_map):
        assert(isinstance(type_map, dict))
        self.type_map = type_map

    def source_classes(self):
        return {k for k in self.type_map.keys()}

    def destination_classes(self):
        return {v for v in self.type_map.values()}

    def __repr__(self):
        return "ConceptTypeTokenClassMapper({:s})".format(str(self.type_map))
        
    def class_from_annotated_character(self, annotation):
        return self.type_map.get(annotation.concept_type)

    def class_info(self):
        info_map = {}
        for (k, v) in self.type_map.items():
            if not(v in info_map):
                info_map[v] = set()
            info_map[v].add(k)
        return [{"class": cl,
                 "info_str": "From concept types {:s}".format(
                     ", ".join(["'{:s}'".format(x) for x in sorted(types)]))}
                for (cl, types) in sorted(info_map.items())]

class CompositeTokenClassMapper(AbstractTokenClassMapper):
    def __init__(self, mappers):
        self.mapper = token_class_mapper_spec.coll().check(mappers)
    
    def class_from_annotated_character(self, c):
        for m in self.mapper:
            y = m.class_from_annotated_character(c)
            if y != None:
                return y

    def class_info(self):
        seen_classes = set()
        dst = []
        for m in self.mapper:
            subinfo = m.class_info()
            for info in subinfo:
                cl = info["class"]
                if not(cl in seen_classes):
                    seen_classes.add(cl)
                    dst.append(info)
        return dst

    def __repr__(self):
        return "CompositeTokenClassMapper({:s})".format(", ".join([str(x) for x in self.mapper]))


def concept_type_token_class_mapper(type_map):
    target_classes = {v for v in type_map.values()}
    return ConceptTypeTokenClassMapper({**type_map, **{v:v for v in target_classes}})
    

class TokenClassificationReportingModule(ade.CharacterClassificationModule):
    def __init__(self, key, token_class_mapper):
        self.k = key
        self.token_class_mapper = token_class_mapper_spec.check(token_class_mapper)
        
    def class_display_data(self, dataset_item_reporter, cl):
        return {"markdown": docr.bold(cl), "text": cl}

    def class_from_annotated_character(self, context, c):
        return self.token_class_mapper.class_from_annotated_character(c)

    def metric_accumulator(self):
        # TODO better metrics
        return ma.CompositeMetricAccumulator([
            ma.JaccardAccumulator("token-class"),
            ma.OverallJaccardAccumulator("token-class")])

    def module_info(self):
        class_info = self.token_class_mapper.class_info()
        dst = docr.markdown_table()
        dst.cell_at(0, 0).set("Class")
        dst.cell_at(0, 1).set("Info")
        row = 1
        for x in class_info:
            dst.cell_at(row, 0).set(x["class"])
            dst.cell_at(row, 1).set(x["info_str"])
            row += 1
        return docr.section("Token classification ({:s})".format(self.k), dst)
    
    def key(self):
        return self.k

    def title(self):
        return "Token classification"

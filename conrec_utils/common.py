from collections.abc import Sequence
import re

def mangle_path(s0):
    """Remove special characters so that it can be used as a filename"""
    s = s0
    for (src, dst) in [("/", "_"), (".", "-")]:
        s = s.replace(src, dst)
    return s

def identity(x):
    return x

def normalize_pred(x):
    if isinstance(x, set):
        return lambda y: y in x

    return x

def generic_group_by(f, coll, init_fn, update_fn):
    dst = {}
    for x in coll:
        k = f(x)
        dst[k] = update_fn(dst[k] if k in dst else init_fn(k), x)
    return dst

def normalize_keyfn(f):
    if isinstance(f, str):
        return lambda x: x[f]
    if isinstance(f, dict):
        return lambda x: f[x]
    return f

def group_by(f, coll):
    f = normalize_keyfn(f)
    def upd(dst, x):
        dst.append(x)
        return dst
    return generic_group_by(f, coll, lambda k: [], upd)

def frequencies(f, coll=None):
    if coll is None:
        return frequencies(lambda x: x, f)
    else:
        return generic_group_by(f, coll, lambda k: 0, lambda acc, x: acc + 1)

def select_keys(m, ks):
    return {k:m[k] for k in ks if k in m}

def remove_keys(m, ks):
    ks = set(ks)
    return {k:v for (k, v) in m.items() if not(k in ks)}

class ArrayIterator:
    def __init__(self, coll):
        self.coll = coll
        self.i = 0

    def __next__(self):
        if self.i < len(self.coll):
            result = self.coll[self.i]
            self.i += 1
            return result
        raise StopIteration


def get_comparison_tuple(x):
    try:
        return x.get_comparison_tuple()
    except AttributeError:
        return None
    
class ComparableByTuple:
    def get_comparison_tuple(self):
        raise NotImplementedError("get_comparison_tuple")
    
    def __eq__(self, other):
        return self.get_comparison_tuple() == get_comparison_tuple(other)

    def __ne__(self, other):
        return self.get_comparison_tuple() != get_comparison_tuple(other)

    def __lt__(self, other):
        return self.get_comparison_tuple() < get_comparison_tuple(other)

    def __le__(self, other):
        return self.get_comparison_tuple() <= get_comparison_tuple(other)

    def __gt__(self, other):
        return self.get_comparison_tuple() > get_comparison_tuple(other)

    def __ge__(self, other):
        return self.get_comparison_tuple() >= get_comparison_tuple(other)

    def __hash__(self):
        return hash(self.get_comparison_tuple())

def partition_by(key_fn, coll):
    key_fn = normalize_keyfn(key_fn)
    dst = []
    part = []
    for x in coll:
        if 0 < len(part) and key_fn(x) != key_fn(part[-1]):
            dst.append(part)
            part = [x]
        else:
            part.append(x)
    if 0 < len(part):
        dst.append(part)
    return dst

def is_stringlike(x):
    return isinstance(x, (str, bytes, bytearray))

def is_common_seq(x):
    return isinstance(x, Sequence) and not is_stringlike(x)

def is_iterable(x):
    try:
        some_object_iterator = iter(x)
        return True
    except TypeError as te:
        return False

def is_common_iterable(x):
    return is_iterable(x) and not is_stringlike(x)

def or_some(*alts):
    for x in alts:
        if x != None:
            return x
    return None

class ObjectWalker:
    def __init__(self, obj):
        self.obj = obj

    def new_or_existing(self, key, init_obj):
        if not(key in self.obj):
            self.obj[key] = init_obj
        return self.at(key)

    def initialize_or_existing(self, key, initialize_fn):
        if not(key in self.obj):
            self.obj[key] = initialize_fn()
        return self.at(key)

    def update(self, key, f, init_value):
        self.obj[key] = f(self.obj[key] if key in self.obj else init_value)

    def set(self, key, x):
        self.obj[key] = x
        return self

    def get(self, key, default=None):
        return self.obj.get(key, default)

    def __getitem__(self, key):
        return self.obj[key]

    def __setitem__(self, k, x):
        self.obj[k] = x

    def at(self, key):
        return ObjectWalker(self.obj[key])

    def __repr__(self):
        return "ObjectWalker({:s})".format(str(self.obj))

def inc(x):
    return x + 1

def segments_from_inds(inds):
    inds = list(sorted(inds))
    n = len(inds)
    if n == 0:
        return []

    dst = []
    lower = inds[0]
    last = lower
    for i in inds[1:]:
        expected_at = last + 1
        if expected_at < i:
            dst.append((lower, expected_at))
            lower = i
            last = i
        else:
            last = i
    dst.append((lower, last+1))
    return dst

class Delay:
    def __init__(self, fn, *args):
        self.fn = fn
        self.has_value = False
        self.value = None
        self.args = args

    def get(self):
        if not(self.has_value):
            self.value = self.fn(*self.args)
            self.has_value = True
        return self.value
        

def div_or_none(a, b):
    return a/b if b != 0 else None

class NonRepeatingValueCounter:
    def __init__(self):
        self.counter = 0
        self.last_value = None
    
    def get(self, input_value):
        if (self.counter == 0) or input_value != self.last_value:
            self.counter += 1
            self.last_value = input_value
        return self.counter



rep = {"condition1": "", "condition2": "text"} # define desired replacements here

def replace_many(text, replacement_map):
    rep = dict((re.escape(k), v) for k, v in replacement_map.items())
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: replacement_map[m.group(0)], text)


def generic_flatten(get_coll, x):
    dst = []
    stack = [x]
    while 0 < len(stack):
        element = stack.pop()
        c = get_coll(element)
        if c is None:
            dst.append(element)
        else:
            stack.extend(reversed(c))
    return dst
        
def basic_flatten(x):
    return generic_flatten(lambda x: x if is_common_seq(x) else None, x)

def merge_with(f, *maps):
    ks = {k
          for m in maps
          for k in m.keys()}

    return {k:f(*[m.get(k) for m in maps])
            for k in ks}

def add_maps(*maps):
    result = {}
    for m in maps:
        for k, v in m.items():
            if k in result:
                result[k] = result[k] + v
            else:
                result[k] = v
    return result

def unique_pred():
    seen = set()
    def pred(x):
        if x in seen:
            return False
        seen.add(x)
        return True
    return pred

def dense_from_sparse(data_map, default_value=None):
    n = None
    for k, v in data_map.items():
        assert(isinstance(k, tuple))
        assert(n is None or n == len(k))
        n = len(k)

    keys_per_dim = [set() for i in range(n)]
    for k, v in data_map.items():
        for i in range(n):
            keys_per_dim[i].add(k[i])

    sorted_keysets = [sorted(keyset) for keyset in keys_per_dim]

    def build_for(ks):
        at = len(ks)
        if at == n:
            return data_map.get(ks, default_value)
        else:
            return [build_for(ks + (k,)) for k in sorted_keysets[at]]
    return build_for(())

def first(coll):
    for x in coll:
        return x

def take(n, coll):
    counter = 0
    for x in coll:
        yield x
        counter += 1
        if n <= counter:
            break

def assign_index(index_map, k):
    v = index_map.get(k)
    if v is None:
        v = len(index_map)
        index_map[k] = v
    return v

class IteratorSeq:
    def __init__(self, iterator):
        self._iterator = iterator

    def evaluate(self):
        try:
            x = next(self._iterator)
            return LazySeq(x, IteratorSeq(self._iterator))
        except StopIteration:
            return None

class LazySeqIterator:
    def __init__(self, s):
        self._s = s
        
    def __next__(self):
        if self._s is None:
            raise StopIteration()
        else:
            x = self._s.first()
            self._s = self._s.next()
            return x
                

class LazySeq:
    def from_iterator(iterator):
        return IteratorSeq(iterator).evaluate()

    def from_collection(l):
        return LazySeq.from_iterator(iter(l))
        
    def __init__(self, first, next_seq):
        self._first = first
        self._next_seq = next_seq

    def first(self):
        return self._first

    def evaluate(self):
        return self
    
    def next(self):
        if self._next_seq is None:
            return None
        else:
            self._next_seq = self._next_seq.evaluate()
            return self._next_seq

    def __iter__(self):
        return LazySeqIterator(self)

    def __repr__(self):
        tail = ""
        if self._next_seq is not None:
            tail = ", ..."
        return f"LazySeq({self._first}{tail})"
    

import csv
import conrec_utils.spec as spec
import conrec_utils.common as common

class ColumnProperties:
    def __init__(self, known_headers):
        self.known_headers = common.basic_flatten(known_headers)
        self.header = self.known_headers[0]
        self.key = self.header
        self.required = True
        self.default_value = None
        
    def with_key(self, k):
        self._key = k
        return self

class BaseColumn:
    def __repr__(self):
        return "BaseColumn({:s}, {:s})".format(self.properties().key, str(self.properties().known_headers))

    def properties(self):
        raise NotImplementedError("properties")

    def read_from_dict(self, x):
        props = self.properties()
        if props.key in x:
            return x[props.key]
        elif props.required:
            raise RuntimeError("Missing key {:s}".format(props.key))
        return props.default_value

    def nullable(self):
        return NullableColumn(self)

    def import_value(self, x):
        return self._import_fn(x)

    def export_value(self, x):
        return self._export_fn(x)

base_column_spec = spec.TypeSpec(BaseColumn)
    
    
class Column(BaseColumn):
    def __init__(self, properties, export_fn, import_fn):
        self._properties = properties
        self._export_fn = export_fn
        self._import_fn = import_fn

    def properties(self):
        return self._properties

    def import_value(self, x):
        return self._import_fn(x)

    def export_value(self, x):
        return self._export_fn(x)

class NullableColumn(BaseColumn):
    def __init__(self, inner_column):
        self._inner_column = inner_column
        
    def __repr__(self):
        return f"NullableColumn({self._inner_column})"

    def properties(self):
        return self._inner_column.properties()

    def import_value(self, x):
        if x == "":
            return None
        return self._inner_column.import_value(x)

    def export_value(self, x):
        if x is None:
            return ""
        return self._inner_column.export_value(x)

class CsvConfig:
    def __init__(self, columns):
        self.columns = base_column_spec.coll().check(columns)
        self.header2column_map = {}
        self.keys = set()
        self.required_keys = set()
        for c in columns:
            props = c.properties()
            if props.key in self.keys:
                raise RuntimeError("Multiple columns with same key {:s}".format(props.key))
            self.keys.add(props.key)
            
            for h in props.known_headers:
                if props.required:
                    self.required_keys.add(props.key)
                if h in self.header2column_map:
                    raise RuntimeError("Multiple columns with same header {:s}".format(h))
                self.header2column_map[h] = c

    def __repr__(self):
        return "CsvConfig({:s})".format(", ".join([c.properties().key for c in self.columns]))

    def write_dicts(self, filename, dicts):
        with open(filename, "w") as f:
            writer = csv.writer(f)
            writer.writerow([col.properties().header for col in self.columns])
            for x in dicts:
                writer.writerow([col.export_value(col.read_from_dict(x)) for col in self.columns])

    def read_dicts(self, filename):
        dst = []
        
        with open(filename, "r") as f:
            reader = csv.reader(f)
            counter = 0
            cols = []
            for row in reader:
                if counter == 0:
                    cols = [self.header2column_map[x] for x in row]
                    ks = {col.properties().key for col in cols}
                    missing = {r for r in self.required_keys if not(r in ks)}
                    if 0 < len(missing):
                        print("Missing required columns: {:s}".format(str(missing)))
                else:
                    if len(row) != len(cols):
                        raise RuntimeError("Inconsistent csv row lengths. Expected columns {:s} but got data {:s}".format(str(cols), str(rows)))
                    dst.append({col.properties().key:col.import_value(x) for (col, x) in zip(cols, row)})
                counter += 1
        return dst

            


def string_column(*headers):
    return Column(ColumnProperties(headers), str, str)

def float_column(*headers):
    return Column(ColumnProperties(headers), str, float)

def int_column(*headers):
    return Column(ColumnProperties(headers), str, int)

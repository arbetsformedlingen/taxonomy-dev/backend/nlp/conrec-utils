from conrec_utils import json_file_map
from conrec_utils import time_utils

class Job:
    def run(self):
        return None

    def has_result(self, m):
        return self.key in m

    def evaluate(self, m):
        if self.has_result(m):
            return m[self.key]
        else:
            result = self.compute()
            m.put(self.key, result)
            return result

    def compute(self):
        return None
    
    def __repr__(self):
        return "Job"

class FunctionJob(Job):
    def __init__(self, key, f):
        self.key = key
        self.f = f
        self.has_data = False
        self.data = None

    def compute(self):
        if self.has_data:
            return self.f(self.data)
        else:
            return self.f()

    def with_data(self, x):
        self.data = x
        self.has_data = True
        return self
        
    def __repr__(self):
        return "FunctionJob({:s},{:s})".format(self.key, str(self.f))
    
        
class JobRunner:
    def __init__(self, job_result_map):
        assert(isinstance(job_result_map, json_file_map.JsonFileMap))
        self.job_result_map = job_result_map
        self.verbose = True

    def reset(self):
        self.job_result_map.clear()

    def __repr__(self):
        return "JobRunner({:s})".format(self.job_result_map.rootdir)
        
    def run_jobs(self, jobs):
        assert(isinstance(jobs, list))
        for x in jobs:
            assert(isinstance(x, Job))
        
        completed_ks = self.job_result_map.keys()

        jobs_to_run = {job for job in jobs if not(job.key in completed_ks)}
        
        if self.verbose:
            print("Running jobs at {:s}".format(self.job_result_map.rootdir))
            print("{:d} jobs in total, {:d} jobs completed, {:d} jobs remain".format(len(jobs), len(completed_ks), len(jobs_to_run)))

        prog = time_utils.ProgressReporter(len(jobs_to_run))
        for job in jobs_to_run:
            job.evaluate(self.job_result_map)
            if self.verbose:
                prog.end_of_iteration_message("Running jobs at {:s}".format(self.job_result_map.rootdir))
        return self.job_result_map
        
        

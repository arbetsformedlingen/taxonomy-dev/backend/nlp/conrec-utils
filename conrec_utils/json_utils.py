import json

#def read_cached_json(url):
#    with open(cached_path(url)) as f:
#        return json.load(f)

def read_json(filename):
    with open(filename, "r") as f:
        return json.load(f)

def write_json(filename, data):
    with open(filename, "w") as f:
        json.dump(data, f)

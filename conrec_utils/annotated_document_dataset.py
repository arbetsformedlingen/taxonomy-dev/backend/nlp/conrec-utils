import subprocess
import csv
import random
from pathlib import Path
import importlib
from conrec_utils import common
from conrec_utils.env import Env
from conrec_utils import spec
from conrec_utils.csv_utils import CsvConfig, string_column, float_column
import conrec_utils.json_utils as json_utils
import conrec_utils.text as text
from conrec_utils import filesystem_location
import os

def dataset_from_git(url, commit_sha):
    l = url.rfind("/")
    u = url.rfind(".git")
    assert(l != None)
    assert(u != None)
    name = url[(l+1):u]
    return {"name": name,
            "repository_url": url,
            "master_branch": "master",
            "commit_sha": commit_sha,
            "index_path": "datasets/{:s}/index.csv".format(name)}

known_repository_list = [dataset_from_git("https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/annotations/mentor-api-prod.git", "c701b275d41581796c2f14cdcded8671366a86ca")]

known_repositories = {x["name"]:x for x in known_repository_list}

default_split_proportions = [("train", 0.7),
                             ("validation", 0.2),
                             ("test", 0.1)]

index_item_spec = spec.KeysSpec({"id": spec.str_spec,
                                 "order": spec.float_spec,
                                 "splitclass": spec.str_spec})

repo_specs_spec = spec.KeysSpec({"name": spec.str_spec})

dataset_data_repr_spec = spec.KeysSpec({"repo_specs": repo_specs_spec,
                                        "derived": spec.KeysSpec({"type": spec.str_spec}).coll()})

index_items_spec = spec.CollSpec(index_item_spec)

def parse_dataset_index_filename_counter(filename):
    prefix = "dataset_index_"
    suffix = ".csv"
    if filename.startswith(prefix) and filename.endswith(suffix):
        try:
            return int(filename[len(prefix):(len(filename)-len(suffix))])
        except ValueError:
            None

def dataset_index_filename_for_counter(c):
    return "dataset_index_{:d}.csv".format(c)


index_csv_config = CsvConfig([string_column(["id", "sha"]),
                              float_column("order"),
                              string_column("splitclass")])

def write_dataset_index(filename, dataset_index):
    return index_csv_config.write_dicts(filename, dataset_index)

def read_dataset_index(filename):
    data = index_csv_config.read_dicts(filename)
    freqs = common.frequencies(lambda x: x["id"], data)
    for (k, v) in freqs.items():
        if v != 1:
            raise RuntimeError("Corrupt dataset index: Same sha {:s} occurs {:d} times".format(k, v))
    return data

def weighted_select(item_prop_pairs, frac_01):
    assert(0 <= frac_01)
    assert(frac_01 <= 1)
    total = 0
    for (item, prop) in item_prop_pairs:
        total += prop
    f = 1.0/total
    at = 0
    for (item, prop) in item_prop_pairs:
        at += f*prop
        if frac_01 <= at:
            return item

def weighted_sample(item_prop_pairs):
    return weighted_select(item_prop_pairs, random.random())

shas_spec = spec.CollSpec(spec.str_spec)

def regenerate_dataset_index(previous_index, shas, proportions):
    assert(isinstance(previous_index, list))
    index_items_spec.check(previous_index)
    shas_spec.check(shas)
    existing_shas = {x["id"] for x in previous_index}
    new_items = [{"id": sha, "order": random.random()} for sha in shas if not(sha in existing_shas)]
    
    def assign_prop(x):
        return {**x, "splitclass": weighted_select(proportions, x["order"])}
    
    return sorted([assign_prop(x) for coll in [previous_index, new_items] for x in coll], key=lambda x: x["order"])

def are_valid_dataset_specs(specs):
    return isinstance(specs, dict) and ("name" in specs) and ("repository_url" in specs) and isinstance(specs["name"], str) and isinstance(specs["repository_url"], str)

def error_on_bad_returncode(run_result):
    if run_result.returncode == 0:
        return run_result
    else:
        raise RuntimeError("Bad return code: " + str(run_result))

def clone_repository(repository_url, dst_path):
    assert(isinstance(repository_url, str))
    assert(isinstance(dst_path, str))
    return error_on_bad_returncode(subprocess.run(['git', 'clone', repository_url, dst_path]))

def pull_repository(repo_path):
    assert(isinstance(repo_path, str))
    print("Git pull repository in {:s}".format(repo_path))
    return error_on_bad_returncode(subprocess.run(['git', 'pull'], cwd=repo_path))

def checkout_repository(repo_path, branch_or_commit):
    assert(isinstance(repo_path, str))
    return error_on_bad_returncode(subprocess.run(['git', 'checkout', branch_or_commit], cwd=repo_path))

def list_shas(repo_path):
    assert(isinstance(repo_path, Path))
    files = os.listdir(repo_path)
    dst = []
    for filename in files:
        if filename.lower().endswith(".json"):
            full_filename = repo_path.joinpath(filename)
            data = json_utils.read_json(full_filename)
            dst.append(data["sha1"])
    return dst

class DerivedInfo:
    def __init__(self, key, title=None, data=None):
        title = key if title == None else title
        self.key = key
        self.title = title
        self.data = data

    def __repr__(self):
        return "DerivedInfo({:s}, '{:s}')".format(self.key, self.title)

derived_info_spec = spec.TypeSpec(DerivedInfo)
derived_infos_spec = spec.CollSpec(derived_info_spec)

annotation_spec = spec.KeysSpec({"concept-id": spec.OrSpec().add(spec.TypeSpec(str)).add(spec.SetSpec([None])),
                                 "start-position": spec.TypeSpec(int),
                                 "end-position": spec.TypeSpec(int)})

annotations_spec = spec.CollSpec(annotation_spec)

sha1_spec = spec.str_spec
assert(spec.is_spec(sha1_spec))


dataset_item_data_spec = spec.KeysSpec({"sha1": sha1_spec,
                                        "text": spec.str_spec,
                                        "annotations": annotations_spec})


class DatasetItem:
    def __init__(self, src_dataset, id, order, splitclass, data):
        self.src_dataset = src_dataset
        self.id = spec.str_spec.check(id)
        self.order = spec.float_spec.check(order)
        self.splitclass = spec.str_spec.check(splitclass)
        self.data = dataset_item_data_spec.check(data)

    def to_data(self):
        return {"id": self.id,
                "order": self.order,
                "splitclass": self.splitclass,
                "data": self.data}

    def __repr__(self):
        return "DatasetItem({:s}, order={:s}, split={:s})".format(
            self.id, str(self.order), self.splitclass)

    def with_data(self, new_data):
        return DatasetItem(self.src_dataset, self.id, self.order, self.splitclass, new_data)

    def update_data(self, f):
        return self.with_data(f(self.data))

dataset_item_map_spec = spec.KeysSpec({"id": spec.str_spec,
                                       "order": spec.float_spec,
                                       "splitclass": spec.str_spec})
dataset_item_spec = spec.TypeSpec(DatasetItem)

def reconstruct_range_parameters(inds):
    lower = min(inds)
    upper = 1+max(inds)
    index_set = set(inds)
    expected_range = range(lower, upper)
    if len(index_set) == len(expected_range):
        return {"lower": lower, "upper": upper}
    
class Dataset:
    """A dataset is a collection of samples of manually annotated documents. It can be all the items or just for a particular split (train, test or validation)."""
    def __init__(self, repository, index, derived_infos):
        derived_infos_spec.check(derived_infos)
        self.derived_infos = derived_infos
        self.repository = repository
        self.index = index
        self.sha2index = {x["id"]:i for (i, x) in enumerate(index)}

    def id_set(self):
        return {x["id"] for x in self.index}
        
    def derive_dataset(self, new_index):
        return Dataset(self.repository, new_index, self.derived_infos + [DerivedInfo("derived")])

    def with_last_info(self, info):
        derived_info_spec.check(info)
        self.derived_infos[-1] = info
        return self
    
    def get_by_id(self, sha):
        i = self.sha2index.get(sha)
        if i == None:
            return None
        return self[i]
        
    def filter_items(self, item_pred_fn):
        """Return a new dataset that is a subset based on a filter predicate function"""
        item_pred_fn = common.normalize_pred(item_pred_fn)
        return self.derive_dataset([item for item in self.index if item_pred_fn(item)]).with_last_info(DerivedInfo("Filtered"))

    def _derived_info_for_splitclass_set(self, splitclass_set):
        return DerivedInfo(
            "splitclass" + "_".join(splitclass_set),
            "/".join(splitclass_set),
            {"type": "splitclass", "classes": list(splitclass_set)})
    
    def filter_items_by_splitclass(self, splitclass_set):
        """Return a new dataset of items that have splitclass in the specified set (train, validation or test)"""
        derived_info = self._derived_info_for_splitclass_set(splitclass_set)
        return self.filter_items(lambda item: item["splitclass"] in splitclass_set).with_last_info(derived_info)

    def train_subset(self):
        """Return the train subset of this dataset"""
        return self.filter_items_by_splitclass({"train"})

    def full_train_subset(self):
        """Return the non-test subset of this dataset (train and validation)"""
        return self.filter_items_by_splitclass({"train", "validation"})

    def validation_subset(self):
        """Return the validation subset of this dataset"""
        return self.filter_items_by_splitclass({"validation"})
    
    def test_subset(self):
        """Return the test subset of this dataset"""
        return self.filter_items_by_splitclass({"test"})

    def splitclass_subsets(self):
        groups = common.group_by("splitclass", self.index)
        return {k:self.derive_dataset(v).with_last_info(self._derived_info_for_splitclass_set({k}))
                for (k, v) in groups.items()}

    def __len__(self):
        """Return the length of this dataset"""
        return len(self.index)

    def splitclass_set(self):
        """Return the set of sample splitclasses"""
        return {x["splitclass"] for x in self.index}

    def splitclass_count_map(self):
        """Returns a map with number of samples for every splitclass"""
        return common.frequencies(lambda x: x["splitclass"], self.index)

    def derive_from_data_item(self, data_item):
        if data_item == None:
            raise RuntimeError("Cannot derive from data item")

        t = data_item["type"]
        if t == "slice":
            return self[data_item["lower"]:data_item["upper"]]
        elif t == "splitclass":
            return self.filter_items_by_splitclass(set(data_item["classes"]))
        else:
            raise RuntimeError("Unknown type " + str(t))

    def derive_from_data(self, data):
        result = self
        for data_item in data:
            result = result.derive_from_data_item(data_item)        
        return result
        
    def __getitem__(self, i):
        """Get the nth item"""
        if isinstance(i, slice):
            inds = range(*i.indices(len(self.index)))
            params = reconstruct_range_parameters(inds)
            if params == None:
                key = "unknownslice"
                title = "Unknown slice"
                data = None
            else:
                lower = params["lower"]
                upper = params["upper"]
                key = "slice{:d}_{:d}".format(lower, upper)
                title = "Slice {:d}:{:d}".format(lower, upper)
                data = {"type": "slice", "lower": lower, "upper": upper}
            return self.derive_dataset([self.index[i] for i in inds]).with_last_info(DerivedInfo(key, title, data))
        
        assert(isinstance(i, int))
        item = self.index[i]
        return DatasetItem(
            self,
            item["id"],
            item["order"],
            item["splitclass"],
            self.repository.load_item(item["id"]))

    def __iter__(self):
        return common.ArrayIterator(self)

    def title(self):
        rep = self.repository.title()
        if 0 == len(self.derived_infos):
            return rep + ": Full dataset"
        else:
            return rep + ": " + " -> ".join([x.title for x in self.derived_infos])

    def derived_data(self):
        return [x.data for x in self.derived_infos]

    def data_repr(self):
        return {"repo_specs": self.repository.get_specs(),
                "derived": self.derived_data()}

    def key(self):
        rep = self.repository.title()
        return rep + "__" + "_".join([x.key for x in self.derived_infos])
    
    def __repr__(self):
        """Brief text representation of the dataset"""
        return "Dataset({:s}, {:s}, {:s})".format(
            self.title(),
            str(self.repository),
            ", ".join(["{:s}:{:d}".format(k, v) for (k, v) in self.splitclass_count_map().items()]))

dataset_spec = spec.TypeSpec(Dataset)    
    
class AbstractRepository:
    def get_index(self):
        return []

    def title(self):
        return "AbstractRepository"
    
    def load_item(self, item_id):
        raise RuntimeError("Not implemented")

    def full_dataset(self):
        """Return the full dataset (train, validation and test)"""
        index = self.get_index()
        if index == None:
            raise RuntimeError("Cannot construct dataset due to missing index.")
        return Dataset(self, index, [])

    def get_specs(self):
        return None

    
dataset_items_spec = spec.CollSpec(dataset_item_spec)

class InMemoryRepository(AbstractRepository):
    def __init__(self, dataset_items):
        dataset_items_spec.check(dataset_items)
        self.index = [common.select_keys(
            x.to_data(),
            ["id", "splitclass", "order"]) for x in dataset_items]
        self.item_data_map = {item.id:item.data for item in dataset_items}

    def get_index(self):
        return self.index

    def load_item(self, item_id):
        return self.item_data_map[item_id]

    def __repr__(self):
        return "InMemoryRepository({:d} items)".format(len(self.index))

class Repository(AbstractRepository):
    """A Repository represents a local git repository of a dataset and the index of items in the dataset."""
    def __init__(self, repo_path, specs):
        self.repo_path = repo_path
        self.specs = specs

    def title(self):
        return self.specs["name"]

    def __repr__(self):
        return "Repository({:s})".format(str(self.repo_path))

    def list_shas(self):
        """List the shas of all dataset items"""
        return list_shas(self.repo_path)

    def index_path(self):
        """Return the path to the dataset index file."""
        return filesystem_location.resolve_root_path().joinpath(self.specs["index_path"])

    def get_index(self):
        """Get the dataset item index or None if it doesn't exist"""
        p = self.index_path()
        if p.exists():
            return read_dataset_index(p)

    def generate_new_index_to_file(self, filename, split_proportions, previous_file_path):
        """Generate a new split for a dataset and write to a file"""
        previous_index = read_dataset_index(previous_file_path) if previous_file_path.exists() else []
        write_dataset_index(filename, regenerate_dataset_index(previous_index, self.list_shas(), split_proportions))

    def generate_new_index(self, split_proportions):
        """Generate a new split for the dataset and write to the default index file."""
        self.generate_new_index_to_file(self.index_path(), split_proportions, self.index_path())
        
    def load_item(self, item_id):
        return json_utils.read_json(self.repo_path.joinpath(item_id + ".json"))

    def get_specs(self):
        return self.specs

def summarize_dataset_item(item):
    data = item.data
    return "splitclass={:s}, text='{:s}' annotations:{:d}".format(
        item.splitclass, text.abbreviate(data["text"], 30), len(data["annotations"]))

def load_repository_at_path(repo_path, dataset_specs):
    assert(are_valid_dataset_specs(dataset_specs))
    repo_str = str(repo_path)
    if not(repo_path.exists()):
        clone_repository(dataset_specs["repository_url"], repo_str)
    checkout_repository(repo_str, dataset_specs["master_branch"])
    pull_repository(repo_str)
    checkout_repository(repo_str, dataset_specs["commit_sha"])
    return Repository(repo_path, dataset_specs)

    
def load_repository(env, dataset_specs):
    """Load a Repository given an env and specs of the dataset"""
    return load_repository_at_path(
        env.cache_path.joinpath("annotated_document_datasets").joinpath(dataset_specs["name"]),
        dataset_specs)

def load_known_repository(env, repository_name):
    return load_repository(env, known_repositories[repository_name])

def dataset_from_data_repr(env, data_repr):
    repo_spec = repo_specs_spec.check(data_repr["repo_specs"])
    return load_repository(env, repo_spec).full_dataset().derive_from_data(data_repr["derived"])

if False:
    repo = load_repository(Env.default(), known_datasets["mentor-api-prod"])
    full_dataset = repo.full_dataset()
    train_dataset = full_dataset.train_subset()
    validation_dataset = full_dataset.validation_subset()
    test_dataset = full_dataset.test_subset()
    item = full_dataset[0]
    print(full_dataset)
    print(summarize_dataset_item(full_dataset[0]))

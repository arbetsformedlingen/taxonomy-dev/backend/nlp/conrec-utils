import importlib
from conrec_utils import common
from conrec_utils import spec

class DocNode:
    pass

class MarkdownToken:
    pass

node_spec = spec.TypeSpec(DocNode)
nodes_spec = spec.CollSpec(node_spec)
text_feature_spec = spec.SetSpec({"bold", "italic", "tt"})
text_features_spec = spec.CollSpec(text_feature_spec)

def escape(s):
    special_chars = "\\[]|"
    assert(special_chars[0] == "\\")
    for char in special_chars:
        s = s.replace(char, "\\{:s}".format(char))
    return s

def remove_linebreaks(s):
    return common.replace_many(s, {"\r": " ", "\n": " "})

class StringToken(MarkdownToken):
    def __init__(self, s):
        self.s = s

    def __repr__(self):
        return "StringToken('{:s}')".format(self.s)

    def render(self):
        return self.s

class SyntacticBlank:
    def __repr__(self):
        return "SyntacticBlank"
    
    def render(self):
        return " "

class SingleBlankLineToken(MarkdownToken):
    def __repr__(self):
        return "SingleBlankLine"

    def render(self):
        return "\n\n"

class SectionHeaderToken(MarkdownToken):
    def __init__(self, level):
        self.level = level

    def __repr__(self):
        return "SectionHeaderToken({:d})".format(self.level)

    def render(self):
        return "#"*(1 + self.level) + " "

class AddressToken(MarkdownToken):
    def __init__(self, address):
        self.address = address

    def __repr__(self):
        return "AddressToken({:s})".format(self.address)

    def render(self):
        return self.address

class BracketToken:
    def __init__(self, bracket, open_or_close):
        self.bracket = bracket
        self.open_or_close = open_or_close

    def __repr__(self):
        return "BracketToken({:s}, {:s})".format(
            self.bracket, str(self.open_or_close))

    def render(self):
        return self.bracket

open_close_spec = spec.SetSpec({"open", "close"})

class FormattingToken(MarkdownToken):
    def __init__(self, features, open_or_close):
        open_close_spec.check(open_or_close)
        self.features = features
        self.open_or_close = open_or_close

    def render(self):
        stars = "*"*((2 if "bold" in self.features else 0) + (1 if "italic" in self.features else 0))
        backtick = "`" if "tt" in self.features else ""
        return (stars + backtick) if self.open_or_close == "open" else (backtick + stars)

    def __repr__(self):
        return "FormattingToken({:s}, {:s})".format(self.open_or_close, str(self.features))

def is_formatting_token(x, kind):
    return isinstance(x, FormattingToken) and x.open_or_close == kind

def is_bracket_token(x, kind):
    return isinstance(x, BracketToken) and x.open_or_close == kind

class MarkdownContext:
    def __init__(self):
        self.text_features = set()
        self.section_level = 0
        self.tokens = []
        self.can_output_linebreaks = True

    def copy(self):
        dst = MarkdownContext()
        dst.text_features = self.text_features.copy()
        dst.section_level = self.section_level
        dst.tokens = self.tokens
        dst.can_output_linebreaks = self.can_output_linebreaks
        return dst

    def without_linebreaks(self):
        dst = self.copy()
        dst.can_output_linebreaks = False
        return dst
    
    def with_text_features(self, features):
        text_features_spec.check(features)
        dst = self.copy()
        for f in features:
            dst.text_features.add(f)
        return dst

    def deeper(self):
        dst = self.copy()
        dst.section_level += 1
        return dst

    def write_text(self, s):
        contents = remove_linebreaks(escape(s)) #.strip()
        if 0 < len(contents):
            self.tokens.append(StringToken(contents))

    def single_blank_line(self):
        assert(self.can_output_linebreaks)
        self.tokens.append(SingleBlankLineToken())

    def text_features_token(self, open_or_close):
        if 0 < len(self.text_features):
            self.tokens.append(FormattingToken(self.text_features, open_or_close))
        
    def open_text_features(self):
        self.text_features_token("open")

    def close_text_features(self):
        self.text_features_token("close")

    def section_header(self):
        self.tokens.append(SectionHeaderToken(self.section_level))

    def address(self, x):
        self.tokens.append(AddressToken(x))

    def open_bracket(self, b):
        self.tokens.append(BracketToken(b, "open"))
        
    def close_bracket(self, b):
        self.tokens.append(BracketToken(b, "close"))
    
class TextNode(DocNode):
    def __init__(self, s):
        self.s = str(s)

    def __repr__(self):
        return "TextNode('{:s}')".format(self.s)

    def render_markdown(self, context):
        context.open_text_features()
        context.write_text(self.s)
        context.close_text_features()

class GroupNode(DocNode):
    def __init__(self, nodes):
        nodes_spec.check(nodes)
        self.nodes = nodes

    def __getitem__(self, i):
        return self.nodes[i]

    def __len__(self):
        return len(self.nodes)
    
    def __repr__(self):
        return "GroupNode({:s})".format(", ".join([str(x) for x in self.nodes]))

    def render_markdown(self, context):
        for node in self.nodes:
            node.render_markdown(context)
        
class TextFeatureNode(DocNode):
    def __init__(self, features, subnode):
        text_features_spec.check(features)
        node_spec.check(subnode)
        self.features = features
        self.subnode = subnode

    def render_markdown(self, context):
        self.subnode.render_markdown(context.with_text_features(self.features))
        
    def __repr__(self):
        return "TextFeatureNode({:s}, {:s})".format(str(self.features), str(self.subnode))

class ParagraphNode(DocNode):
    def __init__(self, subnode):
        self.subnode = subnode

    def render_markdown(self, context):
        context.single_blank_line()
        self.subnode.render_markdown(context)
        context.single_blank_line()
        
    def __repr__(self):
        return "Paragraph({:s})".format(str(self.subnode))
    
class SectionNode(DocNode):
    def __init__(self, title, body):
        self.title = title
        self.body = body

    def render_markdown(self, context):
        context.single_blank_line()
        context.section_header()
        self.title.render_markdown(context.without_linebreaks())
        context.single_blank_line()
        self.body.render_markdown(context.deeper())
        
    def __repr__(self):
        return "SectionNode({:s}, {:s})".format(str(self.title), str(self.body))

class TableCell:
    def __init__(self):
        self.is_header = False
        self.data = None

    def set(self, *contents):
        self.data = normalize_nodes(contents)
        return self

    def set_is_header(self, h=True):
        self.is_header = h
        return self

    def __repr__(self):
        return "TableCell({:s}, is_header={:d})".format(str(self.data), 1 if self.is_header else 0)

def markdown_header_predicate(row, col):
    return row == 0
    
class Table(DocNode):
    def __init__(self, is_header_predicate):
        self.cell_map = {}
        self.is_header_predicate = is_header_predicate

    def cell_at(self, row, col):
        k = (row, col)
        if k in self.cell_map:
            return self.cell_map[k]
        else:
            cell = TableCell().set_is_header(self.is_header_predicate(row, col))
            self.cell_map[k] = cell
            return cell

    def render_padded_markdown_cell(self, row, col):
        k = (row, col)
        data = self.cell_map.get(k)
        if data == None:
            return " "
        return " " + render_markdown_with_context(data.data, MarkdownContext().without_linebreaks()) + " "

    def render_markdown_row(self, row, column_count):
        return "|" + "|".join(
            [self.render_padded_markdown_cell(row, col)
             for col in range(column_count)]) + "|"

    def header_bar(self, cols):
        return "|-"*cols + "|"
    
    def render_markdown(self, context):
        #inner_context = MarkdownContext().without_linebreaks()
        dst = ""
        rows = self.table_size_along_dim(0)
        cols = self.table_size_along_dim(1)
        for i in range(rows):
            if i == 1:
                dst += "\n" + self.header_bar(cols) + "\n"
            elif 0 < i:
                dst += "\n"
            dst += self.render_markdown_row(i, cols)
        context.tokens.append(SingleBlankLineToken())    
        context.tokens.append(StringToken(dst))
        context.tokens.append(SingleBlankLineToken())    

    def rows(self):
        return self.table_size_along_dim(0)

    def cols(self):
        return self.table_size_along_dim(1)
        
    def table_size_along_dim(self, dim):
        if len(self.cell_map) == 0:
            return 0
        return 1 + max([x[dim] for x in self.cell_map])

    def __repr__(self):
        return "Table({:d}x{:d})".format(self.table_size_along_dim(0), self.table_size_along_dim(1))

def hcat_tables(tables):
    dst = Table(tables[0].is_header_predicate)
    col_offset = 0
    for table in tables:
        for ((i, j), v) in table.cell_map.items():
            dst.cell_map[(i, j + col_offset)] = v
        col_offset += table.cols()
    return dst

class LinkNode(DocNode):
    def __init__(self, subnode):
        self.subnode = subnode
        self.target = None

    def __repr__(self):
        return "LinkNode({:s}, {:s})".format(str(self.subnode), str(self.target))

    def render_markdown(self, context):
        context.open_bracket("[")
        self.subnode.render_markdown(context.without_linebreaks())
        context.close_bracket("]")
        context.open_bracket("(")
        context.address(self.target)
        context.close_bracket(")")

    def to(self, target):
        self.target = target
        return self

class CodeBlock(DocNode):
    def __init__(self, s):
        spec.str_spec.check(s)
        self.s = s

    def __repr__(self):
        return "CodeBlock('{:s}')".format(self.s)

    def render_markdown(self, context):
        context.tokens.append(SingleBlankLineToken())
        context.tokens.append(
            StringToken("```\n" + self.s + "\n```"))
        context.tokens.append(SingleBlankLineToken())
        
    
def normalize_nodes_sub(dst, x):
    if isinstance(x, DocNode):
        dst.append(x)
    elif common.is_iterable(x) and not(common.is_stringlike(x)):
        for y in x:
            normalize_nodes_sub(dst, y)
    else:
        dst.append(TextNode(x))

def normalize_nodes(x):
    dst = []
    normalize_nodes_sub(dst, x)
    if len(dst) == 1:
        return dst[0]
    else:
        return GroupNode(dst)

def clean_blank_lines(tokens):
    dst = []
    for x in tokens:
        if isinstance(x, SingleBlankLineToken):
            if not(0 == len(dst) or isinstance(dst[-1], SingleBlankLineToken)):
                dst.append(x)
        else:
            dst.append(x)
    n = len(dst)
    for i in range(n):
        if not(isinstance(dst[n-1-i], SingleBlankLineToken)):
            return dst[0:(n-i)]
    return []

def tweak_formatting_syntax(tokens):
    n = len(tokens)
    dst = []
    at = 0
    while at < n:
        x = tokens[at]
        if 0 == len(dst):
            dst.append(x)
        else:
            last = dst[-1]
            if is_formatting_token(last, "close") and is_formatting_token(x, "open"):
                if last.features == x.features:
                    dst = dst[:-1]
                    dst.append(SyntacticBlank())
                else:
                    dst.append(SyntacticBlank())
                    dst.append(x)
            elif (is_formatting_token(last, "close") and is_bracket_token(x, "open")) or (is_bracket_token(last, "close") and is_formatting_token(x, "open")):
                dst.append(SyntacticBlank())
                dst.append(x)
            else:
                dst.append(x)
                
        at += 1
    return dst
        

def string_from_markdown_tokens(tokens):
    tokens = tweak_formatting_syntax(clean_blank_lines(tokens))
    dst = ""
    for token in tokens:
        dst += token.render()
    return dst

def render_markdown_with_context(x, context):
    normalize_nodes(x).render_markdown(context)
    return string_from_markdown_tokens(context.tokens)

def render_markdown(*x):
    return render_markdown_with_context(x, MarkdownContext())

def render_markdown_to_file(filename, *x):
    with open(filename, "w") as f:
        f.write(render_markdown_with_context(x, MarkdownContext()))


### API    

def italic(*nodes):
    return TextFeatureNode({"italic"}, normalize_nodes(nodes))

def bold(*nodes):
    return TextFeatureNode({"bold"}, normalize_nodes(nodes))

def tt(*nodes):
    return TextFeatureNode({"tt"}, normalize_nodes(nodes))

def section(title, *nodes):
    return SectionNode(normalize_nodes(title), normalize_nodes(nodes))

def paragraph(*nodes):
    return ParagraphNode(normalize_nodes(nodes))

def link(*nodes):
    return LinkNode(normalize_nodes(nodes))

def markdown_table():
    return Table(markdown_header_predicate)

def code_block(s):
    return CodeBlock(s)

# To do: images, lists

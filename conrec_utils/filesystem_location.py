import pathlib

def nth_parent(p, n):
    result = p
    for i in range(n):
        result = result.parent
    return result

def is_project_root(p):
    return p.joinpath("conrec_utils", "filesystem_location.py").exists() and p.joinpath("Makefile").exists() and p.joinpath("pyproject.toml").exists()

def resolve_root_path_from_candidates(root_candidates):
    valid_roots = [p for p in root_candidates if is_project_root(p)]
    n = len(valid_roots)
    if n == 1:
        return valid_roots[0]
    elif n == 0:
        raise RuntimeError("No valid root path found among {:s}".format(root_candidates))
    else:
        raise RuntimeError("Ambiguous root path ({:s})".format(str(valid_roots)))

def local_root_path():
    return nth_parent(pathlib.Path(__file__), 2)

def installed_root_path():
    return nth_parent(pathlib.Path(__file__), 5).joinpath("src/conrec-utils")
    
def resolve_root_path():
    return resolve_root_path_from_candidates(
        [local_root_path(), installed_root_path()])

import time
from conrec_utils import common

def rate_limiter(period=1):
    last = None
    def f():
        nonlocal last
        t = time.time()
        if (last is None) or (period + last) < t:
            last = t
            return True
        return False
    return f

def compute_time_estimate(progress, elapsed_time):
    base = {"elapsed_time": elapsed_time,
            "progress": progress}
    if progress is None or progress <= 0:
        return base
    time_per_progress = elapsed_time/progress
    remaining_progress = 1.0 - progress
    remaining_time = remaining_progress*time_per_progress
    return {**base,
            "time_per_progress": time_per_progress,
            "total_time": time_per_progress,
            "remaining_progress": remaining_progress,
            "remaining_time": remaining_time}

time_segmentation_seconds_breakdown = ["weeks", 7, "days", 24, "hours", 60, "minutes", 60, "seconds"]

def segment_time(t, segmentation=time_segmentation_seconds_breakdown):
    seg = list(reversed(segmentation))
    parts = []
    x = t
    while True:
        n = len(seg)
        unit = seg[0]
        if n == 1:
            parts.append((x, unit))
            break;
        else:
            d = seg[1]
            seg = seg[2:]
            xi = int(round(x))
            y = xi % d
            x = xi // d
            if 0 < y or (len(parts) == 0 and x == 0):
                parts.append((y, unit))
        if x == 0:
            break
    return parts

def format_time_segmentation(time_parts):
    n = min(2, len(time_parts))
    return ", ".join(map(lambda p: "{:d} {:s}".format(p[0], p[1]), reversed(time_parts[-n:])))

def format_seconds(seconds):
    return format_time_segmentation(segment_time(seconds))
            
def format_time_estimate(est):
    progress = est["progress"]
    s = ""
    if progress != None:
        s = "Progress: {:d} %".format(int(round(100*progress)))
    def timeinfo(k, lab):
        if k in est:
            return "\n{:s}: {:s}".format(lab, format_seconds(est[k]))
        return ""
    return s + timeinfo("total_time", "Total") + timeinfo("elapsed_time", "Elapsed") + timeinfo("remaining_time", "Remaining")

class ProgressReporter:
    def __init__(self, total_iterations, completed_iterations = 0, elapsed = 0, skipped = 0, time_period=1):
        self.start = time.time() - elapsed
        self.total_iterations = total_iterations
        self.completed_iterations = completed_iterations
        self.rate_limiter = rate_limiter(time_period)
        self.skipped_iterations = skipped

    def set_start(self, start):
        self.start = start
        
    def end_of_iteration(self, step=1):
        self.completed_iterations += step

    def skip_iteration(self):
        self.skipped_iterations += 1

    def total_iterations_to_complete(self):
        return self.total_iterations - self.skipped_iterations
        
    def time_estimate(self):
        return compute_time_estimate(
            common.div_or_none(self.completed_iterations, self.total_iterations_to_complete()),
            time.time() - self.start)
    
    def elapsed(self):
        return time.time() - self.start

    def progress_report(self):
        total = self.total_iterations_to_complete()
        skipped_message = "" if self.skipped_iterations == 0 else " (skipped {:d} iterations)".format(self.skipped_iterations)
        return "Completed {:d} of {:d} iterations{:s}\n".format(
            self.completed_iterations, total, skipped_message) + format_time_estimate(self.time_estimate())

    def end_of_iteration_message(self, msg, step=1):
        self.end_of_iteration(step=step)
        if self.rate_limiter():
            print(msg)
            print(self.progress_report() + "\n")
            return True
        else:
            return False

    def to_data(self):
        return {"elapsed": self.elapsed(),
                "total_iterations": self.total_iterations,
                "completed_iterations": self.completed_iterations,
                "skipped_iterations": self.skipped_iterations}

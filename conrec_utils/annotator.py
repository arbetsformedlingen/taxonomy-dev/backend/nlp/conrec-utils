import csv
import conrec_utils.spec as spec

class Choice:
    def __init__(self, key, label, shortcut=None, filter_fn=lambda x: True):
        self.key = key
        self.label = label
        self.shortcut = shortcut
        self.filter_fn = filter_fn

    def is_applicable_to(self, item):
        return self.filter_fn(item)
        

choice_spec = spec.TypeSpec(Choice)
choices_spec = choice_spec.coll()
        
class ItemToAnnotate:
    def key(self):
        raise NotImplementedError("key")
    
    def text(self, annotator):
        raise NotImplementedError("textp")

    def data(self):
        raise NotImplementedError("data")

item_to_annotate_spec = spec.TypeSpec(ItemToAnnotate)
items_to_annotate_spec = item_to_annotate_spec.coll()
    
class ItemsModel:
    def get_items(self):
        raise NotImplementedError("get_items")

    def get_choices(self):
        raise NotImplementedError("get_choices")
    
    def get_item_annotation(self, key):
        raise NotImplementedError("get_item_annotation")

    def set_item_annotation(self, key, value):
        raise NotImplementedError("set_item_annotation")

    def save(self):
        raise NotImplementedError("save")

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.save()

items_model_spec = spec.TypeSpec(ItemsModel)
        
class TextItemToAnnotate(ItemToAnnotate):
    def __init__(self, key, text, data):
        self._key = key
        self._text = text
        self._data = data

    def data(self):
        return self._data

    def key(self):
        return self._key

    def text(self):
        return self._text
    

class CsvItemsModel(ItemsModel):
    def __init__(self, choices, column_to_annotate, filename):
        self.filename = filename
        self.choices = choices
        self.header = None
        self.rows = []
        
        with open(filename, "r") as f:
            reader = csv.reader(f)
            for row in reader:
                if self.header == None:
                    self.header = row
                else:
                    self.rows.append(row)

        if not(column_to_annotate in self.header):
            self.header.append(column_to_annotate)

        for i, h in enumerate(self.header):
            if h == column_to_annotate:
                self.column_index = i
                break

    def complete_row(self, row):
        n = len(self.header)
        m = len(row)
        missing = n - m
        assert(0 <= missing)
        return row + [""]*missing

    def render_label(self, row):
        row = self.complete_row(row)
        dst = ""
        for h, v in zip(self.header, row):
            dst += "\n" + h + ": " + v
        return dst

    def make_data(self, row):
        return {k:v for k, v in zip(self.header, self.complete_row(row))}
            
    def get_items(self):
        return [TextItemToAnnotate(i, self.render_label(row), self.make_data(row))
                for i, row in enumerate(self.rows)]

    def save(self):
        print("Save to {:s}".format(str(self.filename)))
        with open(self.filename, "w") as f:
            writer = csv.writer(f)
            writer.writerow(self.header)
            for row in self.rows:
                writer.writerow(self.complete_row(row))

    def get_choices(self):
        return self.choices

    def set_item_annotation(self, key, value):
        assert(isinstance(key, int))
        row = self.complete_row(self.rows[key])
        row[self.column_index] = value
        self.rows[key] = row

    def get_item_annotation(self, key):
        assert(isinstance(key, int))
        row = self.complete_row(self.rows[key])
        value = row[self.column_index]
        if value == "":
            return None
        else:
            return value

def generate_shortcut(choice_map, strings):
    for s in strings:
        for x in s.lower():
            if not(x in choice_map):
                return x
    raise RuntimeError("No shortcut found")
        
def annotate_items(items_model):
    with items_model:
        items_model_spec.check(items_model)
        items = items_to_annotate_spec.check(items_model.get_items())
        remaining = [item for item in items if None == items_model.get_item_annotation(item.key())]
        print("{:d} items remaining".format(len(remaining)))
        counter = 0
        for item in remaining:
            print("\n\n\n****** Annotate item {:d}/{:d}".format(counter+1, len(remaining)))
            choices = choices_spec.check(items_model.get_choices())

            print(item.text())
            print("\n")

            choice_map = {}
            for choice in choices:
                if choice.is_applicable_to(item):
                    if choice.shortcut == None or choice.shortcut in choice_map:
                        shortcut = generate_shortcut(choice_map, [choice.key, choice.label])
                    else:
                        shortcut = choice.shortcut
                    print("{:s}) {:s}".format(shortcut, choice.label))
                    choice_map[shortcut] = choice.key
            cn = len(choice_map)
            if cn == 0:
                raise RuntimeError("No choices for item: \n" + item.text())
            elif cn == 1:
                for v in choice_map.values():
                    choice_key = v
                    break
            else:
                k = None
                while not(k in choice_map):
                    k = input("\nChoice? ")
                choice_key = choice_map[k]
            assert(choice_key in {choice.key for choice in choices})
            items_model.set_item_annotation(item.key(), choice_key)
            counter += 1
    

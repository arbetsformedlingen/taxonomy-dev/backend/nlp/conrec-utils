import importlib
import re
import conrec_utils.job_ads.types as types; importlib.reload(types)
from pathlib import Path
import requests
import urllib
from urllib.parse import urlparse
import os
from contextlib import ExitStack, closing, nullcontext
import shutil
import zipfile
from copy import copy
import conrec_utils.json_utils as json_utils

year_and_suffix_pattern = re.compile("([0-9]{4})(.*)\.zip")
esc_chars = "(){}[]&^%$#!?*.+"

def read_int(src, k, default_value):
    x = src.get(k)
    if x != None:
        try:
            return int(x)
        except Exception as e:
            print(f"Failed to convert value {x} of type {type(x)} to int, return {default_value}.")
            return default_value

def escape_regex(s):
    dst = s
    for c in esc_chars:
        dst = dst.replace(c, "\\{:s}".format(c))
    return dst

def find_urls_in_src(pattern, text):
    normalized_text = text.lower()

def _scrape_index(djs_url):
    index_url = f"{djs_url}/annonser/historiska/index.html"
    expected_suffix = "/index.html"
    assert(index_url.endswith(expected_suffix))
    response = requests.get(index_url)
    if not response.ok:
        raise RuntimeError("Failed to fetch index")
    pref = escape_regex('/annonser/historiska/')
    pattern_str = f"{pref}\w+.zip"
    pattern = re.compile(pattern_str)
    dst = []
    s = response.content.decode("utf-8")
    for x in pattern.finditer(s):
        dst.append(f"{djs_url}{s[x.start():x.end()]}")
    return reversed(dst)

class JobadsFilenameInfo:
    """This class parses a filename and extracts `year` and `suffix` as instance variables."""
    def __init__(self, filename):
        self.year = None
        self.suffix = None
        result = year_and_suffix_pattern.match(filename)
        if result != None:
            (year, suffix) = result.groups()
            self.year = int(year)
            if 0 < len(suffix):
                self.suffix = suffix

    def data(self):
        return {k:v
                for (k, v) in [("year", self.year),
                               ("suffix", self.suffix)]
                if v is not None}

    def is_entire_year(self):
        return self.year is not None and self.suffix is None

class JobAdsDownloadableFile(types.IJobAdSource):
    """This represents a single file to download. """
    def __init__(self, root_path, url):
        assert(isinstance(root_path, Path))
        self._root_path = root_path
        self._url = url
        self._path = Path(urlparse(url).path)
        self._unpack_to_dir = True

    def source_url(self): # override
        return self._url
        
    def filename_info(self):
        return JobadsFilenameInfo(Path(urlparse(self._url).path).name)
        
    def local_dir(self):
        return self._root_path.joinpath("downloaded", self._path.stem)

    def unpacked_path(self):
        return self.local_dir().joinpath("unpacked")
        
    def download_path(self):
        return self.local_dir().joinpath(self._path.name)

    def _download(self):
        os.makedirs(self.local_dir(), exist_ok=True)
        dst_path = self.download_path()
        print(f"Downloading to '{dst_path}'")
        
        def cleanup():
            os.remove(dst_path)

        # https://stackoverflow.com/questions/7243750/download-file-from-web-in-python-3
        with ExitStack() as stack, urllib.request.urlopen(
                self._url) as response, open(dst_path, 'wb') as out_file:
            stack.callback(cleanup)
            shutil.copyfileobj(response, out_file)
            stack.pop_all()

    def _unzip(self):
        assert(self.is_downloaded())
        dst_path = self.unpacked_path()
        def cleanup():
            shutil.rmtree(dst_path, ignore_errors=True)
        with ExitStack() as stack, zipfile.ZipFile(self.download_path(), 'r') as zip_ref:
            stack.callback(cleanup)
            os.makedirs(dst_path, exist_ok=True)
            zip_ref.extractall(dst_path)
            stack.pop_all()            

    def is_downloaded(self):
        return self.download_path().exists()

    def is_unpacked(self):
        return self.unpacked_path().exists()

    def clean(self):
        if self.is_unpacked():
            shutil.rmtree(self.unpacked_path())
        if self.is_downloaded():
            os.remove(self.download_path())

    def prepare(self):
        """Downloads and unzips the file if necessary"""
        if self.is_unpacked():
            return
        
        if not(self.is_downloaded()):
            self._download()
        if self._unpack_to_dir and not(self.is_unpacked()):
            self._unzip()

    def is_ready(self):
        return self.is_unpacked()

    def _preprocess(self, ad0):
        ad = copy(ad0)
        ad["original_id"] = str(ad0.get("original_id", ""))
        ad["number_of_vacancies"] = read_int(ad0, "number_of_vacancies", 1)
        return ad
        
    def load_raw_ads(self):
        print(f"Load ads from '{self.download_path()}'")
        self.prepare()
        if self._unpack_to_dir:
            files = os.listdir(self.unpacked_path())
            for f in files:
                full_path = self.unpacked_path().joinpath(f)
                ads = json_utils.read_json(full_path)
                for ad in ads:
                    yield self._preprocess(ad)
        else:
            with zipfile.ZipFile(self.download_path(), "r") as zipref:
                for compressed_filename in  zipref.namelist():
                    with zipref.open(compressed_filename, "r") as f:
                        print(f"  Read compressed file {compressed_filename}")
                        ads = json.load(f)
                        for ad in ads:
                            yield self._decorate(ad)

    def __repr__(self):
        return f"JobAdsDownloadableFile('{self._url}')"

def _sort_by_year_and_suffix(dfile):
    info = dfile.filename_info()
    suffix = "0" if info.suffix is None else "1" + info.suffix
    return (info.year, suffix)

def dataset_name_from_url(url):
    i = url.find("://")
    name = url
    if 0 <= i:
        name = name[(i+3):]
    return name.replace(".", "_")


class DjsJobAdFileSource(types.IJobAdFileSource):
    def __init__(self, root_path, root_url):
        self._root_path = root_path
        self._root_url = root_url

    def __repr__(self):
        return f"HistoricalJobAdsRepository('{self._root_url}' downloaded to '{self._root_path}')"

    def _downloadable_file_from_url(self, url):
        return JobAdsDownloadableFile(self._root_path, url)

    def _list_urls(self):
        return _scrape_index(self._root_url)
    
    def list_files(self):
        return sorted([self._downloadable_file_from_url(url)
                       for url in self._list_urls()],
                      key=_sort_by_year_and_suffix)

if False: # List the files
    cfg = DjsJobAdFileSource(Path("/tmp/jobads"), "https://data.jobtechdev.se")
    files = cfg.list_files()
    print(f"There are {len(files)} files to download:")
    for f in files:
        print(f"  * {f}")
    f = files[0]

if False: # Download a file    
    to_download = [f for f in files if not(f.is_ready())]
    if to_download:
        f = to_download[0]
        print(f"Next file to download: {f}")
        f.prepare()
    else:
        print("All downloaded")

if False: # Iterate over the jobads of a file
    downloaded = [f for f in files if f.is_ready()]
    if downloaded:
        f = downloaded[0]

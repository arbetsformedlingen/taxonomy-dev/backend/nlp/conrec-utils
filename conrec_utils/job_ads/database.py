import conrec_utils.common as common
import conrec_utils.job_ads.types as types
import os
from copy import copy
from pathlib import Path
import sqlite3
import random
from contextlib import ExitStack, closing, nullcontext

class WithDbAccess:
    def __init__(self, sqlite_repo):
        self._sqlite_repo = sqlite_repo
        self._con = None
        self._item_mapper = common.identity

    def with_item_mapper(self, f):
        self._item_mapper = f
        return self
        
    def enter_db_access(self, con):
        raise NotImplementedError("enter_db_access")

    def query_jobads(self, expr):
        return (item
                for x in self._con.execute(expr)
                for item in [self._item_mapper(field_mapping.deserialize_jobad_data(x))]
                if item is not None)
    
    def __enter__(self):
        self._con = self._sqlite_repo.db_connection()
        return self.enter_db_access(self._con)

    def __exit__(self, *args):
        self._con.__exit__(*args)

        
class SequentialDbAccess(WithDbAccess):
    def __init__(self, sqlite_repo, access_config):
        super().__init__(sqlite_repo)
        self._access_config = access_config

    def sql_expr(self, selection="*"):
        c = self._access_config

        source_urls = c.get("source_urls")
        include_ids = c.get("include_ids")
        exclude_ids = c.get("exclude_ids")
        
        src_expr = f"dataset_url IN {sql_set_expr(source_urls)}" if source_urls != None else None
        include_expr = f"id IN {sql_set_expr(include_ids)}" if include_ids != None else None
        exclude_expr = f"id NOT IN {sql_set_expr(exclude_ids)}" if exclude_ids != None else None
        conditions = [c
                      for c in [src_expr, include_expr, exclude_expr]
                      if c is not None]
        cond_expr = "WHERE " + " AND ".join(conditions) if conditions else ""
        expr = f"SELECT {selection} FROM JobAds {cond_expr}"
        return expr

    def enter_db_access(self, con):
        return self.query_jobads(self.sql_expr())
    
    def count(self):
        with self._sqlite_repo.db_connection() as con:
            result = con.execute(self.sql_expr('count(id)'))
            return list(result)[0][0]
        
class RandomDbAccess(WithDbAccess):
    def __init__(self, sqlite_repo):
        super().__init__(sqlite_repo)

    def __getitem__(self, jobad_id):
        return list(self.query_jobads(f"SELECT * FROM JobAds WHERE id in {sql_set_expr([jobad_id])}"))[0]
        
    def enter_db_access(self, con):
        return self
        
class SqlTypeRepr:
    def __init__(self, pytype, reptype, sql_type_string, default_value):
        self.pytype = pytype
        self.reptype = reptype
        self.sql_type_string = sql_type_string
        self.default_value = default_value

    def serialize(self, x):
        if not(x is None):
            if not(isinstance(x, self.pytype)):
                raise RuntimeError(f"Failed to serialize {x} of type {type(x)} to {self.pytype}")
            return self.reptype(x)

    def deserialize(self, x):
        return self.pytype(x)

str_repr = SqlTypeRepr(str, str, "string", "")
int_repr = SqlTypeRepr(int, int, "integer", 0)
bool_repr = SqlTypeRepr(bool, int, "integer", False)
float_repr = SqlTypeRepr(float, float, "real", 0.0)

def _deserialize_into(dst, path, src):
    k = path[0]
    if len(path) == 1:
        dst[k] = src
    else:
        if not(k in dst):
            dst[k] = {}
        _deserialize_into(dst[k], path[1:], src)

class MappedField:
    def __init__(self, path, type_repr):
        assert(isinstance(type_repr, SqlTypeRepr))
        self._path = common.basic_flatten(path)
        self._type_repr = type_repr
        self._dec = None
        self._default_value = type_repr.default_value

    def __repr__(self):
        return f"MappedField({', '.join(self._path)})"
        
    def decorate_with(self, dec):
        dst = copy(self)
        dst._dec = dec
        return dst

    def column_name(self):
        return "_".join(self._path)

    def column_initialization(self):
        dec = "" if self._dec is None else f" {self._dec}"
        return f"{self.column_name()} {self._type_repr.sql_type_string}{dec}"

    def serialize(self, source):
        e = self.extract(source)
        x = e[0] if 0 < len(e) else self._default_value
        try:
            return self._type_repr.serialize(x)
        except Exception as e:
            print(f"Failed to serialize for field {self._path}")
            raise e

    def extract(self, source):
        path = self._path
        for k in self._path:
            if not(k in source):
                return []
            source = source[k]
        return [source]

    def has(self, source):
        return 0 < len(self.extract(source))

    def deserialize_into(self, dst, src):
        _deserialize_into(dst, self._path, src)

class FieldMapping:
    def __init__(self, field_mapping):
        self.field_mapping = field_mapping

    def value_presence_mask(self, src_array):
        mask = [True for field in self.field_mapping]
        for src in src_array:
            for i, field in enumerate(self.field_mapping):
                if not(field.has(src)):
                    mask[i] = False
        return mask

    def report_value_presence(self, src_array):
        mask = self.value_presence_mask(src_array)
        for field, e in zip(self.field_mapping, mask):
            print(f"{field}: {'OK' if e else 'MISSING'}")
        
    def init_columns_sql(self):
        return self.mapped_field_expr(lambda field: field.column_initialization())

    def mapped_field_expr(self, f):
        return ",\n".join([f(field) for field in self.field_mapping])

    def column_names_expr(self):
        return self.mapped_field_expr(lambda field: field.column_name())

    def placeholders_expr(self):
        return self.mapped_field_expr(lambda field: "?")

    def serialize_jobad_data(self, src):
        return [field.serialize(src) for field in self.field_mapping]

    def deserialize_jobad_data(self, src):
        dst = {}
        for field, x in zip(self.field_mapping, src):
            field.deserialize_into(dst, x)
        return dst

    def filter(self, pred):
        return FieldMapping([f for f in self.field_mapping if pred(f)])

    def select_by_column_names(self, cnames):
        m = {f.column_name():f for f in self.field_mapping}
        return FieldMapping([m[cname] for cname in cnames])

field_mapping = FieldMapping([
    MappedField(["access_to_own_car"], bool_repr),
    MappedField(["application_deadline"], str_repr),
    MappedField(["description", "text"], str_repr),
    MappedField(["description", "conditions"], str_repr),
    #MappedField(["detected_language"], str_repr),
    #MappedField(["driving_license", "concept_id"], str_repr),
    #MappedField(["driving_license_required"], bool_repr),
    MappedField(["duration", "concept_id"], str_repr),
    MappedField(["employer", "url"], str_repr),
    MappedField(["employer", "organization_number"], str_repr),
    MappedField(["employer", "name"], str_repr),
    MappedField(["employer", "workplace"], str_repr),
    MappedField(["employment_type", "concept_id"], str_repr),
    MappedField(["experience_required"], bool_repr),
    MappedField(["external_id"], str_repr),
    MappedField(["franchise"], bool_repr),
    MappedField(["headline"], str_repr),
    MappedField(["hire_work_place"], bool_repr),
    MappedField(["id"], str_repr).decorate_with("primary key"),
    MappedField(["larling"], bool_repr),
    MappedField(["last_publication_date"], str_repr),
    MappedField(["logo_url"], str_repr),
    MappedField(["number_of_vacancies"], int_repr),
    MappedField(["occupation", "concept_id"], str_repr),
    MappedField(["occupation_field", "concept_id"], str_repr),
    MappedField(["occupation_group", "concept_id"], str_repr),
    MappedField(["open_for_all"], bool_repr),
    MappedField(["original_id"], str_repr),
    MappedField(["publication_date"], str_repr),
    MappedField(["remote_work"], bool_repr),
    MappedField(["removed"], bool_repr),
    MappedField(["removed_date"], str_repr),
    MappedField(["salary_description"], str_repr),
    MappedField(["salary_type", "concept_id"], str_repr),
    MappedField(["scope_of_work", "min"], int_repr),
    MappedField(["scope_of_work", "max"], int_repr),
    MappedField(["source_type"], str_repr),
    MappedField(["timestamp"], int_repr),
    MappedField(["trainee"], bool_repr),
    MappedField(["webpage_url"], str_repr),
    MappedField(["working_hours_type", "concept_id"], str_repr),
    MappedField(["workplace_address", "municipality"], str_repr),
    MappedField(["workplace_address", "municipality_concept_id"], str_repr),
    MappedField(["workplace_address", "region_concept_id"], str_repr),
    MappedField(["workplace_address", "country_concept_id"], str_repr),
    MappedField(["dataset_url"], str_repr),
])

random_samples_mapping = field_mapping.select_by_column_names(["id", "dataset_url", "occupation_concept_id"])

def sql_set_expr(items):
    return "(" + ', '.join([f"'{s}'" for s in items]) + ")"

class SqliteJobAdsDatabase(types.IDatabase):
    def __init__(self, db_path):
        self._db_path = db_path

    def db_path(self):
        return self._db_path
        
    def db_connection(self):
        """Returns a database connection"""
        os.makedirs(Path(self._db_path).parent, exist_ok=True)
        return sqlite3.connect(self._db_path)

    def wrap_connection(self, con):
        """If con is None, it creates a database connectio, otherwise it returns the one passed in."""
        if con is None:
            return self.db_connection()
        else:
            return nullcontext(con)

    def is_initialized(self, con=None):
        """Test if there is a jobads table in the databse"""
        with self.wrap_connection(con) as con:
            table_names = {name
                           for result_rec in con.execute("SELECT name FROM sqlite_master")
                           for name in result_rec}
            expected_table_names = ["JobAds"]
            for expected_table_name in expected_table_names:
                if expected_table_name not in table_names:
                    return False
            return True

    def add_source_url(self, url, con=None):
        with self.wrap_connection(con) as con:
            con.executemany("INSERT OR REPLACE INTO JobAdSources(url) VALUES(?)", [[url]])

    def _count(self, table_name, column_name):
        with self.db_connection() as con:
            if self.is_initialized():
                result = con.execute(f"SELECT COUNT({column_name}) FROM {table_name}")
                return list(result)[0][0]
        return 0
            
    def get_source_url_count(self, con=None):
        """Count the number of jobads in the database"""
        return self._count("JobAdSources", "url")
        
    def count(self, con=None):
        """Count the number of jobads in the database"""
        return self._count("JobAds", "id")

    def get_source_urls(self, con=None):
        if self.is_initialized(con):
            with self.wrap_connection(con) as con:
                return {x[0] for x in con.execute("SELECT DISTINCT url FROM JobAdSources")}
        return set()
    
    def initialize_db(self, con=None):
        """Initialize the database with the tables"""
        with self.wrap_connection(con) as con:
            con.execute(f"CREATE TABLE IF NOT EXISTS JobAds ({field_mapping.init_columns_sql()});")
            con.execute(f"CREATE TABLE IF NOT EXISTS JobAdSources (url text primary key)")
            con.execute(f"CREATE INDEX IF NOT EXISTS idx_source_url ON JobAdSources(url);")
            con.execute(f"CREATE INDEX IF NOT EXISTS idx_dataset_url ON JobAds(dataset_url);")
            assert(self.is_initialized(con))

    def reset_db(self, con=None):
        """Delete all data from the database"""
        with self.wrap_connection(con) as con:
            con.execute("""DROP TABLE IF EXISTS JobAds""")
            con.execute("""DROP TABLE IF EXISTS JobAdSources""")

    def count(self):
        """Count the number of jobads in the database"""
        with self.db_connection() as con:
            if self.is_initialized(con):
                result = con.execute("SELECT COUNT(id) FROM JobAds")
                return list(result)[0][0]
        return 0

    def insert_jobads(self, jobads_to_add, con=None):
        """Add or update jobads"""
        with self.wrap_connection(con) as con:
            column_names = field_mapping.column_names_expr()
            placeholders = field_mapping.placeholders_expr()
            con.executemany(
                f"INSERT OR REPLACE INTO JobAds({column_names}) VALUES({placeholders});",
                (field_mapping.serialize_jobad_data(x) for x in jobads_to_add))

    def dataset_urls(self, con=None):
        """Return the set of all dataset urls present in the dataset"""
        with self.wrap_connection(con) as con:
            if self.has_jobads_table(con):
                return {row[0] for row in con.execute("SELECT DISTINCT dataset_url FROM JobAds")}
            else:
                return set()

    def random_samples(self, submapping, dataset_urls, n):
        """Returns a random sample of jobads"""
        with self.db_connection() as con:
            url_expr = sql_set_expr(dataset_urls)
            result = con.execute(f"SELECT {submapping.column_names_expr()} FROM JobAds WHERE dataset_url in {url_expr} ORDER BY RANDOM() LIMIT {n};")
            return [submapping.deserialize_jobad_data(sample)
                    for sample in result]

    def add_job_ad_sources(self, job_ad_sources):
        types.job_ad_source_spec.coll().check(job_ad_sources)
        with self.db_connection() as con:
            self.initialize_db(con)
            existing_urls = self.get_source_urls(con)

            existing_url_set = set(existing_urls)
            remaining_urls = [src
                              for src in job_ad_sources
                              if src.source_url() not in existing_url_set]
            if len(remaining_urls) == 0:
                return
            
            n = len(job_ad_sources)
            for i, src in enumerate(job_ad_sources):
                progress = f"{i+1}/{n}"
                url = src.source_url()
                if url not in existing_urls:
                    print(f"Add ads from {url} ({progress})")
                    self.insert_jobads(src.load_ads())
                    self.add_source_url(url)
                    src.clean()
                else:
                    print(f"Already added {url} ({progress})")
                    
    def __repr__(self):
        return f"SqliteJobAdsDatabase('{self._db_path}')"

    def random_access(self):
        return RandomDbAccess(self)

    def sequential_access(self, args=None):
        return SequentialDbAccess(self, args or {})

#known_djs_urls = ['https://data.jobtechdev.se/annonser/historiska/2022q3.zip', 'https://data.jobtechdev.se/annonser/historiska/2022q2.zip', 'https://data.jobtechdev.se/annonser/historiska/2022q1.zip', 'https://data.jobtechdev.se/annonser/historiska/2022.zip', 'https://data.jobtechdev.se/annonser/historiska/2021_first_6_months.zip', 'https://data.jobtechdev.se/annonser/historiska/2021.zip', 'https://data.jobtechdev.se/annonser/historiska/2020.zip', 'https://data.jobtechdev.se/annonser/historiska/2019.zip', 'https://data.jobtechdev.se/annonser/historiska/2018.zip', 'https://data.jobtechdev.se/annonser/historiska/2017.zip', 'https://data.jobtechdev.se/annonser/historiska/2016.zip', 'https://data.jobtechdev.se/annonser/historiska/2015.zip', 'https://data.jobtechdev.se/annonser/historiska/2014.zip', 'https://data.jobtechdev.se/annonser/historiska/2013.zip', 'https://data.jobtechdev.se/annonser/historiska/2012.zip', 'https://data.jobtechdev.se/annonser/historiska/2011.zip', 'https://data.jobtechdev.se/annonser/historiska/2010.zip', 'https://data.jobtechdev.se/annonser/historiska/2009.zip', 'https://data.jobtechdev.se/annonser/historiska/2008.zip', 'https://data.jobtechdev.se/annonser/historiska/2007.zip', 'https://data.jobtechdev.se/annonser/historiska/2006.zip']


import importlib
import os
import conrec_utils.job_ads.downloader as downloader
from conrec_utils.env import Env
import conrec_utils.job_ads.database as database; importlib.reload(database)
import conrec_utils.job_ads.types as types
import conrec_utils.job_ads.dataset_split as dss; importlib.reload(dss)
from copy import copy
from contextlib import ExitStack, closing, nullcontext
import conrec_utils.spec as spec
from pathlib import Path
import conrec_utils.dataset_common as dataset_common
import conrec_utils.json_utils as json_utils


class IConfig:
    def raw_database(self):
        raise NotImplementedError("raw_database")

    def file_sources(self):
        raise NotImplementedError("file_sources")

    def datasplit_path(self):
        raise NotImplementedError("datasplit_path")

    def target_sample_distribution(self):
        raise NotImplementedError("target_sample_distribution")

    # Utility functions
    def all_files(self):
        return [f
                for fs in self.file_sources()
                for f in fs.list_files()]

class DefaultConfig(IConfig):
    def __init__(self, env):
        self._env = env
        self._djs_url = "https://data.jobtechdev.se"

    def raw_database(self):
        return database.SqliteJobAdsDatabase(
            self._root_path().joinpath("jobads.db"))

    def file_sources(self):
        full_source = downloader.DjsJobAdFileSource(
            self._root_path(), self._djs_url)
        def pred(f):
            info = f.filename_info()
            year = info.year
            return info.is_entire_year() and 2017 <= year and year <= 2023
        return [types.filter_job_ad_files(pred, full_source)]
        
    def datasplit_path(self):
        return self._env.conrec_root_path.joinpath(
            "datasets", "historical-job-ads", "datasplit.csv")

    def target_sample_distribution(self):
        return {dataset_common.test.label(): 10000,
                dataset_common.test0.label(): 10000,
                dataset_common.validation.label(): 10000}

    def default():
        return DefaultConfig(Env.default())

    # Private
    def _root_path(self):
        name = downloader.dataset_name_from_url(self._djs_url)
        return self._env.cache_path.joinpath("historical_job_ads", name)
        
class Setup:
    def __init__(self, config):
        assert(isinstance(config, IConfig))
        self._config = config

    def populated_database(self):
        db = self._config.raw_database()
        db.add_job_ad_sources(self._config.all_files())
        return db

    def load_datasplit(self):
        return dss.DatasetSplit.load(self._config.datasplit_path())
    
    def generate_datasplit(self):
        urls = [f.source_url() for f in self._config.all_files()]
        init_datasplit = dss.DatasetSplit(urls, {})
        datasplit = dss.extend_dataset_split_with_samples(
            init_datasplit,
            self._config.target_sample_distribution(),
            self.populated_database())
        datasplit.write_to_csv(self._config.datasplit_path())

    def random_access(self):
        return self.populated_database().random_access()
    
    def access_dataset(self, label=None):
        split = self.load_datasplit()
        args = {"source_urls": split.source_urls()}
        if label is not None:
            if label == split.default_label():
                args["exclude_ids"] = split.labeled_ids()
            else:
                args["include_ids"] = split.filter_ids_by_label(label)
        return self.populated_database().sequential_access(args)

default_cfg = DefaultConfig.default()
setup = Setup(default_cfg)

if False:
    split = setup.load_datasplit()
    labels = split.labels()
    print(f"Labels: {labels}")

    for label in labels:
        with setup.access_dataset(label) as ds:
            counter = 0
            for x in ds:
                counter += 1
            print(f"  * {label}: {counter}")


if False:
    db = setup.populated_database()
    ids = []
    with db.sequential_access() as access:
        for x in access:
            ids.append(x["id"])

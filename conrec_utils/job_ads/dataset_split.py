import csv
import conrec_utils.spec as spec
import conrec_utils.job_ads.types as types
import conrec_utils.dataset_common as dataset_common
import conrec_utils.job_ads.database as db
import conrec_utils.common as common
import os
from copy import copy
from pathlib import Path
import random
from jobtech_taxonomy_library_python.generated.defs import taxonomy_version_map

random_samples_mapping = db.field_mapping.select_by_column_names(["id", "dataset_url", "occupation_concept_id"])

def _extend_to_count(dst_set0, src_set, n):
    dst_set = copy(dst_set0)
    for x in src_set:
        if n is None or len(dst_set) < n:
            dst_set.add(x)
        else:
            break
    return dst_set

def is_valid_sample(sample):
    concept_id = sample["occupation"]["concept_id"]
    try:
        concept = taxonomy_version_map.latest_concept(concept_id)
        return True
    except StopIteration:
        print(f"Missing occupation for jobad {sample['id']}")
        return False


def extend_dataset_split_with_samples(ds_split, target_distribution, database):
    distrib_to_add = _distrib_diff(target_distribution, ds_split.distribution())
    dst_id_label_map = copy(ds_split.id_label_map())
    ids = set(dst_id_label_map.keys())

    counter = 0
    for v in distrib_to_add.values():
        counter += v

    gen_ids = set()

    while len(gen_ids) < counter:
        for sample in database.random_samples(random_samples_mapping, ds_split.source_urls(), counter - len(gen_ids)):
            sample_id = sample["id"]
            if not(sample_id in ids) and is_valid_sample(sample):
                gen_ids.add(sample_id)

    gen_ids = list(gen_ids)
    random.shuffle(gen_ids)

    for label, n in distrib_to_add.items():
        for i in gen_ids[0:n]:
            dst_id_label_map[i] = label
        gen_ids = gen_ids[n:]
    assert(len(gen_ids) == 0)

    return ds_split.with_id_label_map(dst_id_label_map)


id_label_map_spec = spec.MapOfSpec(types.id_spec, dataset_common.label_spec)

class DatasetSplit:
    def __init__(self, source_urls, id_label_map, default_label=None):
        types.url_spec.coll().check(source_urls)
        id_label_map_spec.check(id_label_map)
        
        self._source_urls = set(source_urls)
        self._id_label_map = id_label_map
        self._default_label = spec.str_spec.check(common.or_some(dataset_common.train.label(), default_label))

        label_set = {label for i, label in id_label_map.items()}
        assert(self._default_label not in label_set)

    def labels(self):
        dst = set([self._default_label])
        for i, label in self._id_label_map.items():
            dst.add(label)
        return dst
        
    def default_label(self):
        return self._default_label

    def labeled_ids(self):
        return set(self._id_label_map.keys())
        
    def from_urls(urls):
        return DatasetSplit(urls, {})

    def splitkey_set(self):
        return {label
                for group in [self._id_label_map.values(), [self._default_label]]
                for label in group}

    def extend_source_urls_to_count(self, urls, max_count):
        dst = copy(self)
        dst._source_urls = _extend_to_count(self._source_urls, urls, max_count)
        return dst

    def with_id_label_map(self, id_label_map):
        dst = copy(self)
        dst._id_label_map = id_label_map
        return dst

    def source_urls(self):
        return self._source_urls

    def filter_ids_by_label(self, label):
        return {k for k, v in self._id_label_map.items() if v == label}

    def id_label_map(self):
        return self._id_label_map
    
    def distribution(self):
        dst = {}
        for i, label in self._id_label_map.items():
            dst[label] = dst.get(label, 0) + 1
        return dst

    def __repr__(self):
        return f"DatasetSplit({len(self._source_urls)} urls, {len(self._id_label_map)} samples)"
        
    def write_to_csv(self, path0):
        path = Path(path0)
        os.makedirs(path.parent, exist_ok=True)
        with open(path, "w") as f:
            writer = csv.writer(f)
            writer.writerow(["property", "value"])
            writer.writerow(["default-label", self._default_label])
            url_rows = [["source-url", url]
                        for url in self._source_urls]
            sample_rows = [[label, k] for k, label in self._id_label_map.items()]
            all_rows = [row
                        for rows in [url_rows, sorted(sample_rows)]
                        for row in rows]
            
            for row in all_rows:
                writer.writerow(row)

    def empty():
        return DatasetSplit(set(), {})

    def label(self, sample):
        if sample["dataset_url"] in self._source_urls:
            return self._id_label_map.get(sample["id"], self._default_label)

    def read_from_csv(path):
        default_label = dataset_common.train.label()
        source_urls = set()
        id_label_map = {}
        with open(path, "r") as f:
            reader = csv.reader(f)
            header = next(reader)
            if header != ["property", "value"]:
                raise RuntimeError(f"Invalid header {header}")
            for row in reader:
                prop, val = row
                if prop == "source-url":
                    source_urls.add(val)
                elif prop == "default-label":
                    default_label = val
                else:
                    id_label_map[val] = prop
            return DatasetSplit(source_urls, id_label_map, default_label)

    def load(path):
        if path.exists():
            return DatasetSplit.read_from_csv(path)
        else:
            return DatasetSplit.empty()

    def _data_tuple(self):
        return (self._source_urls, self._id_label_map, self._default_label)
        
    def __eq__(self, other):
        if self is other:
            return True
        elif isinstance(other, DatasetSplit):
            return self._data_tuple() == other._data_tuple()
        else:
            return False
        
def _distrib_diff(a, b):
    ks = {k for x in [a, b] for k in x}
    return {k:n
            for k in ks
            for n in [a.get(k, 0) - b.get(k, 0)]
            if 0 < n}               


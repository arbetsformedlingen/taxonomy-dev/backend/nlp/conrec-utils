import conrec_utils.spec as spec
from copy import copy

id_spec = spec.str_spec
url_spec = spec.str_spec

class IDatabase:
    def random_access(self):
        raise NotImplementedError("random_access")

    def sequential_access(self, args=None):
        raise NotImplementedError("sequential_access")

def normalize_id(ad_id):
    prefix = ""
    if isinstance(ad_id, str):
        prefix = "s"
    elif isinstance(ad_id, int):
        prefix = "i"
    else:
        prefix = "o"
    return f"{prefix}{ad_id}"

def decorate_ad(ad0, dataset_url):
    ad = copy(ad0);
    ad["dataset_url"] = dataset_url
    ad["id"] = normalize_id(ad0["id"])
    return ad


class IJobAdSource:
    def source_url(self):
        raise NotImplementedError("identifier")

    def load_raw_ads(self):
        raise NotImplementedError("load_ads")

    def load_ads(self):
        src_url = self.source_url()
        return [decorate_ad(ad, src_url)
                for ad in self.load_raw_ads()]

    def clean(self):
        None

job_ad_source_spec = spec.TypeSpec(IJobAdSource)
    
class IJobAdFileSource:
    def list_files(self):
        raise NotImplementedError("get_files")

job_ad_file_source_spec = spec.TypeSpec(IJobAdFileSource)    

class FilteredJobAdFileSource(IJobAdFileSource):
    def __init__(self, predicate_fn, src):
        self._predicate_fn = predicate_fn
        self._src = src

    def list_files(self):
        return [job_ad_source_spec.check(f)
                for f in self._src.list_files()
                if self._predicate_fn(f)]

def filter_job_ad_files(p, src):
    return FilteredJobAdFileSource(p, src)

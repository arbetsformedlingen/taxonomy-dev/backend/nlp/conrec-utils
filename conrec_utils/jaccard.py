from conrec_utils import common

class JaccardIndex(common.ComparableByTuple):
    def __init__(self, a_minus_b, intersection_size, b_minus_a):
        assert(0 <= a_minus_b)
        assert(0 <= intersection_size)
        assert(0 <= b_minus_a)
        assert(isinstance(a_minus_b, int))
        assert(isinstance(intersection_size, int))
        assert(isinstance(b_minus_a, int))

        union_size = a_minus_b + intersection_size + b_minus_a
        
        assert(intersection_size <= union_size)
        self.a_minus_b = a_minus_b
        self.b_minus_a = b_minus_a
        self.intersection_size = intersection_size
        self.union_size = union_size
        self.value = intersection_size/union_size if 0 < union_size else None

        # The rational for a score of 1 if both are empty is that they are equal, meaning full score.
        self.score = 1 if self.value == None else self.value
        self.distance = 1.0 - self.score

    def data(self):
        return {"a_minus_b": self.a_minus_b,
                "intersection": self.intersection_size,
                "b_minus_a": self.b_minus_a}

    def from_data(data):
        return JaccardIndex(data["a_minus_b"], data["intersection"], data["b_minus_a"])

        
    def get_comparison_tuple(self):
        return (self.a_minus_b, self.intersection_size, self.b_minus_a)
        
    def __repr__(self):
        return "JaccardIndex({:d}/({:d} + {:d} + {:d}) = {:s})".format(
            self.intersection_size,
            self.a_minus_b,
            self.intersection_size,
            self.b_minus_a,
            str(self.value)) if self.value != None else "JaccardIndex(undefined)"

    def rendered_fraction(self):
        return "{:d}/{:d}".format(self.intersection_size, self.union_size)

    def rendered_percentage(self):
        return "{:d}%".format(int(round(100*self.score)))

    def __add__(self, other):
        if isinstance(other, JaccardIndex):
            return JaccardIndex(
                self.a_minus_b + other.a_minus_b,
                self.intersection_size + other.intersection_size,
                self.b_minus_a + other.b_minus_a)
        else:
            raise ValueError("other is not a JaccardIndex")

    def __radd__(self, other):
        return self.__add__(other)

zero_jaccard_index = JaccardIndex(0, 0, 0)

class JaccardAnalysis:
    def __init__(self, A, B):
        self.A = set(A)
        self.B = set(B)
        self.union = self.A.union(self.B)
        self.intersection = self.A.intersection(self.B)
        self.A_minus_B = self.A.difference(self.B)
        self.B_minus_A = self.B.difference(self.A)
        self.jaccard_index = JaccardIndex(len(self.A_minus_B), len(self.intersection), len(self.B_minus_A))

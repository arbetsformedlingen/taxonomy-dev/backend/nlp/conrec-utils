import requests
from conrec_utils import jae_synonyms

default_settings = {"address": "https://jobad-enrichments-test-api.jobtechdev.se",
                    "save-period": 1000,
                    "step": 100,
                    "retry-count": 3,
                    "jae_parameters": {
                        "include_terms_info": True,
                        "include_sentences": True,
                        "sort_by_prediction_score": "NOT_SORTED"
                    }}


def preclean_jae_text(s):
    problematic_chars = {"\u200b": "",
                         "\u201d": "",
                         "\u2013": "-",
                         "\u2019": "'"}

    
    
    for (k, v) in problematic_chars.items():
        s = s.replace(k, v)
    return s

def preclean_jae_document_input(docinput):
    ks = {"doc_headline", "doc_text"}
    return {k:(preclean_jae_text(v) if k in ks else v) for (k,v) in docinput.items()}

def jae_document_input(doc_id, headline, text):
    assert(isinstance(doc_id, str))
    assert(isinstance(headline, str))
    assert(isinstance(text, str))
    
    return {"doc_id": doc_id,
            "doc_headline": headline,
            "doc_text": text}

def charset(strings):
    assert(isinstance(strings, list))
    dst = set()
    for s in strings:
        assert(isinstance(s, str))
        for c in s:
            dst.add(c)
    return "".join(sorted(dst))

def is_jae_document_input(x):
    if not(isinstance(x, dict)):
        return False

    for k in ["doc_id", "doc_headline", "doc_text"]:
        if not(isinstance(x.get(k), str)):
            return False

    return True

synonym_type2singular = {"competencies": "competence",
                         "occupations": "occupation",
                         "traits": "trait",
                         "geos": "geo"}

def flat_candidates(enriched_document):
    return [{**candidate,
             "synonym_type": synonym_type,
             "synonym_key": jae_synonyms.make_synonym_key(synonym_type, candidate["concept_label"])}
            for (k, candidates) in enriched_document["enriched_candidates"].items()
            for candidate in candidates
            for synonym_type in [synonym_type2singular[k]]]

class JobadEnricher:
    def __init__(self, settings={}):
        self.settings = {**default_settings, **settings}

    def enrich_documents(self, src_docs):
        assert(isinstance(src_docs, list))
        
        jae_address = self.settings["address"]

        for doc in src_docs:
            if not(is_jae_document_input(doc)):
                raise RuntimeError("Invalid document input for JAE")
        
        jae_post_data = {
            **self.settings["jae_parameters"],
            "documents_input": src_docs,
        };
    
        for i in range(self.settings["retry-count"]):
            jae_response = requests.post(jae_address + "/enrichtextdocuments", json=jae_post_data)
            if jae_response.status_code == 200:
                return jae_response.json()
            else:
                print("Failed JAE request")
        raise RuntimeError("Failed to enrich")

    def __repr__(self):
        return "JobadEnricher(address={:s})".format(self.settings["address"])
        
## Example

# e = JobadEnricher()
# result = e.enrich_documents([jae_document_input("mjao", "Hej", "Vi söker en javautvecklare")])
# candidates = flat_candidates(result[0])
# print("Good so far")

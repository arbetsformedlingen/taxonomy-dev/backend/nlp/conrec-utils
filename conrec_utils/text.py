from conrec_utils.interval import Interval

def abbreviate(s, maxlen):
    n = len(s)
    if n <= maxlen:
        return s
    else:
        return s[0:(maxlen-3)] + "..."

punctuations = ".!?"
    
class SentenceSegmenter:
    def __init__(self):
        self.s = None
        self.at = None
        self.length = None

    def normalize_index(self, i):
        return self.at if i == None else i

    def can_read(self, i=None):
        assert(self.s != None)
        return self.normalize_index(i) < self.length

    def is_whitespace(self, i=None):
        return self.can_read(i) and self.s[self.normalize_index(i)].isspace()
    
    def step(self):
        self.at += 1

    def is_long_whitespace(self, i=None):
        i = self.normalize_index(i)
        return self.is_whitespace(i) and self.is_whitespace(i+1)

    def read_punctuations(self):
        start = self.at
        while self.can_read() and (self.s[self.at] in punctuations):
            self.step()
        return start != self.at
        
    def advance_to_beginning_of_sentence(self):
        while self.is_whitespace():
            self.step()

    def advance_to_end_of_sentence(self):
        while self.can_read():
            if self.is_long_whitespace():
                break
            if self.read_punctuations():
                break
            self.step()
    
    def segment(self, s):
        self.s = s
        self.at = 0
        self.length = len(s)

        while self.can_read():
            self.advance_to_beginning_of_sentence()

            if not(self.can_read()):
                break
            
            lower = self.at
            self.advance_to_end_of_sentence()
            upper = self.at
            yield Interval(lower, upper)
            

from conrec_utils.json_utils import read_json, write_json
from pathlib import Path
import os

class JsonCache:
    def __init__(self, cache_path):
        assert(isinstance(cache_path, Path))
        self._path = cache_path
        if self._path.exists():
            self._data = read_json(self._path)
        else:
            self._data = {}

    def __enter__(self):
        return self._data

    def __exit__(self, exc_type, exc_val, exc_tb):
        os.makedirs(self._path.parent, exist_ok=True)
        write_json(self._path, self._data)

from conrec_utils import json_utils
import os

def key_from_filename(prefix, filename):
    if filename.startswith(prefix) and filename.endswith(".json"):
        return filename[len(prefix):-5]

def is_valid_key_char(x):

    if x.isalnum():
        return True

    if x in "-_$":
        return True
    
    return False
    
def is_valid_key(k):
    if not(isinstance(k, str)):
        return False

    for c in k:
        if not(is_valid_key_char(c)):
            return False
    
    return True
    
class JsonFileMap:
    # Attention! Never work with more than one JsonFileMap for the same directory
    def __init__(self, rootdir, prefix="filemap_"):
        self.prefix = prefix
        self.rootdir = rootdir
        os.makedirs(rootdir, exist_ok=True)
        self.keyset = {key for key in [key_from_filename(prefix, filename) for filename in os.listdir(rootdir)] if key!=None}

    def __repr__(self):
        return "JsonFileMap(count={:d}, root={:s}/{:s}*)".format(
            len(self.keys()), self.rootdir, self.prefix)

    def path_for_key(self, key):
        return os.path.join(self.rootdir, self.prefix + key + ".json")
        
    def put(self, key, data):
        assert(is_valid_key(key))
        json_utils.write_json(self.path_for_key(key), data)
        self.keyset.add(key)

    def get(self, key, default_value=None):
        if key in self.keyset:
            return json_utils.read_json(self.path_for_key(key))
        return default_value

    def remove(self, key):
        assert(key in self.keyset)
        os.remove(self.path_for_key(key))
        self.keyset.remove(key)

    def clear(self):
        ks = list(self.keyset)
        for k in ks:
            self.remove(k)

    def keys(self):
        return self.keyset

    def items(self):
        for k in self.keyset:
            yield (k, self.get(k))

    def values(self):
        for k in self.keyset:
            yield self.get(k)
    
    def __contains__(self, k):
        return k in self.keyset

    def __setitem__(self, k, v):
        self.put(k, v)

    def __getitem__(self, k):
        if not(k in self.keyset):
            raise RuntimeError("Missing key")
        return self.get(k)

import conrec_utils.json_utils as json_utils
import conrec_utils.doc_renderer as docr
import argparse
import sys

class Task:
    def perform(self, setup):
        raise NotImplementedError("perform")

    def inherit_from(self, data):
        raise NotImplementedError("inherit_from")

    def docstring(self):
        return None

class Setup:
    def __init__(self, name, tasks):
        self.parser = argparse.ArgumentParser(prog=name)
        self.parser.add_argument("--config-file", nargs=1, type=str)
        self.parser.add_argument('--run-task', nargs=1, type=str)
        self.tasks = tasks

    def get_tasks(self):
        assert(self.tasks != None)
        return self.tasks
        
    def configure(self, config):
        raise NotImplementedError("configure")

    def run_task(self, task_name):
        task = self.tasks.get(task_name)
        if task == None:
            self.parser.error("No task named '{:s}'".format(task_name))
        task.perform(self)

    def document_tasks(self, output_filename_md):
        task_table = docr.markdown_table()
        task_table.cell_at(0, 0).set("Task key")
        task_table.cell_at(0, 1).set("Description")
        row = 1
        for k, task in self.get_tasks().items():
            task_table.cell_at(row, 0).set(docr.tt(k))
            task_table.cell_at(row, 1).set(task.docstring())
            row += 1
        docr.render_markdown_to_file(output_filename_md, task_table)

    def run(self, argv):
        assert(isinstance(argv, list))
        args = self.parser.parse_args(argv)
        config = json_utils.read_json(args.config_file[0])
        self.configure(config)
        for k, v in config.get("tasks", {}).items():
            inherit_from_key = v["inherit_from"]
            self.tasks[k] = self.tasks[inherit_from_key].inherit_from(v)
        if args.run_task != None:
            for task in args.run_task:
                self.run_task(task)

    def run_argv(self):
        self.run(sys.argv[1:])

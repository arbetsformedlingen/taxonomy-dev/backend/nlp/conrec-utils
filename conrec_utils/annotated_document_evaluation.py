## Code

import csv
import importlib
import os
import copy
import conrec_utils.annotated_document_dataset as add
from conrec_utils import common
from conrec_utils import spec
from conrec_utils import time_utils
from conrec_utils import json_utils
import jobtech_taxonomy_library_python.generated.defs as taxonomy_defs
import conrec_utils.metric_accumulator as ma
import conrec_utils.doc_renderer as docr
from conrec_utils.interval import Interval
from conrec_utils.jaccard import JaccardIndex, JaccardAnalysis, zero_jaccard_index
from pathlib import Path
import requests
from flask import Flask, jsonify
from flask.views import View
from flask import request as flask_request
import conrec_utils.text as cutext
import hashlib
import html
import shutil
from conrec_utils.cached_path import cache

path_spec = spec.TypeSpec(Path)
optional_path_spec = spec.NullableSpec(path_spec)


def mkdirp_val(p):
    os.makedirs(p, exist_ok=True)
    return p

def make_parents(p):
    path_spec.check(p)
    mkdirp_val(p.parent)
    return p

class AbstractResultRepository:
    def set_item_data(self, dataset_key, item_key, item_data):
        raise NotImplementedError("set_item_data")

    def get_item_data(self, dataset_key, item_key):
        raise NotImplementedError("get_item_data")

    def set_dataset_info(self, dataset_key, info):
        raise NotImplementedError("set_dataset_info")

    def get_dataset_info(self, dataset_key):
        raise NotImplementedError("get_dataset_info")

    def list_dataset_keys(self):
        raise NotImplementedError("list_dataset_keys")

    def list_item_keys(self, dataset_key):
        raise NotImplementedError("list_item_keys")

    def dataset_subdir(self, dataset_key, name):
        raise NotImplementedError("dataset_subdir")
    
    def item_subdir(self, dataset_key, item_key, name):
        raise NotImplementedError("item_subdir")

class ResultRepository(AbstractResultRepository):
    def __init__(self, root_path):
        path_spec.check(root_path)
        self.root_path = root_path

    def datasets_path(self):
        return self.root_path.joinpath("datasets")
        
    def dataset_path(self, dataset_key):
        return self.datasets_path().joinpath(dataset_key)

    def dataset_info_path(self, dataset_key):
        return self.dataset_path(dataset_key).joinpath("data.json")

    def dataset_items_path(self, dataset_key):
        return self.dataset_path(dataset_key).joinpath("items")
    
    def dataset_item_path(self, dataset_key, item_key):
        return self.dataset_items_path(dataset_key).joinpath(item_key)

    def dataset_item_data_path(self, dataset_key, item_key):
        return self.dataset_item_path(dataset_key, item_key).joinpath("data.json")
    
    def set_item_data(self, dataset_key, item_key, data):
        mkdirp_val(self.dataset_item_path(dataset_key, item_key))
        json_utils.write_json(self.dataset_item_data_path(dataset_key, item_key), data)
        
    def get_item_data(self, dataset_key, item_key):
        p = self.dataset_item_data_path(dataset_key, item_key)
        return json_utils.read_json(p) if p.exists() else None

    def set_dataset_info(self, dataset_key, info):
        mkdirp_val(self.dataset_path(dataset_key))
        json_utils.write_json(self.dataset_info_path(dataset_key), info)

    def get_dataset_info(self, dataset_key):
        p = self.dataset_info_path(dataset_key)
        return json_utils.read_json(p) if p.exists() else None

    def list_dataset_keys(self):
        p = self.datasets_path()
        return os.listdir(p) if p.exists() else []

    def list_item_keys(self, dataset_key):
        p = self.dataset_items_path(dataset_key)
        return os.listdir(p) if p.exists() else []

    def dataset_subdir(self, dataset_key, name):
        return mkdirp_val(self.dataset_path(dataset_key).joinpath(name))
    
    def item_subdir(self, dataset_key, item_key, name):
        return mkdirp_val(self.dataset_item_path(dataset_key, item_key).joinpath(name))

    def __repr__(self):
        return "ResultRepository({:s})".format(str(self.root_path))

class InMemoryDatasetResults:
    def __init__(self):
        self.info = None
        self.items = {}

    def set_item_data(self, k, data):
        self.items[k] = data

    def get_item_data(self, k):
        return self.items.get(k)

    def list_item_keys(self):
        return list(self.items.keys())

class InMemoryResultRepository(AbstractResultRepository):
    def __init__(self):
        self.data = {}

    def at_new_dataset(self, dataset_key):
        return common.ObjectWalker(self.data).initialize_or_existing(dataset_key, lambda: InMemoryDatasetResults()).obj
        
    def set_item_data(self, dataset_key, item_key, item_data):
        self.at_new_dataset(dataset_key).set_item_data(item_key, item_data)

    def get_item_data(self, dataset_key, item_key):
        r = self.data.get(dataset_key)
        if r == None:
            return
        return r.get_item_data(item_key)

    def set_dataset_info(self, dataset_key, info):
        self.at_new_dataset(dataset_key).info = info

    def get_dataset_info(self, dataset_key):
        r = self.data.get(dataset_key)
        if r == None:
            return
        return r.info

    def list_dataset_keys(self):
        return list(self.data.keys())

    def list_item_keys(self, dataset_key):
        r = self.data.get(dataset_key)
        if r == None:
            return []
        return r.list_item_keys()

    def __repr__(self):
        return "InMemoryResultRepository()"

result_repository_spec = spec.TypeSpec(AbstractResultRepository)

raw_results_spec = spec.KeysSpec({"annotations": add.annotations_spec})

assert(isinstance(add.sha1_spec, spec.Spec))

results_spec = raw_results_spec.and_spec(spec.KeysSpec({"sha1": add.sha1_spec}))

class AnnotatedCharacter(common.ComparableByTuple):
    def __init__(self, character_index, concept_id, concept_type):
        spec.int_spec.check(character_index)
        optional_concept_id_spec.check(concept_id)
        self.character_index = character_index
        self.concept_id = concept_id
        self.concept_type = concept_type
        self._hash = hash((concept_id, character_index))
        
    def __repr__(self):
        return "AnnotatedCharacter(concept '{:s}' at {:d})".format(self.concept_id, self.character_index)

    def get_comparison_tuple(self):
        return (self.character_index, self.concept_id)
    
    def __hash__(self):
        return self._hash

    def get_concept_id_list(self):
        return [] if self.concept_id == None else [self.concept_id]

def annotated_character_from_annotation(annotation, index):
    add.annotation_spec.check(annotation)
    spec.int_spec.check(index)
    return AnnotatedCharacter(index, annotation["concept-id"], annotation["type"])
    
class AnnotationOverlap(common.ComparableByTuple):
    def __init__(self, value, interval, ground_truth_exists, prediction_exists):
        self.value = value
        self.interval = interval
        self.ground_truth_exists = ground_truth_exists
        self.prediction_exists = prediction_exists

    def is_detected(self):
        return self.ground_truth_exists and self.prediction_exists

    def __repr__(self):
        return "AnnotationOverlap({:s} at {:s}, gt:{:d}, pred:{:d})".format(
            self.value,
            str(self.interval),
            1 if self.ground_truth_exists else 0,
            1 if self.prediction_exists else 0)

    def overlapping_size(self):
        return self.interval.width() if self.ground_truth_exists and self.prediction_exists else 0

    def truth_minus_prediction(self):
        return self.interval.width()*(int(self.ground_truth_exists) - int(self.prediction_exists))
    
    def compute_jaccard_index(self):
        d = self.truth_minus_prediction()
        return JaccardIndex(max(0, d), self.overlapping_size(), max(0, -d))

    def get_comparison_tuple(self):
        return (self.interval, self.value, self.ground_truth_exists, self.prediction_exists)

    def with_interval(self, interval):
        return AnnotationOverlap(self.value, interval, self.ground_truth_exists, self.prediction_exists)
        
def compute_overlaps_for_feature(feature, gt_chars, pred_chars):
    gt_inds = {c.character_index for c in gt_chars}
    pred_inds = {c.character_index for c in pred_chars}
    all_inds = sorted(gt_inds.union(pred_inds))
    per_char_overlaps = [AnnotationOverlap(feature, Interval(i, i+1),
                                           i in gt_inds, i in pred_inds) for i in all_inds]

    parts = common.partition_by(
        lambda overlap:
        (overlap.ground_truth_exists, overlap.prediction_exists),
        per_char_overlaps)

    dst = []
    for part in parts:
        inds = [i
                for x in part
                for i in x.interval.to_range()]
        segments = common.segments_from_inds(inds)
        for (l, u) in segments:
            dst.append(part[0].with_interval(Interval(l, u)))
    return list(sorted(dst))
    
def segment_annotated_character_overlaps(feature_fn, ground_truth_chars, predicted_chars):
    annotated_characters_spec.check(ground_truth_chars)
    annotated_characters_spec.check(predicted_chars)
    
    all_features = {f
                    for chars in [ground_truth_chars, predicted_chars]
                    for c in chars
                    for f in [feature_fn(c)]
                    if f != None}

    gt_concept_groups = common.group_by(feature_fn, ground_truth_chars)
    concept_groups = common.group_by(feature_fn, predicted_chars)

    return sorted([overlap
                   for f in all_features
                   for overlap in compute_overlaps_for_feature(
                           f,
                           gt_concept_groups.get(f, []),
                           concept_groups.get(f, []))])
    

concept_id_spec = spec.str_spec
optional_concept_id_spec = spec.NullableSpec(concept_id_spec)
annotated_character_spec = spec.TypeSpec(AnnotatedCharacter)
annotated_characters_spec = spec.CollSpec(annotated_character_spec)


def annotated_characters_from_annotations(annotations):
    add.annotations_spec.check(annotations)
    return {annotated_character_from_annotation(annotation, i)
            for annotation in annotations
            for i in range(annotation["start-position"], annotation["end-position"])}

def path_difference(root_path, sub_path0):
    sub_path = sub_path0
    parts = []
    iters = 0
    while sub_path != root_path:
        parts.append(sub_path.name)
        sub_path = sub_path.parent
        iters += 1
        if iters > 100:
            raise RuntimeError(
                "Cannot compute path difference between {:s} and {:s}".format(
                str(root_path), str(sub_path0)))
    return list(reversed(parts))

def relative_path(root_path, sub_path):
    return "/".join(path_difference(root_path, sub_path))

def highlight_annotation_text(text, interval, marg=30):
    n = len(text)
    text_lower = max(0, interval.lower - marg)
    text_upper = min(n, interval.upper + marg)

    parts = ["..." if 0 < text_lower else "",
             text[text_lower:interval.lower],
             "<<<" + text[interval.lower:interval.upper] + ">>>",
             text[interval.upper:text_upper],
             "..." if text_upper < n else ""]
    return "".join(parts)


def highlight_annotation_markdown(text, interval, marg=30):
    n = len(text)
    text_lower = max(0, interval.lower - marg)
    text_upper = min(n, interval.upper + marg)

    parts = ["..." if 0 < text_lower else "",
             text[text_lower:interval.lower],
             docr.bold(text[interval.lower:interval.upper]),
             text[interval.upper:text_upper],
             "..." if text_upper < n else ""]
    return [p for p in parts if (isinstance(p, docr.DocNode) or 0 < len(p))]


def zero_to_empty_string(x):
    return "" if x == 0 else x
    
def bool_marker(x):
    return "x" if x else ""

class AbstractReporter(object):
    pass

reporter_spec = spec.TypeSpec(AbstractReporter)

class AbstractReportingModule:
    def evaluate(self, context, dataset_item, results):
        raise NotImplementedError("AbstractReportingModule evaluate")
    
    def detail_sections(self, context, dataset_item, results):
        return []

    def key(self):
        raise NotImplementedError("AbstractReportingModule key")
    
    def aggregate_results(self, a, b):
        raise NotImplementedError("AbstractReportingModule aggregate_results")

    def render_results_table(self, results):
        raise NotImplementedError("AbstractReportingModule render_results_table")

    def results_str(self, results):
        raise NotImplementedError("AbstractReportingModule results_str")

    def data_from_results(self, results):
        return results

    def module_info(self):
        return None

    def render_module_report(self, module_output_path, context, result_repository, datasets):
        pass

class StatsModule(AbstractReportingModule):
    def __init__(self):
        self.keys = ["document_count", "text_length", "true_annotations", "reported_annotations"]
        self.headers = ["Document count", "Text length", "True annotations", "Reported annotations"]
    
    def evaluate(self, context, dataset_item, results):
        if not("annotations" in results):
            print("Warning: Missing annotations for item {:s} ".format(dataset_item.id))
            print(results)
        annotations = results.get("annotations", [])
        item_data = dataset_item.data
        return {"document_count": 1,
                "text_length": len(item_data["text"]),
                "true_annotations": len(item_data["annotations"]),
                "reported_annotations": len(annotations)}
    
    def key(self):
        return "StatsModule"

    def aggregate_results(self, a, b):
        dst = {}
        for k in self.keys:
            dst[k] = a[k] + b[k]
        return dst

    def render_results_table(self, results):
        dst = docr.markdown_table()
        for (j, h) in enumerate(self.headers):
            dst.cell_at(0, j).set(h)
        for (i, r) in enumerate(results):
            row = i + 1
            for (j, k) in enumerate(self.keys):
                dst.cell_at(row, j).set(r[k])
        return dst

    def results_str(self, results):
        return "\n".join([h + ": {:d}".format(results[k]) for (h, k) in zip(self.headers, self.keys)])

def expand_items(reporting_context, result_repository, datasets):
    return ({"dataset": dataset,
             "dataset_item": reporting_context.get_by_id(dataset, item_key),
             "item_results": result_repository.get_item_data(dataset.key(), item_key)}
            for dataset in datasets
            for item_key in result_repository.list_item_keys(dataset.key()))

def segment_rows_to_markdown(rows):
    table = docr.markdown_table()

    i = 0
    table.cell_at(i, 0).set("Interval #")
    table.cell_at(i, 1).set("Text")
    table.cell_at(i, 2).set("Class")
    table.cell_at(i, 3).set("Width")
    table.cell_at(i, 4).set("Truth")
    table.cell_at(i, 5).set("Predicted")
    table.cell_at(i, 6).set("Overlap")
    
    for row in rows:
        i += 1
        segment = row["segment"]
        interval = segment.interval
        if row["interval_counter"] == 216:
            print(highlight_annotation_markdown(row["source_text"], interval))
            
        table.cell_at(i, 0).set(row["interval_counter"])
        table.cell_at(i, 1).set(highlight_annotation_markdown(row["source_text"], interval))
        table.cell_at(i, 2).set(row["class_display_data"]["markdown"])
        table.cell_at(i, 3).set(segment.interval.width())        
        table.cell_at(i, 4).set(bool_marker(segment.ground_truth_exists))
        table.cell_at(i, 5).set(bool_marker(segment.prediction_exists))
        table.cell_at(i, 6).set(zero_to_empty_string(segment.overlapping_size()))
    return table

def segment_rows_to_csv(rows, writer):
    writer.writerow(["Interval #", "Text", "Class", "Width", "Truth", "Predicted", "Overlap"])
    for row in rows:
        segment = row["segment"]
        interval = segment.interval
        writer.writerow([row["interval_counter"],
                         highlight_annotation_text(row["source_text"], interval),
                         row["class_display_data"]["text"],
                         segment.interval.width(),                         
                         bool_marker(segment.ground_truth_exists),
                         bool_marker(segment.prediction_exists),                         
                         zero_to_empty_string(segment.overlapping_size())])

class CharacterClassificationModule(AbstractReportingModule):
    def class_display_data(self, context, cl):
        raise NotImplementedError("class_display_data")

    def class_from_annotated_character(
            self, context, annotated_character):
        raise NotImplementedError("class_from_annotated_character")

    def title(self):
        raise NotImplementedError("title")

    def compute_eval_data(self, context, dataset_item, results):
        if dataset_item.data["annotations"] == None:
            raise RuntimeError("No annotations")
        annotations = results.get("annotations", [])
        item_data = dataset_item.data
        source_text = item_data["text"]
        true_annotations = item_data["annotations"]
        true_chars = annotated_characters_from_annotations(true_annotations)
        chars = annotated_characters_from_annotations(annotations)
        segments = self.segment_annotated_character_overlaps(context, true_chars, chars)
        return {"true_chars": true_chars,
                "chars": chars,
                "segments": segments}

    def evaluate(self, context, dataset_item, results):
        data = self.compute_eval_data(context, dataset_item, results)
        return self.accumulate_metric(
            context,
            len(dataset_item.data["text"]),
            data["true_chars"],
            data["chars"]).get_metrics()

    def detail_sections(self, context, dataset_item, results):
        data = self.compute_eval_data(context, dataset_item, results)
        char_table = self.render_segment_table(
            context, dataset_item, data["segments"])
        concept_table = self.render_element_table(
            context, dataset_item, data["true_chars"], data["chars"])
        return [docr.section(self.title(),
                             docr.section("Character-wise comparison", char_table),
                             docr.section("Class-wise comparison", concept_table))]

    def metric_accumulator(self):
        raise NotImplementedError("metric_accumulator")

    def accumulate_metric(self, context, n, true_chars, pred_chars):
        def group_chars_by_index(chars):
            return common.group_by(lambda c: c.character_index, chars)
        
        true_groups = group_chars_by_index(true_chars)
        pred_groups = group_chars_by_index(pred_chars)

        acc = self.metric_accumulator()
        for i in range(n):
            def class_set(group_map):
                return {cl
                        for c in group_map.get(i, [])
                        for cl in [self.class_from_annotated_character(context, c)]
                        if cl != None}
            acc.accumulate(class_set(true_groups), class_set(pred_groups))
        return acc

    def data_from_results(self, results):
        return self.metric_accumulator().data_from_metrics(results)
    
    def aggregate_results(self, a, b):
        return self.metric_accumulator().add(a, b)

    def render_segment_table(self, context, dataset_item, segments):
        source_text = dataset_item.data["text"]
        
        total_char_jaccard = zero_jaccard_index
        row = 1
        nrvc = common.NonRepeatingValueCounter()
        rows = []
        for segment in segments:
            rows.append({"interval_counter": nrvc.get(segment.interval),
                         "source_text": source_text,
                         "segment": segment,
                         "class_display_data": self.class_display_data(context, segment.value)})
            total_char_jaccard = total_char_jaccard + segment.compute_jaccard_index()
        char_table = segment_rows_to_markdown(rows)
        
        char_table.cell_at(row, 1).set(docr.bold("Overall"))
        char_table.cell_at(row, 4).set(docr.bold(total_char_jaccard.intersection_size))
        char_table.cell_at(row, 5).set(docr.bold(total_char_jaccard.union_size))
        char_table.cell_at(row, 6).set(ma.format_jaccard(total_char_jaccard))
        return char_table

    def render_module_report(self, output_path, context, result_repository, datasets):
        put_mdwiki(output_path.joinpath("index.html"))
        items = expand_items(context, result_repository, datasets)
        rows = []
        nrvc = common.NonRepeatingValueCounter()
        for item in items:
            dataset_item = item["dataset_item"]
            item_results = item["item_results"]
            edata = self.compute_eval_data(context, dataset_item, item_results)
            for segment in edata["segments"]:
                rows.append({**item,
                             "interval_counter": nrvc.get(segment.interval),
                             "source_text": dataset_item.data["text"],
                             "segment": segment,
                             "class_display_data": self.class_display_data(context, segment.value)})
        docr.render_markdown_to_file(
            output_path.joinpath("index.md"),
            segment_rows_to_markdown(rows))
        with open(output_path.joinpath("segments.csv"), "w") as f:
            writer = csv.writer(f)
            segment_rows_to_csv(rows, writer)

    def get_class_set_from_annotated_characters(self, context, chars):
        return {cl
                for c in chars
                for cl in [self.class_from_annotated_character(context, c)]
                if cl != None}
    
    def render_element_table(self, context, dataset_item, true_chars, chars):
        concept_table = docr.markdown_table()
        concept_table.cell_at(0, 0).set("Truth")
        concept_table.cell_at(0, 1).set("Predicted")
        concept_table.cell_at(0, 2).set("Overlap")
        concept_table.cell_at(0, 3).set("Class")

        true_set = self.get_class_set_from_annotated_characters(context, true_chars)
        pred_set = self.get_class_set_from_annotated_characters(context, chars)

        concept_analysis = JaccardAnalysis(true_set, pred_set)
        row = 1
        for class_id in sorted(concept_analysis.union):
            in_true = class_id in true_set
            in_pred = class_id in pred_set
            in_both = in_true and in_pred
            concept_table.cell_at(row, 0).set(bool_marker(in_true))
            concept_table.cell_at(row, 1).set(bool_marker(in_pred))
            concept_table.cell_at(row, 2).set(bool_marker(in_both))
            concept_table.cell_at(row, 3).set(self.class_display_data(context, class_id)["markdown"])
            row += 1
        concept_jaccard_index = concept_analysis.jaccard_index
        concept_table.cell_at(row, 2).set(docr.bold(concept_jaccard_index.intersection_size))
        concept_table.cell_at(row, 3).set(ma.format_jaccard(concept_jaccard_index))
        return concept_table

    def segment_annotated_character_overlaps(self, context, true_chars, chars):
        return segment_annotated_character_overlaps(
            lambda c: self.class_from_annotated_character(context, c),
            true_chars, chars)

    def render_results_table(self, results):
        acc = self.metric_accumulator()
        table_data = [acc.markdown_header_value_pairs(r)
                      for r in results]
        dst = docr.markdown_table()
        
        if len(table_data) == 0:
            return dst

        for (j, (h, x)) in enumerate(table_data[0]):
            dst.cell_at(0, j).set(h)
        for (i, row) in enumerate(table_data):
            for (j, (h, x)) in enumerate(row):
                dst.cell_at(i+1, j).set(x)
        return dst

    def results_str(self, results):
        acc = self.metric_accumulator()
        return acc.report_text(results)            

    
class ConceptRecognitionReportingModule(CharacterClassificationModule):
    def class_display_data(self, context, cid):
        return {"markdown": context.concept_link(cid),
                "text": context.concept_text(cid),
                "id": cid}

    def class_from_annotated_character(self, context, c):
        return c.concept_id

    def metric_accumulator(self):
        return ma.CompositeMetricAccumulator([
            ma.JaccardAccumulator("conrec"),
            ma.OverallJaccardAccumulator("conrec")])

    def key(self):
        return "ConceptRecognitionReportingModule"

    def title(self):
        return "Concept recognition"

default_reporting_modules = [StatsModule(), ConceptRecognitionReportingModule()]

class ResultSummary:
    def __init__(self, key, data, total, sub_results):
        self.key = spec.str_spec.check(key)
        self.data = spec.KeysSpec({"type": spec.str_spec}).check(data)
        self.total = total #ma.metric_accumulator_map_spec.check(total)
        self.sub_results = result_summary_list_spec.check(sub_results)
        self.sub_result_map = {r.key:r for r in sub_results}

    def __repr__(self):
        return "ResultSummary(key={:s}, data={:s}, total={:s}, sub_results={:s})".format(
            self.key, str(self.data), str(self.total), ", ".join([str(x) for x in self.sub_results]))


    def render_doc_node(self, context, title):
        assert(isinstance(title, str))

        if len(self.sub_results) == 0:
            results = [{**self.data, "results": self.total}]
        else:
            results = [{**x.data, "results": x.total} for x in self.sub_results]
            results.append({"type": "total", "results": self.total})
            
        title_table = docr.markdown_table()
        title_table.cell_at(0, 0).set(title)
        for (i, x) in enumerate(results):
            title_table.cell_at(i+1, 0).set(context.get_table_item_label(x))

        other_tables = [module.render_results_table([x["results"][module.key()]
                                                     for x in results])
                        for module in context.reporting_modules]
        all_tables = [title_table] + other_tables

        return docr.hcat_tables(all_tables)

def render_results_comparison(context, algo_summary_pairs):
    title_table = docr.markdown_table()
    title_table.cell_at(0, 0).set("Algorithm")

    for (i, (algo, summary)) in enumerate(algo_summary_pairs):
        title_table.cell_at(i+1, 0).set(algo)
    other_tables = [module.render_results_table(summary.total[module.key()]
                                                for algo, summary in algo_summary_pairs)
                    for module in context.reporting_modules]

    all_tables = [title_table] + other_tables
    return docr.hcat_tables(all_tables)
    

result_summary_spec = spec.TypeSpec(ResultSummary)
result_summary_list_spec = spec.CollSpec(result_summary_spec)
result_summary_map_spec = spec.MapOfSpec(spec.any_spec, result_summary_spec)


class AbstractReportWriter:
    pass
        
class MarkdownReportWriter(AbstractReportWriter):
    def __init__(self, root_path):
        self.root_path = path_spec.check(root_path)

    def __repr__(self):
        return "MarkdownReportWriter({:s})".format(str(self.root_path))

    def top_path(self):
        return self.root_path.joinpath("index.md")

    def dataset_path(self, dataset_key):
        return self.root_path.joinpath(dataset_key, "README.md")

    def item_path(self, dataset_key, item_key):
        return self.root_path.joinpath(dataset_key, item_key, "README.md")

    def module_output_path(self, module_key):
        return self.root_path.joinpath("modules", module_key)

    def output(self, dst_path, x):
        docr.render_markdown_to_file(make_parents(dst_path), x)
        
    def write_top(self, x):
        self.output(self.top_path(), x)
        put_mdwiki(self.root_path.joinpath("index.html"))

    def write_dataset_report(self, dataset_key, x):
        self.output(self.dataset_path(dataset_key), x)

    def write_item_report(self, dataset_key, item_key, x):
        self.output(self.item_path(dataset_key, item_key), x)

def put_mdwiki(dst_path):
    dst_path = Path(dst_path)
    if dst_path.exists():
        return
    mdwiki_files = cache.get("https://github.com/Dynalon/mdwiki/releases/download/0.6.2/mdwiki-0.6.2.zip", extract_archive=True)
    src = Path(mdwiki_files).joinpath("mdwiki-0.6.2").joinpath("mdwiki.html")
    assert(src.exists())
    shutil.copy(src, dst_path)
        
class ReportingContext:
    def __init__(self, env, taxonomy_version_map, reporting_modules=None, writer=None):
        self.env = env
        self.taxonomy_version_map = taxonomy_version_map
        self.show_progress = True
        self.verbose = True
        self.reporting_modules = common.or_some(reporting_modules, default_reporting_modules)
        self.writer = writer
        self.registered_datasets = {}
        self.current_dir = None

    def get_by_id(self, dataset, item_id):
        item = dataset.get_by_id(item_id)
        good_annotations = []
        for annotation in item.data.get("annotations", []):
            try:
                cid = annotation.get("concept-id")
                if cid != None:
                    concept = self.taxonomy_version_map.latest_concept(cid)
                good_annotations.append(annotation)
            except StopIteration:
                print(f"WARNING: No concept with id {cid} for dataset item {item.id}")
        return item.update_data(lambda data: {**data, "annotations": good_annotations})
                
    def with_register_dataset(self, ds):
        dst = copy.copy(self)
        dst.registered_datasets = {**self.registered_datasets, ds.key():ds}
        return dst

    def with_current_dir(self, d):
        dst = copy.copy(self)
        dst.current_dir = d
        return dst

    def with_writer(self, writer):
        dst = copy.copy(self)
        spec.TypeSpec(AbstractReportWriter).check(writer)
        dst.writer = writer;
        return dst

    def with_set_reporting_modules(self, modules):
        dst = copy.copy(self)
        dst.reporting_modules = modules
        return dst

    def get_table_item_label(self, data):
        assert(self.current_dir != None)
        t = data["type"]
        if t == "total":
            return docr.bold("Total")
        elif t == "dataset":
            ds = data["dataset"]
            return self.markdown_link_to_dataset(self.current_dir, ds)
        elif t == "dataset_item":
            item = data["dataset_item"]
            return self.markdown_link_to_dataset_item(self.current_dir, item)
    
    def concept_link(self, concept_id):
        concept = self.taxonomy_version_map.latest_concept(concept_id)
        if concept == None:
            return "{:s} (not found in taxonomy)".format(concept_id)
        return docr.link([concept.preferred_label(), ", ", docr.bold(concept.type())]).to(concept.uri())

    def concept_text(self, concept_id):
        concept = self.taxonomy_version_map.latest_concept(concept_id)
        if concept == None:
            return concept_id
        return "{:s} <{:s} {:s}>".format(concept.preferred_label(), concept.type(), concept_id)

    def with_reporting_module(self, mod):
        return self.with_set_reporting_modules(self.reporting_modules + [mod])
    
    def module_keys(self):
        return [module.key() for module in self.reporting_modules]
        
    def select_module_data(self, results):
        return common.select_keys(results, self.module_keys())
        
    def _aggregate_results(self, results):
        n = len(results)
        if 1 <= n:
            r0 = results[0]
            total = self.select_module_data(results[0].total)
            for r in results[1:]:
                for module in self.reporting_modules:
                    k = module.key()
                    total[k] = module.aggregate_results(total[k], r.total[k])
                    assert(total[k] != None)
            return total
        else:
            raise RuntimeError("No results to aggregate")

    def aggregate_result_summaries(self, key, data, result_summaries):
        result_summary_list_spec.check(result_summaries)
        return ResultSummary(
            key, data,
            self._aggregate_results(result_summaries),
            result_summaries)

    def evaluate_dataset_item(self, dataset_item, results):
        results_spec.check(results)
        return ResultSummary(
            dataset_item.id,
            {"type": "dataset_item",
             "dataset_item": dataset_item},
            {module.key():module.evaluate(self, dataset_item, results)
             for module in self.reporting_modules}, [])

    def load_dataset(self, result_repository, dataset_key):
        regd = self.registered_datasets.get(dataset_key)
        if regd != None:
            return regd
        dataset_info = result_repository.get_dataset_info(dataset_key)
        if dataset_info == None:
            raise RuntimeError("No dataset info for key " + str(dataset_key))
        return add.dataset_from_data_repr(self.env, dataset_info["dataset"])
    
    def summarize_results(self, result_repository):
        result_repository_spec.check(result_repository)
        per_dataset = []
        for dataset in self.result_repository_datasets(result_repository):
            per_item = [self.evaluate_dataset_item(self.get_by_id(dataset, item_key),
                                                   result_repository.get_item_data(dataset.key(), item_key))
                        for item_key in result_repository.list_item_keys(dataset.key())]
            per_dataset.append(self.aggregate_result_summaries(
                dataset.key(),
                {"type": "dataset",
                 "dataset": dataset},
                per_item))
        return self.aggregate_result_summaries("top", {"type": "top"}, per_dataset)

    def results_str(self, result_summary):
        results = result_summary_spec.check(result_summary).total
        return "\n".join([module.results_str(results[module.key()])
                          for module in self.reporting_modules])

    def result_repo_str(self, result_repo):
        return self.results_str(self.summarize_results(result_repo))

    def __repr__(self):
        return "ReportingContext(tax={:s})".format(
            str(self.taxonomy_version_map))

    def markdown_link_to_dataset(self, src_path, dataset):
        return docr.link(dataset.title()).to(
            relative_path(src_path, self.writer.dataset_path(dataset.key())))

    def markdown_link_to_dataset_item(self, src_path, dataset_item):
        return docr.link(dataset_item.id).to(
            relative_path(
                src_path,
                self.writer.item_path(
                    dataset_item.src_dataset.key(), dataset_item.id)))

    def render_item_report(self, result_repository, summarized_results, item):
        dataset = item.src_dataset
        results = result_repository.get_item_data(item.src_dataset.key(), item.id)
        detail_sections = [sec
                           for module in self.reporting_modules
                           for sec in module.detail_sections(self, item, results)]

        doc = docr.section(
            "Results for '{:s}'".format(item.id),
            summarized_results.render_doc_node(
                self.with_current_dir(self.writer.item_path(dataset.key(), item.id).parent),
                "Title"),
            docr.section("Source text", item.data["text"]),
            detail_sections)

        self.writer.write_item_report(dataset.key(), item.id, doc)
    
    def render_dataset_report(self, result_repository, summarized_results, dataset):
        item_keys = result_repository.list_item_keys(dataset.key())
        for item_key in item_keys:
            self.render_item_report(
                result_repository,
                summarized_results.sub_result_map[item_key],
                self.get_by_id(dataset, item_key))

        doc = docr.section(
            ["Results for dataset ", docr.bold(dataset.title())],
            summarized_results.render_doc_node(
                self.with_current_dir(self.writer.dataset_path(dataset.key()).parent),
                "Document"))
        self.writer.write_dataset_report(dataset.key(), doc)

    def result_repository_datasets(self, result_repository):
        return [self.load_dataset(result_repository, dataset_key)
                for dataset_key in result_repository.list_dataset_keys()]

    def render_module_reports(self, result_repository):
        datasets = self.result_repository_datasets(result_repository)
        for module in self.reporting_modules:
            module_path = mkdirp_val(self.writer.module_output_path(module.key()))
            module.render_module_report(module_path, self, result_repository, datasets)
    
    def render_detailed_report(self, result_repository, summarized_results=None):
        assert(self.writer != None)
        if summarized_results == None:
            summarized_results = self.summarize_results(result_repository)
        datasets = self.result_repository_datasets(result_repository)
        for dataset in datasets:
            self.render_dataset_report(
                result_repository,
                summarized_results.sub_result_map[dataset.key()],
                dataset)

        module_info = [info
                       for module in self.reporting_modules
                       for info in [module.module_info()] if info != None]

        doc = docr.section(
            "Report for annotated documents",
            summarized_results.render_doc_node(
                self.with_current_dir(self.writer.root_path), "Dataset"),
            module_info)

        self.writer.write_top(doc)
        if self.verbose:
            print("*** Wrote report to " + str(self.writer))
            print(self.results_str(summarized_results))

    def render_report(self, result_repository, summarized_results=None):
        self.render_detailed_report(result_repository, summarized_results)
        self.render_module_reports(result_repository)


reporting_context_spec = spec.TypeSpec(ReportingContext)

class EvaluationContext:
    def __init__(self, result_repo):
        self.result_repo = result_repository_spec.check(result_repo)

evaluation_context_spec = spec.TypeSpec(EvaluationContext)        

class AbstractAlgorithm:
    def preprocess_for_dataset(self, evaluation_context, dataset):
        return None

    def name(self):
        return "AbstractAlgorithm"

    def process_text(self, text):
        raise NotImplementedError("process_text")
    
    def process_dataset_item(self, evaluation_context, preprocessed_data, dataset_item):
        return self.process_text(dataset_item.data["text"])

# For debugging.
class ConstantAlgorithm(AbstractAlgorithm):
    def __init__(self, result):
        self.result = result

    def process_text(self, text):
        # https://atlas.jobtechdev.se/page/taxonomy.html#concept=dhgR_9jk_95r
        return self.result
    

############################################################## REPORTERS
class DatasetItemReporter(AbstractReporter):
    def __init__(self, dataset, item, result_repository):
        self.dataset = dataset
        self.item = add.dataset_item_spec.check(item)
        self.key = item.id
        self.result_repository = result_repository_spec.check(result_repository)

    def report_annotations(self, annotations):
        add.annotations_spec.check(annotations)
        self.report_results({"annotations": annotations})

    def report_results(self, results):
        raw_results_spec.check(results, "dataset item {:s} and result repo {:s}".format(self.key, str(self.result_repository)))
        results = {"sha1": self.item.data["sha1"], **results}
        self.result_repository.set_item_data(self.dataset.key(), self.key, results)

    def has_reported_result(self):
        return self.get_results() != None # Fix this, don't load the data

    def get_results(self):
        return self.result_repository.get_item_data(self.dataset.key(), self.key)

    def get_annotations(self):
        return self.get_results()["annotations"]

    def subdir(self, name):
        return self.result_repository.item_subdir(self.dataset.key(), self.key, name)

    def debug_directory(self):
        return self.subdir("debug")    

    def __repr__(self):
        return "DatasetItemReporter(id={:s})".format(self.key)

class DatasetReporter(AbstractReporter):
    def __init__(self, dataset, result_repository):
        self.dataset = spec.TypeSpec(add.Dataset).check(dataset)
        self.result_repository = result_repository_spec.check(result_repository)
        result_repository.set_dataset_info(dataset.key(), {"dataset": dataset.data_repr()})
        
    def for_item(self, dataset_item):
        return DatasetItemReporter(
            self.dataset,
            dataset_item,
            self.result_repository)

    def evaluation_context(self):
        return EvaluationContext(self.result_repository)
    
    def evaluate_algorithm(self, algorithm, incremental=False, verbose=False):
        assert(isinstance(algorithm, AbstractAlgorithm))
        e_context = self.evaluation_context()
        preprocessed = algorithm.preprocess_for_dataset(e_context, self.dataset)
        prog = time_utils.ProgressReporter(len(self.dataset))
        for item in self.dataset:
            rep = self.for_item(item)
            if incremental and rep.has_reported_result():
                prog.skip_iteration()
            else:
                results = raw_results_spec.check(algorithm.process_dataset_item(e_context, preprocessed, item))
                item_reporter = rep.report_results(results)
                prog.end_of_iteration_message("Evaluating algorithm")
        if verbose:
            #print(prog.to_data())
            print(prog.progress_report())
            
    def title(self):
        return "Dataset {:s}".format(self.dataset.title())
    
    def __repr__(self):
        return "DatasetReporter({:s})".format(self.dataset.title())

class Reporter(AbstractReporter):
    def __init__(self, result_repository):
        result_repository_spec.check(result_repository)
        self.result_repository = result_repository

    def for_dataset(self, dataset):
        return DatasetReporter(dataset, self.result_repository)

    def __repr__(self):
        return "Reporter({:s})".format(str(self.path))

def evaluate_algorithm(result_repository, datasets, algorithm, incremental=False, verbose=False):
    reporter = Reporter(result_repository)
    for dataset in datasets:
        if verbose:
            print("Evaluate on " + str(dataset))
        reporter.for_dataset(dataset).evaluate_algorithm(algorithm, incremental=incremental, verbose=verbose)






class MentorApiHttpAlgorithm(AbstractAlgorithm):
    def __init__(self, url):
        self.url = spec.str_spec.check(url)

    def education_description(self, text):
        response = requests.post(self.url + "/nlp/education-description",
                                 params={"text": spec.str_spec.check(text)},

                                 # You may have to set this to False in order for it to work.
                                 verify=True)
        try:
            return response.json()
        except Exception as e:
            print("FAILED to DECODE response '{:s}'".format(str(response)))
            print("For text '{:s}'".format(cutext.abbreviate(text, 30)))
            return {"annotations": []}
            
    def process_dataset_item(self, evaluation_context, preprocessed_data, dataset_item):
        return raw_results_spec.check(self.education_description(dataset_item.data["text"]))

default_mentor_api = MentorApiHttpAlgorithm("https://mentor-api-mentor-api-develop.test.services.jtech.se")



sha1 = hashlib.sha1()

def annotate_text(algorithm, text):
    assert(isinstance(text, str))
    result = raw_results_spec.check(algorithm.process_text(text))
    return {"text": text, "sha1": hashlib.sha1(text.encode("utf-8")).hexdigest(), **result}

class MentorApiHttpServerAlgorithmView(View):
    def __init__(self, algorithm, req_parser):
        self.algorithm = algorithm
        self.req_parser = req_parser

    def dispatch_request(self, **kwargs):
        text = self.req_parser.get_text(flask_request)
        return jsonify(annotate_text(self.algorithm, text))

class QueryStringAlgoArgs:
    def get_text(self, req):
        return req.args.get("text")

class JsonBodyAlgoArgs:
    def get_text(self, req):
        return req.get_json().get("text")
    
class MentorApiHttpServerJobAdView(View):
    def __init__(self, algorithm):
        self.algorithm = algorithm

    def dispatch_request(self, **kwargs):
        jobad_id = flask_request.args.get("id")
        addr = "https://jobsearch.api.jobtechdev.se/ad/{:s}".format(jobad_id)
        print("ADDR: " + addr)
        response = requests.get(addr)
        if response.status_code == 200:
            ad = response.json()
            headline = ad["headline"]
            text = ad["description"]["text"]
            result = {**annotate_text(self.algorithm, text),
                      "meta": {"jobad": ad, "name": headline}}
            return jsonify(result)
        else:
            return jsonify(response.json()), response.status_code

# curl -d '{"key1":"value1", "key2":"value2"}' -H "Content-Type: application/json" -X POST http://localhost:5000/nlp/demo

class MentorApiHttpServerDemoView(View):
    def __init__(self, server):
        self.algorithm = server.algorithm
        self.taxonomy_version_map = server.taxonomy_version_map
        self.req_parser = JsonBodyAlgoArgs()

    def dispatch_request(self, **kwargs):
        print("Got a request")
        text = ""
        result_html = "No result"
        if flask_request.method == "POST":
            text = flask_request.form.get("text")
            try:
                results = self.algorithm.process_text(text)

                annotations = results.get("annotations", [])
                result_html = "<table><tr><th>Found string</th><th>Start pos</th><th>End pos</th><th>Concept id</th><th>Concept label</th><th>Concept type</th></tr>"

                for x in annotations:
                    spos = x["start-position"]
                    epos = x["end-position"]
                    ms = " - "
                    if 0 <= spos and spos <= epos and epos <= len(text):
                        ms = text[spos:epos]

                    ctype = ""
                    cid = x.get("concept-id", "")
                    clabel = ""
                    concept = self.taxonomy_version_map.latest_concept(cid)
                    if concept != None:
                        clabel = concept.preferred_label()
                        ctype = concept.type()
                    
                    result_html += f"<tr><td>{ms}</td><td>{spos}</td><td>{epos}</td><td>{cid}</td><td>{clabel}</td><td>{ctype}</td></tr>"
                
                result_html += "</table>"
            except Exception as e:
                result_html = f"<pre>{str(e)}</pre>"
        style = """

        textarea {
          width: 60em;
          height: 30em;
        }

table, th, td {
  padding: 0.5em;
  border: 1px solid black;
  border-collapse: collapse;
}


        """
            
        return f"""<html><head><title>Demo of {self.algorithm.name()}</title><style>{style}</style></head><body>
        <h1>Input</h1>
        <form method='post'>
        <textarea name='text'>{html.escape(text)}</textarea>
        <input type='submit' value='Process'>
        </form>
        <h1>Result</h1>
        {result_html}
        </body>"""
        

class MentorApiHttpServer:
    def __init__(self, algorithm, taxonomy_version_map=None):
        self.algorithm = algorithm
        self.taxonomy_version_map = taxonomy_version_map

    def title(self):
        return "MentorApiHttpServer for algorithm " + self.algorithm.name()
    
    def make_app(self):
        app = Flask(self.title())

        # Endpoint for education-descriptions
        app.add_url_rule(
            "/nlp/education-description",
            view_func=MentorApiHttpServerAlgorithmView.as_view(
                "education_description_view", self.algorithm, QueryStringAlgoArgs()),
            methods=["POST"])

        # General purpose endpoint (currently same interface as education-description)
        app.add_url_rule("/nlp/annotate-text",
                         view_func=MentorApiHttpServerAlgorithmView.as_view(
                             "annotate_text_view", self.algorithm, JsonBodyAlgoArgs()),
                         methods=["POST"])

        app.add_url_rule("/nlp/job-ad",
                         view_func=MentorApiHttpServerJobAdView.as_view("jobad_view", self.algorithm),
                         methods=["POST", "GET"])

        app.add_url_rule("/nlp/demo",
                         view_func=MentorApiHttpServerDemoView.as_view("demo_view", self),
                         methods=["POST", "GET"])
        
        return app

    def make_test_app(self):
        app = self.make_app()
        app.config.update({
            "TESTING": True,
        })
        return app

    def run_dev(self):
        self.make_app().run()

def trim_dataset_item(item, max_annotations=None):
    result = item.copy()
    del result.data["meta"]["jobad"]
    if max_annotations != None:
        annotations = result.data["annotations"]
        result.data["annotations"] = annotations[0:min(max_annotations, len(annotations))]
    return result

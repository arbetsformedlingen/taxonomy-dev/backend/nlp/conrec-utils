import requests
import urllib
from urllib.parse import urlparse
from contextlib import ExitStack, closing, nullcontext
import os
import shutil
from pathlib import Path
import conrec_utils.json_utils as json
import zipfile

class CachedDownloader:
    def __init__(self, cache_root):
        self._cache_root = Path(cache_root)

    def default():
        return CachedDownloader(Path.home().joinpath(".cache", "conrec-utils-cache"))

    def full_path(self, filename):
        return self._cache_root.joinpath(filename)
    
    def index_file(self):
        return self.full_path("index.json")

    def load_index(self):
        f = self.index_file()
        return json.read_json(f) if f.exists() else {"counter": 0, "filemap": {}}

    def save_index(self, index):
        os.makedirs(self._cache_root, exist_ok=True)
        json.write_json(self.index_file(), index)

    def download(self, url, dst_path):
        os.makedirs(self._cache_root, exist_ok=True)
        print(f"Downloading to '{dst_path}'")
    
        def cleanup():
            os.remove(dst_path)

        # https://stackoverflow.com/questions/7243750/download-file-from-web-in-python-3
        with ExitStack() as stack, urllib.request.urlopen(url) as response, open(dst_path, 'wb') as out_file:
            stack.callback(cleanup)
            shutil.copyfileobj(response, out_file)
            stack.pop_all()

    def full_filename(self, filename):
        return self.full_path(filename + ".dat")

    def get_filename(self, url):
        index = self.load_index()
        filemap = index["filemap"]
        if url not in filemap:
            counter = index["counter"]
            filename = f"file{counter}"
            self.download(url, self.full_filename(filename))
            index["counter"] = counter + 1
            index["filemap"][url] = filename
            self.save_index(index)
        else:
            filename = filemap[url]
        return filename

    def get(self, url, extract_archive=False):
        base_filename = self.get_filename(url)
        src_filename = self.full_filename(base_filename)
        if extract_archive:
            unpacked = self.full_path(base_filename)
            with zipfile.ZipFile(src_filename, 'r') as zip_ref:
                zip_ref.extractall(unpacked)
            return unpacked
        else:
            return src_filename

cache = CachedDownloader.default()

if False:
    url = "https://data.jobtechdev.se/taxonomy/version/latest/query/concept-types/concept-types.json"
    filename = cache.get(url)
    with open(filename) as f:
        print(f.read())
if False:
    root = cache.get("https://github.com/Dynalon/mdwiki/releases/download/0.6.2/mdwiki-0.6.2.zip", extract_archive=True)
    print(root)

    

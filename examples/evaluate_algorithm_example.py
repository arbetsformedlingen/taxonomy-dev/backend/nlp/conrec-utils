import importlib
from conrec_utils.env import Env
import conrec_utils.annotated_document_dataset as add
import conrec_utils.annotated_document_evaluation as ade
from pathlib import Path
from jobtech_taxonomy_library_python.generated.defs import taxonomy

# This is a class that we write for our algorithm that we are going to evaluate.
class MyAnnotationAlgorithm(ade.AbstractAlgorithm):

    # This method is called *before* the actual processing of the dataset
    # items. It is an opportunity to perform batch computations on the
    # dataset if the algorithm needs that. But we can as well just return
    # None.
    def preprocess_for_dataset(self, evaluation_context, dataset):
        return None

    # This method should run the algorithm on a single dataset item (a document)
    # Because this is just an example, we are going to report the concept sjuksköterska
    # if the text contains the word "sjuksköterska" or variants of that word.
    def process_dataset_item(self, evaluation_context, preprocessed_data, dataset_item):
        annotations = []

        concepts_to_look_for = [{'preferred-label': 'Sjuksköterska, grundutbildad',
                                 'concept-id': 'bXNH_MNX_dUR',
                                 "type": "occupation-name",
                                 "synonyms": [
                                     "sjuksköterska", "sjuksköterskor",
                                     "sjuksköterskan", "sjuksköterskorna"]}]

        text = dataset_item.data["text"].lower()
        for c in concepts_to_look_for:
            for s in c["synonyms"]:
                i = text.find(s)
                if 0 <= i:
                    annotations.append(
                        {**c,
                         "start-position": i,
                         "end-position": (i + len(s))})


        # This is how we report the result in the form of annotations.
        return {"annotations": annotations}


# Load data used for evaluation
env = Env.default()
repo = add.load_known_repository(env, "mentor-api-prod")
dataset = repo.full_dataset().validation_subset()

my_algorithm = MyAnnotationAlgorithm()

# Compute results
result_repo = ade.InMemoryResultRepository()
ade.evaluate_algorithm(result_repo, [dataset], my_algorithm)

# Render report
ctx = ade.ReportingContext(env, taxonomy).with_writer(ade.MarkdownReportWriter(Path("/tmp/my_algo_report")))
ctx.render_report(result_repo)

.PHONY: test specific-test clean-results evaluate-validation report comparison generate-new-indices

test:
	poetry run python3 -m unittest discover

specific-test:
#	poetry run python3 -m unittest tests.test_conrec
#	poetry run python3 -m unittest tests.test_common
#	poetry run python3 -m unittest tests.test_eval_common
	poetry run python3 -m unittest tests.test_text2ssyk_evaluation
#	poetry run python3 -m unittest tests.test_job_ads_dataset
#	poetry run python3 -m unittest tests.test_metric_accumulator
#	poetry run python3 -m unittest tests.test_textgrid
#	poetry run python3 -m unittest tests.job_ads.test_downloader
#	poetry run python3 -m unittest tests.job_ads.test_database
#	poetry run python3 -m unittest tests.job_ads.test_dataset_split

clean-results:
	rm -rf results

evaluate-validation:
	poetry run python3 conrec_utils/cli.py --config-file config.json --run-task evaluate-validation

report:
	poetry run python3 conrec_utils/cli.py --config-file config.json --run-task generate-reports

comparison:
	poetry run python3 conrec_utils/cli.py --config-file config.json --run-task comparison-report

generate-new-indices:
	poetry run python3 conrec_utils/cli.py --config-file config.json --run-task generate-new-indices

download-all-historical-jobads:
	poetry run python3 -c 'import conrec_utils.historical_job_ads_dataset as ds; ds.download_all()'

text2ssyk-make-report:
	poetry run python3 -c 'import conrec_utils.text2ssyk_evaluation as e; e.make_report()'

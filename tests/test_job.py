from conrec_utils.json_file_map import JsonFileMap
from conrec_utils.job import JobRunner, FunctionJob
import unittest
import tempfile

class TestJob(unittest.TestCase):
    def test_job_runner(self):
        with tempfile.TemporaryDirectory() as tempdir:
            m = JsonFileMap(tempdir)
            r = JobRunner(m)
            r.reset()
            self.assertEqual(0, len(m.keys()))
            m = r.run_jobs([FunctionJob("a", lambda: 20), FunctionJob("b", lambda: 30)])
            self.assertEqual(m.get("a"), 20)

    def test_job(self):
        with tempfile.TemporaryDirectory() as tempdir:
            m = JsonFileMap(tempdir)
            job = FunctionJob("add_numbers", lambda: 3 + 3)
            self.assertFalse(job.has_result(m))
            self.assertEqual(6, job.compute())
            self.assertFalse(job.has_result(m))
            self.assertEqual(6, job.evaluate(m))
            self.assertTrue(job.has_result(m))
            self.assertEqual(6, job.evaluate(m))

    def test_job_with_data(self):
        with tempfile.TemporaryDirectory() as tempdir:
            m = JsonFileMap(tempdir)
            job = FunctionJob("add_numbers", lambda nums: nums[0] + nums[1]).with_data((3, 4))
            self.assertEqual(7, job.compute())
        
        
            

import unittest
from conrec_utils import spec

class TestSpec(unittest.TestCase):
    def test_it(self):
        self.assertFalse(spec.TypeSpec(int).is_valid(3.0))
        self.assertTrue(spec.TypeSpec(int).is_valid(3))

        m = spec.KeysSpec({"a": spec.TypeSpec(int),
                          "b": spec.TypeSpec(str),
                          "c": spec.OptionalSpec(spec.TypeSpec(int))})

        self.assertTrue(m.is_valid({"a": 3, "b": "xy"}))
        self.assertFalse(m.is_valid({"a": 3, "b": 9}))
        self.assertTrue(m.is_valid({"a": 3, "b": "xy", "c": 119}))
        self.assertFalse(m.is_valid({"a": 3, "b": "xy", "c": "119"}))

        k = spec.KeysSpec({"x": spec.KeysSpec({"y": spec.CollSpec(spec.TypeSpec(int))})})

        self.assertTrue(k.is_valid({"x": {"y": [3, 4, 5]}}))
        self.assertFalse(k.is_valid({"x": {"y": [3, 4, "x"]}}))

        oddspec = spec.PredicateSpec(lambda i: i % 2 == 1)
        
        self.assertTrue(oddspec.is_valid(3))
        self.assertFalse(oddspec.is_valid(4))

    def test_map_of_spec(self):
        s = spec.MapOfSpec(spec.str_spec, spec.int_spec)

        self.assertTrue(s.is_valid({"a": 9}))
        self.assertFalse(s.is_valid({"a": "Mjao"}))
        self.assertFalse(s.is_valid(["a", "Mjao"]))

    def test_or_spec(self):
        s = spec.OrSpec().add(spec.str_spec).add(spec.int_spec)
        self.assertTrue(s.is_valid(3))
        self.assertTrue(s.is_valid("abc"))
        self.assertFalse(s.is_valid([]))

    def test_nullable_spec(self):
        s = spec.NullableSpec(spec.str_spec)

        self.assertTrue(s.is_valid(None))
        self.assertTrue(s.is_valid("asdfasdf"))
        self.assertFalse(s.is_valid(9))

    def test_and_spec(self):
        a = spec.KeysSpec({"a": spec.str_spec})
        b = spec.KeysSpec({"b": spec.str_spec})

        c = spec.AndSpec([a, b])

        self.assertFalse(c.is_valid({"a": "mjao"}))
        self.assertFalse(c.is_valid({"b": "mjao"}))
        self.assertTrue(c.is_valid({"a": "mjao", "b": "mjao"}))

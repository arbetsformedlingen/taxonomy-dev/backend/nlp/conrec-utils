import unittest
import conrec_utils.textgrid as tg
from conrec_utils.textgrid import TextGrid

class TestTextgrid(unittest.TestCase):
    def test_textgrid(self):
        grid = TextGrid()

        grid.set_string(0, 0, "Hej")
        grid.set_string(1, 1, "August\n!!!")

        self.assertEqual(grid.render_string(), 'Hej      \n   August\n   !!!   ')

    def test_padding(self):
        block = tg.LinesTextBlock.from_string("hej")
        grid = TextGrid()
        grid.set_block(0, 0, tg.PaddedTextBlock(block, {}))
        self.assertEqual(grid.render_string(), "hej")
        grid.set_block(0, 0, tg.PaddedTextBlock(block, {"horizontal": 2}))
        self.assertEqual(grid.render_string(), "  hej  ")
        grid.set_block(0, 0, tg.PaddedTextBlock(block, {"horizontal": 2, "right": 4}))
        self.assertEqual(grid.render_string(), "  hej    ")

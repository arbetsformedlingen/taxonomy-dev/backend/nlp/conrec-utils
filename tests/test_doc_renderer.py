import unittest
import conrec_utils.doc_renderer as dr

class TestDocRenderer(unittest.TestCase):
    def test_it(self):
        x = dr.normalize_nodes(["Hej", [[["Kisse"]]]])
        self.assertTrue(isinstance(x, dr.GroupNode))
        self.assertEqual(x[0].s, "Hej")
        self.assertEqual(x[1].s, "Kisse")

        it = dr.italic("Mjao")
        self.assertTrue("italic" in it.features)
        self.assertEqual("Mjao", it.subnode.s)

    def test_render(self):
        self.assertEqual("Mjao", dr.render_markdown("Mjao"))
        self.assertEqual("*Mjao*", dr.render_markdown(dr.italic("Mjao")))
        self.assertEqual("**Mjao**", dr.render_markdown(dr.bold("Mjao")))
        self.assertEqual("***Mjao***", dr.render_markdown(dr.bold(dr.italic("Mjao"))))
        self.assertEqual("*`Mjao`*", dr.render_markdown(dr.tt(dr.italic("Mjao"))))
        self.assertEqual("Mjao", dr.render_markdown(dr.paragraph("Mjao")))
        self.assertEqual(
            "Mjao\n\nKatt",
            dr.render_markdown(dr.paragraph("Mjao"), dr.paragraph("Katt")))
        self.assertEqual("# Mjao\n\nKatt", dr.render_markdown(dr.section("Mjao", "Katt")))
        self.assertEqual("", dr.render_markdown(dr.bold()))
        self.assertEqual(
            "# Mjao\n\n## Katt\n\nKisse",
            dr.render_markdown(dr.section("Mjao", dr.section("Katt", "Kisse"))))
        self.assertEqual("**Katt Mjao**", dr.render_markdown(dr.bold("Katt", "Mjao")))
        self.assertEqual("**Katt** ***Mjao***",
                         dr.render_markdown(dr.bold("Katt", dr.italic("Mjao"))))

        self.assertEqual(
            "[JobTech](www.jobtechdev.se)",
            dr.render_markdown(dr.link("JobTech").to("www.jobtechdev.se")))
        self.assertEqual(
            "*Visit* [JobTech](www.jobtechdev.se) *tomorrow*",
            dr.render_markdown(
                dr.italic("Visit"),
                dr.link("JobTech").to("www.jobtechdev.se"),
                dr.italic("tomorrow")))
        self.assertEqual("**Mjao** [**hej**](www.google.com) **Kisse**",
                         dr.render_markdown(
                             dr.bold("Mjao",
                                     dr.link("hej").to("www.google.com"),
                                     "Kisse")))


        table = dr.markdown_table()
        table.cell_at(1, 1).set("Mjao")
        self.assertEqual(
            '| | |\n|-|-|\n| | Mjao |',
            dr.render_markdown(table))
        self.assertEqual("```\nprintln('Mjao');\n```", dr.render_markdown(dr.code_block("println('Mjao');")))

    def test_render_table_with_linebreaks(self):
        table = dr.markdown_table()
        table.cell_at(0, 0).set("Title")
        table.cell_at(1, 0).set("An essay on\n\nAugust", " or ", dr.bold(dr.italic("Lilla August")))
        s = dr.render_markdown(table)
        self.assertEqual(s, "| Title |\n|-|\n| An essay on  August or ***Lilla August*** |")
        

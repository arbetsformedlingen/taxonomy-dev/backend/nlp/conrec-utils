from conrec_utils.json_file_map import JsonFileMap
import unittest
import tempfile

class TestJsonFileMap(unittest.TestCase):
    def test_it(self):
        with tempfile.TemporaryDirectory() as tempdir:
            root = tempdir
            m = JsonFileMap(root, prefix="A_")
            m.clear()

            m.put("a", 119)
            self.assertEqual({"a"}, m.keys())
            self.assertEqual(119, m.get("a"))

            m.put("b", 7)
            self.assertEqual(7, m.get("b"))
            self.assertEqual(119, m.get("a"))

            m.remove("a")
            
            m = JsonFileMap(root, prefix="A_")
            self.assertEqual({"b"}, m.keys())

            self.assertFalse("katt" in m)            
            m["katt"] = "August"
            self.assertTrue("katt" in m)
            self.assertEqual("August", m["katt"])

    def test_colls(self):
        with tempfile.TemporaryDirectory() as tempdir:
            root = tempdir
            m = JsonFileMap(root, prefix="A_")
            m["a"] = 0
            m["b"] = 1

            ks = set()
            for k in m.keys():
                ks.add(k)
            self.assertEqual(ks, {"a", "b"})

            vs = set()
            for v in m.values():
                vs.add(v)
            self.assertEqual(vs, {0, 1})
                
            items = set()
            for (k, v) in m.items():
                items.add((k, v))
            self.assertEqual({("a", 0), ("b", 1)}, items)
                
        
if __name__=='__main__':
    unittest.main()

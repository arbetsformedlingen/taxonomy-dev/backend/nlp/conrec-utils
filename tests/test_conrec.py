import unittest
from conrec_utils.env import Env
import conrec_utils.annotated_document_dataset as dataset

class TestDataset(unittest.TestCase):
    def test_split(self):
        items = [{"id": "abc", "order": 0.9, "splitclass": "test"}]
        dataset.write_dataset_index("/tmp/kattskit.csv", items)
        items2 = dataset.read_dataset_index("/tmp/kattskit.csv")
        self.assertEqual(items, items2)

    def test_dataset_index_filename(self):
        self.assertEqual(9, dataset.parse_dataset_index_filename_counter("dataset_index_9.csv"))
        self.assertEqual(None, dataset.parse_dataset_index_filename_counter("dataset_index_abc.csv"))

        for i in range(100):
            self.assertEqual(i, dataset.parse_dataset_index_filename_counter(
                dataset.dataset_index_filename_for_counter(i)))

    def test_weighted_select(self):
        self.assertEqual("a", dataset.weighted_select([("a", 1), ("b", 3)], 0.24))
        self.assertEqual("b", dataset.weighted_select([("a", 1), ("b", 3)], 0.26))
        self.assertEqual("x", dataset.weighted_sample([("x", 1)]))

    def test_regenerate(self):
        result = dataset.regenerate_dataset_index([{"id": "a", "order": 0.1, "splitclass": "mjao"}], ["a", "b", "c"], dataset.default_split_proportions)
        self.assertEqual(3, len(result))
        rmap = {x["id"]:x for x in result}
        self.assertEqual(0.1, rmap["a"]["order"])
        self.assertEqual("train", rmap["a"]["splitclass"])
        assert("b" in rmap)
        assert("c" in rmap)

    def test_dataset(self):
        repo = dataset.load_known_repository(Env.default(), "mentor-api-prod")
        full_dataset = repo.full_dataset()

        splits = full_dataset.splitclass_subsets()
        self.assertEqual(3, len(splits))

        self.assertEqual("mentor-api-prod: Full dataset", full_dataset.title())
        
        train = full_dataset.train_subset()

        self.assertEqual("mentor-api-prod: train", train.title())
        
        test = full_dataset.test_subset()
        validation = full_dataset.validation_subset()
        self.assertEqual(len(train) + len(test) + len(validation), len(full_dataset))
        print(train)
        counter = 0
        for x in full_dataset:
            counter += 1
        self.assertEqual(counter, len(full_dataset))
        self.assertEqual([{"type": "splitclass", "classes": ["test"]}], test.derived_data())

        train_slice = train[2:9]
        self.assertEqual(7, len(train_slice))
        self.assertTrue(len(train_slice) < len(full_dataset))
        self.assertEqual(train_slice[0].id, train[2].id)
        self.assertEqual([{"type": "splitclass", "classes": ["train"]}, {"type": "slice", "lower": 2, "upper": 9}], train_slice.derived_data())

        train_slice2 = full_dataset.derive_from_data(train_slice.derived_data())


        self.assertEqual(train_slice.id_set(), train_slice2.id_set())
        self.assertEqual(train_slice.derived_data(), train_slice2.derived_data())

        train_slice3 = dataset.dataset_from_data_repr(
            Env.default(), train_slice.data_repr())

        self.assertEqual(train_slice3.id_set(), train_slice.id_set())
        

        items_in_slice = []
        for x in train_slice:
            items_in_slice.append(x)

        self.assertEqual(7, len(items_in_slice))
        self.assertEqual(items_in_slice[0].id, train_slice[0].id)

        sha = train_slice[0].id
        self.assertEqual(None, test.get_by_id(sha))
        self.assertEqual(sha, train.get_by_id(sha).id)

        item2 = train_slice[0]
        item3 = item2.update_data(lambda data: {**data, "greeting": "Tjena"})
        self.assertFalse("greeting" in item2.data)
        self.assertTrue("greeting" in item3.data)
        self.assertEqual("Tjena", item3.data["greeting"])

import unittest
import conrec_utils.cli_tools as cli_tools
import pathlib
import tempfile

class SquareTask(cli_tools.Task):
    def __init__(self, offset):
        self.offset = offset

    def inherit_from(self, data):
        return SquareTask(data["offset"])
    
    def perform(self, setup):
        setup.result = setup.a*setup.a + self.offset

class MySetup(cli_tools.Setup):
    def __init__(self):
        super().__init__("MySetup", {"square": SquareTask(0)})
        self.a = 0
        self.result = 0
        

    def configure(self, config):
        self.a = config["a"]

class TestCliTools(unittest.TestCase):
    def test_it(self):
        with tempfile.TemporaryDirectory() as tempdir:
            p = pathlib.Path(tempdir)
            cfgname = p.joinpath("config.json")
            with open(cfgname, "w") as f:
                f.write('{"a": 3}')
            setup = MySetup()
            setup.run(["--config-file", str(cfgname), "--run-task", "square"])
            self.assertEqual(setup.result, 9)
            
        with tempfile.TemporaryDirectory() as tempdir:
            p = pathlib.Path(tempdir)
            cfgname = p.joinpath("config.json")
            with open(cfgname, "w") as f:
                f.write('{"a": 3, "tasks": {"mjao": {"inherit_from": "square", "offset": 100}}}')
            setup = MySetup()
            setup.run(["--config-file", str(cfgname), "--run-task", "mjao"])
            self.assertEqual(setup.result, 109)

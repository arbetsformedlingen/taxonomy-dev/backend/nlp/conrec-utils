import conrec_utils.eval_common as ec
from conrec_utils.eval_common import AlgorithmKey, ClassificationResult
import unittest
import tempfile
from conrec_utils.env import Env
from pathlib import Path

class TestSetup(ec.EvaluationSetup):
    def __init__(self, env):
        self._env = env

    def env(self):
        return self._env

    def name(self):
        return "TestSetup"

    def result_repository_at_path(self, path):
        return ec.CsvClassificationResultRepository(path.joinpath("results.csv"))

class TestEvalCommon(unittest.TestCase):
    def test_algorithm_key(self):
        k = AlgorithmKey("levmar", "3.4")
        self.assertEqual(k, AlgorithmKey("levmar", "3.4"))
        self.assertNotEqual(k, AlgorithmKey("levmar", "3.6"))
        self.assertEqual(("levmar", "3.4"), k.get_comparison_tuple())

        dst = {}
        dst[k] = 119

        self.assertEqual(119, dst[AlgorithmKey("levmar", "3.4")])

    def test_csv_classification_result_repository(self):
        with tempfile.TemporaryDirectory() as tempdir:
            filename = Path(tempdir).joinpath("results.csv")
            repo = ec.CsvClassificationResultRepository(filename)

            ak = AlgorithmKey("levmar", "1.2")
            x = ClassificationResult(ak, "234", "a")
            y = ClassificationResult(ak, "119", "b")
            items = [x, y]

            self.assertEqual(None, x.timestamp_seconds_since_epoch())
            x3 = x.at_now()
            self.assertTrue(isinstance(x3.timestamp_seconds_since_epoch(), float))
            self.assertNotEqual(x, x3)

            repo.save(items)
            items2 = repo.load()

            self.assertEqual(items, items2)

            x2 = ClassificationResult(ak, "234", "b")
            z = ClassificationResult(ak, "mjao", "c")
            repo.extend([x2, z])

            items3 = repo.load()

            self.assertEqual(items3, [y, x2, z])

            repo.extend([x3])
            self.assertEqual(repo.load(), [y, z, x3])

    def test_setup(self):
        with tempfile.TemporaryDirectory() as tempdir:
            env = Env.at_home(Path(tempdir))

            setup = TestSetup(env)
            k = AlgorithmKey("levmar", "1.2")

            self.assertEqual(0, len(setup.algorithm_repository_keys()))
            ar = setup.algorithm_repository(k)

            self.assertEqual({}, ar.load_metadata())
            ar.save_metadata({"number_of_components": 3})
            self.assertEqual({"number_of_components": 3}, ar.load_metadata())
            
            self.assertEqual([k], setup.algorithm_repository_keys())
            self.assertEqual(k, ar.algorithm_key())

            z = ClassificationResult(k, "mjao", "c")

            ar.result_repository().extend([z])

            self.assertEqual([z], ar.result_repository().load())

    def test_classification_reporter(self):
        samples = [ec.Sample(str(i), i, i < 12)
                   for i in range(20)]

        class GoodAlgo(ec.ClassificationAlgorithm):
            def key(self):
                return ec.AlgorithmKey("good-algorithm")

            def classify_sample(self, sample):
                return sample.features() < 11

        class BadAlgo(ec.ClassificationAlgorithm):
            def key(self):
                return ec.AlgorithmKey("bad-algorithm")

            def classify_sample(self, sample):
                return sample.features() < 9 or sample.features() > 17

        results = [algo.make_classified_sample(sample)
                   for sample in samples
                   for algo in [GoodAlgo(), BadAlgo()]]

        self.assertEqual(2*20, len(results))

        reporter = ec.MetricTableClassificationReporter()
        metrics = reporter.compute_metrics(results)
        ec.metric_item_spec.coll().check(metrics)
        self.assertEqual(6, len(metrics))
        s = reporter.render_summary_text(metrics)
        self.assertTrue(isinstance(s, str))
        print(s)

    def test_sample(self):
        x = ec.Sample("id", "mjao", "a")
        y = x.with_source_data("M")
        self.assertEqual(y._source_data, "M")

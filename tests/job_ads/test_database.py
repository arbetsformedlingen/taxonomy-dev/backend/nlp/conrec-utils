import conrec_utils.job_ads.database as db
import tests.job_ads_testdata as jat
import unittest
import tempfile
from pathlib import Path

class TestDatabase(unittest.TestCase):
    def test_database(self):
        data = jat.InMemoryJobAdFileSource()

        with tempfile.TemporaryDirectory() as td:
            filename = Path(td).joinpath("jobads.db")
            database = db.SqliteJobAdsDatabase(filename)
            self.assertFalse(database.is_initialized())
            self.assertEqual(0, database.get_source_url_count())
            database.initialize_db()
            self.assertTrue(database.is_initialized())
            database.add_source_url("hej")
            self.assertEqual(1, database.get_source_url_count())
            database.add_source_url("hej")
            self.assertEqual(1, database.get_source_url_count())
            self.assertEqual({"hej"}, database.get_source_urls())
            database.add_source_url("mjao")
            self.assertEqual(2, database.get_source_url_count())
            self.assertEqual({"hej", "mjao"}, database.get_source_urls())
            self.assertEqual(0, database.count())
            database.add_job_ad_sources(data.list_files())
            database.add_job_ad_sources(data.list_files())
            self.assertEqual(5, database.count())

            
            with database.sequential_access() as access:
                ids = []
                m = {}
                for item in access:
                    x = item["id"]
                    self.assertTrue(isinstance(x, str))
                    ids.append(x)
                    m[x] = item
                self.assertEqual(5, len(ids))

            with database.random_access() as access:
                for i in ids:
                    self.assertEqual(access[i], m[i])
            
            

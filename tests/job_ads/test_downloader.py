import conrec_utils.job_ads.downloader as downloader
import unittest
from pathlib import Path
import tempfile

class TestDownloader(unittest.TestCase):
    def test_downloadable_file(self):
        with tempfile.TemporaryDirectory() as tmpdir:
            root = Path(tmpdir)
            f = downloader.JobAdsDownloadableFile(root, 'https://data.jobtechdev.se/annonser/historiska/2006.zip')
            info = f.filename_info()
            self.assertEqual(2006, info.year)
            self.assertFalse(f.is_downloaded())
            self.assertFalse(f.is_unpacked())
            self.assertFalse(f.is_ready())
        
    def test_name(self):
        self.assertEqual("data_jobtechdev_se", downloader.dataset_name_from_url("https://data.jobtechdev.se"))

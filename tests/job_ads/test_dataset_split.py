import unittest
from conrec_utils.env import Env
import tempfile
import conrec_utils.dataset_common as dataset_common
import conrec_utils.job_ads.dataset_split as dss
import conrec_utils.spec as spec
from pathlib import Path
import random

class TestDatasetSplit(unittest.TestCase):
    def test_dataset_split(self):
        with tempfile.TemporaryDirectory() as tempdir:
            env = Env.at_home(Path(tempdir))
        
            split = dss.DatasetSplit(["a.com", "b.com"], {str(i):dataset_common.test.label()
                                                           for i in range(10)})

            self.assertNotEqual(split, dss.DatasetSplit.empty())

            self.assertEqual(dataset_common.train.label(), split.label({"dataset_url": "a.com", "id": "93"}))
            self.assertEqual(dataset_common.train.label(), split.label({"dataset_url": "a.com", "id": "10"}))
            self.assertEqual(dataset_common.test.label(), split.label({"dataset_url": "a.com", "id": "9"}))
            self.assertEqual(dataset_common.test.label(), split.label({"dataset_url": "b.com", "id": "8"}))        
            self.assertEqual(None, split.label({"dataset_url": "c.com", "id": "9"}))

            self.assertEqual({dataset_common.test.label(): 10}, split.distribution())
            self.assertEqual({dataset_common.test.label(), dataset_common.train.label()}, split.splitkey_set())

            self.assertEqual({"b": 3, "c": 30},
                             dss._distrib_diff(
                                 {"a": 3, "b": 4, "c": 30},
                                 {"a": 9, "b": 1, "d": 14}))

            self.assertEqual(set(["a.com", "b.com"]), split.source_urls())
            self.assertEqual(set([str(i) for i in range(10)]), split.filter_ids_by_label("test"))

            with tempfile.TemporaryDirectory() as tempdir:
                filename = Path(tempdir).joinpath("split.csv")

                split.write_to_csv(filename)

                split2 = dss.DatasetSplit.read_from_csv(filename)

                self.assertEqual(split, split2)


            split3 = split.extend_source_urls_to_count(["a.com", "b.com", "d.com", "e.com"], 2)
            self.assertEqual(split, split3)

            split4 = split.extend_source_urls_to_count(["a.com", "b.com", "d.com", "e.com"], 3)
            self.assertNotEqual(split, split4)
            self.assertEqual(set(["a.com", "b.com", "d.com"]), split4.source_urls())

            class MockDb:
                def random_samples(self, fmapping, urls, n):
                    tmp = list(range(1000, 10000))
                    random.shuffle(tmp)
                    return [{"id": str(x), "occupation": {"concept_id": "15DU_axL_nRT"}} for x in tmp[0:n]]

            split5 = dss.extend_dataset_split_with_samples(split4, {"test": 12, "validation": 30}, MockDb())

            self.assertEqual({"test": 12, "validation": 30}, split5.distribution())

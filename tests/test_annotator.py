import unittest
import conrec_utils.annotator as annotator
import tempfile
import pathlib
import csv

class TestAnnotator(unittest.TestCase):
    def test_annotator(self):
        with tempfile.TemporaryDirectory() as tempdir:
            path = pathlib.Path(tempdir)
            filename = path.joinpath("table.csv")
            with open(filename, "w") as f:
                w = csv.writer(f)
                w.writerow(["Dish"])
                w.writerow(["Fried herring"])
                w.writerow(["Janssons frestelse"])
                w.writerow(["Valthornsmussla"])
                w.writerow(["Falafel"])

            def make_model():
                return annotator.CsvItemsModel([
                    annotator.Choice("tasty", "Tasty"),
                    annotator.Choice("disgusting", "Not so tasty")], "tastiness", filename)

            with make_model() as model:
                choices = model.get_choices()
                self.assertEqual(["tasty", "disgusting"], [x.key for x in choices])
                items = model.get_items()
                self.assertEqual([0, 1, 2, 3], [item.key() for item in items])
                for item in items:
                    self.assertTrue(isinstance(item.text(), str))
                    self.assertTrue(isinstance(item.data(), dict))
                    self.assertEqual(None, model.get_item_annotation(item.key()))
                model.set_item_annotation(2, "tasty")
                    
            with make_model() as model:
                items = model.get_items()
                for item in items:
                    print(item.text())
                    expected = "tasty" if item.key() == 2 else None
                    self.assertEqual(expected, model.get_item_annotation(item.key()))

            # Just experimenting
            # with make_model() as model:
            #     annotator.annotate_items(model)
            # with open(filename, "r") as f:
            #     print(f.read())

                
                
            
            
            

import unittest
import conrec_utils.metric_accumulator as adu
from conrec_utils.jaccard import JaccardIndex, JaccardAnalysis, zero_jaccard_index

class TestMetricAccumulator(unittest.TestCase):
    def check_property_metric_data_conversion(self, acc):
        m = acc.get_metrics()
        self.assertEqual(m, acc.metrics_from_data(acc.data_from_metrics(m)))

    def check_property_addition(self, acc_prototype):
        a = acc_prototype()
        b = acc_prototype()
        c = acc_prototype()

        x0 = {2, 3}
        y0 = {3, 4, 9, 34}
        a.accumulate(x0, y0)
        c.accumulate(x0, y0)

        x1 = {20, 30}
        y1 = {30, 40}
        b.accumulate(x1, y1)
        c.accumulate(x1, y1)

        am = a.get_metrics()
        bm = b.get_metrics()
        cm = c.get_metrics()

        self.assertEqual(c.add(am, bm), cm)
    
    def test_jaccard_acc(self):
        acc = adu.JaccardAccumulator()
        acc.accumulate({2, 3}, {3, 4})
        self.assertEqual(acc.get_metrics(), JaccardIndex(1, 1, 1))
        self.check_property_metric_data_conversion(acc)
        self.check_property_addition(adu.JaccardAccumulator)
        
    def test_overall_acc(self):
        acc = adu.OverallJaccardAccumulator()
        acc.accumulate({2, 3}, {3, 4})
        acc.accumulate({}, {5, 6})
        self.check_property_metric_data_conversion(acc)        
        self.assertEqual(JaccardIndex(1, 1, 3), acc.get_metrics())
        self.check_property_addition(adu.OverallJaccardAccumulator)

    def test_composite_acc(self):
        acc = adu.CompositeMetricAccumulator([adu.JaccardAccumulator(), adu.OverallJaccardAccumulator()])
        acc.accumulate({2, 3}, {3, 4, 5})
        self.check_property_metric_data_conversion(acc)
        self.assertEqual({"JaccardAccumulator": JaccardIndex(1, 1, 2),
                          "OverallJaccardAccumulator": JaccardIndex(1, 1, 2)}, acc.get_metrics())
        self.check_property_addition(lambda: adu.CompositeMetricAccumulator([adu.JaccardAccumulator(), adu.OverallJaccardAccumulator()]))

    def test_binary_classification_matrix(self):
        mat = adu.BinaryClassificationMatrix.zero()
        mat.add(False, False)
        mat.add(False, False)
        mat.add(True, True)

        self.assertEqual(1.0, mat.precision().value())
        self.assertEqual(1.0, mat.recall().value())
        self.assertEqual(1.0, mat.f1().value())
        mat.add(True, False)
        self.assertEqual(1.0, mat.precision().value())
        self.assertTrue(mat.recall().value() < 1.0)
        self.assertTrue(mat.f1().value() < 1.0)
        mat.add(False, True)
        self.assertTrue(mat.precision().value() < 1.0)
        self.assertTrue(mat.recall().value() < 1.0)
        self.assertTrue(mat.f1().value() < 1.0)
        
    def test_pair_accumulator(self):
        acc = adu.PairAccumulator()

        acc.accumulate_pairs([(0, 0), (0, 0), (1, 1)])
        self.assertEqual(3, acc.count())
        self.assertEqual({0, 1}, acc.all_classes())

        mat = acc.binary_classification_matrix(1)
        self.assertEqual(2, mat.true_negative())
        self.assertEqual(1, mat.true_positive())
        self.assertEqual(3, mat.count())

    def frac_test(self):
        f0 = adu.Frac(2, 4)
        f1 = adu.Frac(10, 12)
        f2 = adu.Frac(0, 0)

        self.assertEqual(None, f2.value())
        self.assertEqual(f0.value(), 0.5)
        self.assertEqual((f0 + f1).value(), 0.75)
        self.assertEqual((f0 + f1 + f2).value(), 0.75)
        f0_ = f0.normalize()
        self.assertEqual(f0.value(), 0.5)
        self.assertEqual(f0._num, 0.5)
        

import unittest
import os
import tempfile
from conrec_utils.csv_utils import string_column, float_column, CsvConfig


class TestCsv(unittest.TestCase):
    def test_csv(self):
        with tempfile.TemporaryDirectory() as tempdir:
            filename = os.path.join(tempdir, "sample_data.csv")
            
            config = CsvConfig([string_column("id", "sha"), float_column("order"), string_column("splitclass"), float_column("katt").nullable()])
        
            sample_data = [{"id": "0", "order": 0.34, "splitclass": "train", "katt": 9.3},
                           {"id": "1", "order": 0.44, "splitclass": "test", "katt": None}]

            config.write_dicts(filename, sample_data)
            data2 = config.read_dicts(filename)

            self.assertEqual(sample_data, data2)


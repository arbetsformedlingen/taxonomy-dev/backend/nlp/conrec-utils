import unittest
from conrec_utils import jae_synonyms
from conrec_utils.env import Env

s = jae_synonyms.load_synonyms(Env.default())

class TestSynonyms(unittest.TestCase):
    def test_synonyms(self):
        self.assertLess(0, len(s))
        group = s.synonym_groups['competence/Arbetslivserfarenhet']
        self.assertTrue(isinstance(group, list))
        self.assertTrue(isinstance(group[0], dict))

import conrec_utils.eval_common as ec
import conrec_utils.text2ssyk_evaluation as e
import unittest
import tempfile
from pathlib import Path
from conrec_utils.env import Env
from tests.job_ads_testdata import TestConfig
import conrec_utils.dataset_common as dataset_common
import conrec_utils.job_ads.config as config

class TestSetup(e.EvaluationSetup):
    def __init__(self, env):
        super().__init__()
        self._env = env
        setup = config.Setup(TestConfig(self._env))
        setup.generate_datasplit()
        self._jats = setup

    def jobads_setup(self):
        return self._jats

    def evaluation_splitkeys(self):
        return [dataset_common.test.label()]
        
    def env(self):
        return self._env

class MyAlgo(ec.ClassificationAlgorithm):
    def key(self):
        return ec.AlgorithmKey("my-algo", "0.1.0")

    def classify_sample(self, sample):
        if not(isinstance(sample.features(), str)):
            return -1
        print(f"Classify sample {sample}")
        return "2619"
    
class Text2SsykEvaluation(unittest.TestCase):
    def test_setup(self):
        with tempfile.TemporaryDirectory() as tempdir:
            setup = TestSetup(Env.at_home(Path(tempdir)))
            algo = MyAlgo()
            setup.evaluate(algo)
            results = list(setup.load_all_results())
            self.assertEqual(1, len(results))
            for x in results:
                assert(isinstance(x, ec.ClassificationResult))
                
            classified_samples = setup.make_classified_samples(results)
            self.assertEqual(1, len(classified_samples))
            for x in classified_samples:
                assert(isinstance(x, ec.ClassifiedSample))
            setup.make_report()

    def test_batched_visit(self):
        with tempfile.TemporaryDirectory() as tempdir:
            setup = TestSetup(Env.at_home(Path(tempdir)))

            split = setup.jobads_setup().load_datasplit()
            print(f"--- Number of examples in database: {setup.jobads_setup().populated_database().count()}")
            print(f"--- SPLIT: {split.id_label_map()}")
            
            visited = []
            def visit(x):
                visited.append(x.data)
            train_label = dataset_common.train.label()
            setup.visit_samples([train_label], visit, message="Training...")
            self.assertEqual(3, len(visited))
            v0 = visited[0]
            with setup.random_access() as m:
                sample = m[v0.sample_id()]
            self.assertEqual(v0.sample_id(), sample.sample_id())

            batches = []
            def visit_batch(b0):
                b = [x.data for x in b0]
                batches.append(b)
                for x in b:
                    self.assertTrue(isinstance(x, ec.Sample))
                    self.assertTrue(isinstance(x.source_data(), dict))

            setup.visit_samples([train_label], visit_batch, message="Training...", batch_size=2)

            self.assertEqual(2, len(batches))
            self.assertEqual([[visited[0], visited[1]], [visited[2]]], batches)

import unittest
from conrec_utils.interval import Interval


class TestText(unittest.TestCase):
    def test_interval(self):
        self.assertEqual(Interval(3, 4), Interval(3, 4))
        self.assertNotEqual(Interval(3, 4), Interval(3, 5))
        x = Interval(3, 9)
        self.assertEqual(x.lower, 3)
        self.assertEqual(x.upper, 9)
        self.assertEqual(len(x), 6)
        self.assertEqual(x[0], 3)
        self.assertEqual(x[1], 4)
        self.assertEqual("bc", Interval(1, 3).slice_object("abcd"))

        self.assertEqual(Interval(3, 5), Interval.from_tuple((3, 5)))
        self.assertEqual((3, 5), Interval(3, 5).to_tuple())
        

        


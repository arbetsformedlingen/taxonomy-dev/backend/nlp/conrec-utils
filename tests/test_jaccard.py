import unittest
from conrec_utils.jaccard import JaccardIndex, JaccardAnalysis, zero_jaccard_index

class TestJaccard(unittest.TestCase):
    def test_jaccard(self):
        ja = JaccardAnalysis({1, 2, 3}, {2, 4, 6})
        j = ja.jaccard_index
        
        self.assertEqual(0.2, j.value)

        data = j.data()

        j2 = JaccardIndex.from_data(data)

        self.assertEqual(j, j2)



import unittest
from conrec_utils.jae_api import JobadEnricher, jae_document_input, flat_candidates

class TestJaeApi(unittest.TestCase):
    def test_jae_api(self):
        jae = JobadEnricher()
        input_docs = [jae_document_input("mjao", "Hej", "Vi söker en javautvecklare")]
        enriched_docs = jae.enrich_documents(input_docs)
        self.assertEqual(1, len(enriched_docs))
        self.assertEqual(
            "javautvecklare",
            enriched_docs[0]["enriched_candidates"]["occupations"][0]["term"])
        cands = flat_candidates(enriched_docs[0])
        cand = cands[0]
        self.assertTrue("synonym_key" in cand)
        self.assertTrue("synonym_type" in cand)

import unittest
import tempfile
from pathlib import Path
import conrec_utils.json_cache as jc

class TestJsonCache(unittest.TestCase):
    def test_json_cache(self):
        with tempfile.TemporaryDirectory() as d:
            path = Path(d).joinpath("some", "deep", "directory", "cache.json")
            with jc.JsonCache(path) as tempdata:
                self.assertEqual(tempdata, {})
                tempdata["a"] = 119
            self.assertTrue(path.exists())
            try:
                with jc.JsonCache(path) as tempdata:
                    self.assertEqual(tempdata, {"a": 119})
                    tempdata["b"] = 120;
                    raise RuntimeError("Mjao")
            except RuntimeError:
                pass
            with jc.JsonCache(path) as tempdata:
                self.assertEqual({"a": 119, "b": 120}, tempdata)

import unittest
from conrec_utils.interval import Interval
from conrec_utils import text


class TestText(unittest.TestCase):
    def test_sentence_generator(self):
        s = "Hej! Vad vill du jobba? "
        result = list(text.SentenceSegmenter().segment(s))
        self.assertEqual(result, [Interval(0, 4), Interval(5, 23)])
        self.assertEqual(result[1].slice_object(s), "Vad vill du jobba?")

        


from conrec_utils import time_utils
import unittest


class TestTimeUtils(unittest.TestCase):
    def test_compute_time_estimate(self):
        progress = 0.25
        elapsed_time = 2

        est = time_utils.compute_time_estimate(progress, elapsed_time)
        self.assertEqual(8.0, est["total_time"])
        self.assertEqual(6.0, est["remaining_time"])
        info = time_utils.format_time_estimate(est)
        self.assertTrue(isinstance(info, str))

    def test_time_format(self):
        self.assertEqual("30 seconds", time_utils.format_seconds(30))
        self.assertEqual("1 hours", time_utils.format_seconds(3600))
        self.assertEqual("1 hours, 3 seconds", time_utils.format_seconds(3603))
        self.assertEqual("1 hours, 10 minutes", time_utils.format_seconds(4203))
        self.assertEqual("0 seconds", time_utils.format_seconds(0))

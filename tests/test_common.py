import unittest
from conrec_utils import common


elements = [3, 4, 9, 119]

class MyIterableClass:
    def __len__(self):
        return len(elements)

    def __getitem__(self, i):
        return elements[i]

    def __iter__(self):
        return common.ArrayIterator(self)


class State:
    def __init__(self):
        self.current = None

class MockDbConnection:
    def __init__(self, state):
        self._state = state
    
    def __enter__(self):
        self._state.current = "entered"
        return range(4)

    def __exit__(self, *args):
        self._state.current = "exited"

def square_numbers(state):
    with MockDbConnection(state) as numbers:
        for number in numbers:
            yield number*number

# Will a generator always terminate when no one is consuming values any longer?
            
class TestCommon(unittest.TestCase):
    def test_yield_from_context_mgr0(self):
        state = State()
        for i, x in enumerate(square_numbers(state)):
            self.assertEqual(i*i, x)
            self.assertEqual(state.current, "entered")
        self.assertEqual(state.current, "exited")
    
    def test_yield_from_context_mgr1(self):
        state = State()
        pairs = enumerate(square_numbers(state))
        for i, x in pairs:
            self.assertEqual(state.current, "entered")
            self.assertEqual(i*i, x)
            if i > 2:
                break
        pairs = None
        self.assertEqual(state.current, "exited")

    def test_yield_from_context_mgr2(self):
        state = State()
        pairs = enumerate(square_numbers(state))
        for i, x in pairs:
            self.assertEqual(state.current, "entered")
            self.assertEqual(i*i, x)
            if i > 2:
                break
        self.assertEqual(state.current, "entered")                
    
    def test_group_by(self):
        def mod2(x):
            return x % 2
        result = common.group_by(mod2, [1, 2, 3, 4, 5])
        self.assertEqual(result, {0: [2, 4], 1: [1, 3, 5]})
        self.assertEqual({0: 2, 1: 3}, common.frequencies(mod2, [1, 2, 3, 4, 5]))

    def test_frequencies(self):
        self.assertEqual({3: 4, 1: 1}, common.frequencies([3, 3, 3, 3, 1]))

    def test_select_keys(self):
        self.assertEqual(
            {"a": 3},
            common.select_keys({"a": 3, "b": 4}, ["a", "c"]))

        self.assertEqual(
            {"a": 3},
            common.remove_keys({"a": 3, "b": 4}, ["b", "c"]))

    def test_iterable(self):
        result = []
        for x in MyIterableClass():
            result.append(x)
        self.assertEqual(result, elements)

    def test_partition_by(self):
        self.assertEqual([[1, 3, 5], [2, 8, 120], [17]], common.partition_by(lambda x: x % 2, [1, 3, 5, 2, 8, 120, 17]))

    def test_coll_preds(self):
        self.assertTrue(common.is_stringlike("asdfsadf"))
        self.assertTrue(common.is_common_seq([3, 4, 5]))
        self.assertFalse(common.is_common_seq({"a": 3}))
        self.assertTrue(common.is_iterable([3, 4, 5]))
        self.assertFalse(common.is_iterable(9))

    def test_or_some(self):
        self.assertEqual(3, common.or_some(None, 3, None, 5, 6))

    def test_inc(self):
        self.assertEqual(4, common.inc(3))

    def test_object_walker(self):
        x = {}
        common.ObjectWalker(x).new_or_existing("a", {}).new_or_existing("b", {})["c"] = 119
        common.ObjectWalker(x).new_or_existing("a", {}).new_or_existing("b", {})["d"] = 120

        self.assertEqual(x, {"a": {"b": {"c": 119, "d": 120}}})

    def test_segments_from_inds(self):
        s = common.segments_from_inds([1, 2, 5, 6, 7, 10, 20])
        self.assertEqual(s, [(1, 3), (5, 8), (10, 11), (20, 21)])

    def test_delay(self):
        k = common.Delay(lambda x: x*x*x, 10)
        self.assertEqual(k.get(), 1000)
        self.assertEqual(k.get(), 1000)

    def test_unique_value_counter(self):
        c = common.NonRepeatingValueCounter()
        self.assertEqual(1, c.get(119))
        self.assertEqual(1, c.get(119))
        self.assertEqual(1, c.get(119))
        self.assertEqual(1, c.get(119))
        self.assertEqual(1, c.get(119))
        self.assertEqual(1, c.get(119))
        self.assertEqual(2, c.get(118))
        self.assertEqual(2, c.get(118))
        self.assertEqual(2, c.get(118))
        self.assertEqual(3, c.get(119))
        self.assertEqual(3, c.get(119))

    def test_replace_many(self):
        self.assertEqual(
            "abc-=xyz",
            common.replace_many("abc\r\nxyz", {"\r": "-", "\n": "="}))

    def test_generic_flatten(self):
        self.assertEqual(
            [1, 2, 3],
            common.generic_flatten(lambda x: x if isinstance(x, list) else None, [1, [[[[2]], [[[[[3]]]]]]]]))
        self.assertEqual(
            [1, 2, 3],
            common.basic_flatten([1, [[[[2]], [[[[[3]]]]]]]]))

    def test_merge_with(self):
        A = {"a": 3, "b": 4}
        B = {"b": 9, "c": 100}
        C = {"a": 3, "b": 13, "c": 100}
        self.assertEqual(C, common.merge_with(lambda a, b: (a or 0) + (b or 0), A, B))
        self.assertEqual(C, common.add_maps(A, B))
        
    def test_dense_from_sparse(self):
        self.assertEqual(
            ["August", "Jonas"],
            common.dense_from_sparse({(100,): "Jonas",
                                      (0,): "August"}))
        self.assertEqual(
            [["August", None], [None, "Jonas"]],
            common.dense_from_sparse({(100, 34): "Jonas",
                                      (0, 19): "August"}))
        self.assertEqual(
            [["August"], ["Jonas"]],
            common.dense_from_sparse({("row1", 0): "Jonas",
                                      ("row0", 0): "August"}))

    def test_unique_pred(self):
        p = common.unique_pred()
        self.assertEqual([4, 1, 2, 3], [x for x in [4, 1, 1, 2, 1, 2, 2, 3, 1, 3, 4] if p(x)])


    def test_mangle_path(self):
        self.assertEqual("a_b", common.mangle_path("a/b"))
        self.assertEqual("a-b", common.mangle_path("a.b"))

    def test_lazy_seq(self):
        s = common.LazySeq.from_iterator((x for x in range(4)))

        for i in range(4):
            s.next()
            s.next()
            s.next()
            self.assertEqual(s.first(), i)
            self.assertEqual(s.first(), i)
            self.assertEqual(s.first(), i)
            self.assertEqual(list(s), list(range(i, 4)))
            s = s.next()
        self.assertIsNone(s)

        for coll in [[3, 9, 7], [9], range(9)]:
            self.assertEqual(list(coll), list(common.LazySeq.from_collection(coll)))

        self.assertEqual([2, 3], list(common.LazySeq.from_collection((x for x in [2, 3]))))

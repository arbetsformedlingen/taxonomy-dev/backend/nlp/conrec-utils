import unittest
from conrec_utils.env import Env
import conrec_utils.annotated_document_dataset as add
import conrec_utils.annotated_document_evaluation as ade
import conrec_utils.token_classification as tc
from conrec_utils.jaccard import JaccardIndex, JaccardAnalysis, zero_jaccard_index
import tempfile
import pathlib
import json
from jobtech_taxonomy_library_python.generated.defs import taxonomy_version_map

env = Env.default()

from distutils.dir_util import copy_tree

base_context = ade.ReportingContext(env, taxonomy_version_map)

annotations = [{'preferred-label': 'Sjuksköterska, grundutbildad', 'sentiment': 'MUST_HAVE', 'type': 'occupation-name', 'start-position': 0, 'end-position': 14, 'concept-id': 'bXNH_MNX_dUR', 'matched-string': 'sjuksköterskor', 'annotation-id': 0}, {'preferred-label': 'Kollektivavtal', 'sentiment': 'OFFERED', 'type': 'skill', 'start-position': 463, 'end-position': 477, 'concept-id': 'z7wE_9xb_3jX', 'matched-string': 'kollektivavtal', 'annotation-id': 4}, {'preferred-label': 'Narkossjuksköterska', 'sentiment': 'NOT_RELEVANT', 'type': 'keyword', 'start-position': 593, 'end-position': 612, 'concept-id': 'jCqK_dxK_w65', 'matched-string': 'narkossjuksköterska', 'annotation-id': 5}]

sample_item = add.DatasetItem(None, '1abfa5e75fbc1d6a2601a9989638a16125d6eb60', 0.00010294308343750114, 'train', {'meta': {'name': 'Sjuksköterskor till barn- och ungdomskliniken', 'user': 'hannes'}, 'sha1': '1abfa5e75fbc1d6a2601a9989638a16125d6eb60', 'annotations': [{'preferred-label': 'Sjuksköterska, grundutbildad', 'sentiment': 'MUST_HAVE', 'type': 'occupation-name', 'start-position': 0, 'end-position': 14, 'concept-id': 'bXNH_MNX_dUR', 'matched-string': 'sjuksköterskor', 'annotation-id': 0}, {'preferred-label': 'Sjuksköterskor', 'sentiment': 'NOT_RELEVANT', 'type': 'ssyk-level-3', 'start-position': 149, 'end-position': 163, 'concept-id': 'jzaN_Vqk_TP1', 'matched-string': 'sjuksköterskor', 'annotation-id': 2}, {'preferred-label': 'Sjuksköterska', 'sentiment': 'NOT_RELEVANT', 'type': 'keyword', 'start-position': 223, 'end-position': 236, 'concept-id': 'jrjV_wcz_fxg', 'matched-string': 'sjuksköterska', 'annotation-id': 3}], 'text': 'Sjuksköterskor till barn- och ungdomskliniken\nOm oss\nBemanning med omsorg.\n\nOfelia vård månar om den svenska sjukvården, våra medborgare och om våra sjuksköterskor. Vi är Sveriges tryggaste bemanningsföretag för dig som är sjuksköterska. Vi har uppdragen som du söker och villkoren som du förtjänar.\nOfelia vård är ett auktoriserat bemanningsföretag med medlemskap i Kompetensföretagen. Detta är en garanti att vi följer arbetsmarknadens lagar och regler. Vi har kollektivavtal med Vårdförbundet, Läkarförbundet och Unionen. Detta är en självklarhet för oss!\nMed en VD med lång erfarenhet som narkossjuksköterska och erfarna konsultchefer med vårderfarenhet så vet vi vad som är viktigt för dig som konsult ute på uppdrag. Vi är upphandlade i hela Sverige och finns tillgängliga för dig dygnet runt.\nOfelia vård finns här för dig som sjuksköterska som vill arbeta för de villkor som du förtjänar, den arbetsmiljö som du vill ha och den flexibilitet som livet ibland kräver.\n\n\nVi har utöver detta en mängd förmånlig rabatter för alla våra anställda\n\nDina arbetsuppgifter\nSedvanligt arbete för sjuksköterska på barn-och ungdomsklinik\n\n\nKontakta oss gärn för mer information på info@ofelia.se 010-178 36 20\n\nVi hjälper självfallet till med resa och boende vid behov.\n\n\nDin profil\nVi söker dig som är legitimerad sjuksköterska med relevant erfarenhet.\n\nVarmt välkommen med dina och önskemål och ansökan.'})

sample_repo = add.InMemoryRepository([sample_item])

def butlast(x):
    return x[0:max(0, len(x) - 1)]

def report_butlast(dataset_item_reporter):
    dataset_item_reporter.report_annotations(butlast(dataset_item_reporter.item.data["annotations"]))

dummy_annotations = [{"concept-id": "dhgR_9jk_95r",
                      "start-position": 0,
                      "end-position": 10}]

def is_nonempty_string(x):
    return isinstance(x, str) and 0 < len(x)

def call_by_method(client, method, *args, **kvargs):
    if method == "post":
        return client.post(*args, **kvargs)
    else:
        return client.get(*args, **kvargs)

class CheatingAlgorithm(ade.AbstractAlgorithm):
    def preprocess_for_dataset(self, evaluation_context, dataset):
        ade.evaluation_context_spec.check(evaluation_context)
        add.dataset_spec.check(dataset)
        return "data for cheating..."

    def process_dataset_item(self, evaluation_context, preprocessed_data, dataset_item):
        ade.evaluation_context_spec.check(evaluation_context)
        add.dataset_item_spec.check(dataset_item)
        assert(preprocessed_data == "data for cheating...")
        annotations = butlast(dataset_item.data["annotations"])
        return {"annotations": annotations}

class TestEvaluation(unittest.TestCase):
    def check_report_rendering(self, context, result_repo, tempdir):
        report_path = pathlib.Path(tempdir).joinpath("report")
        self.assertFalse(report_path.joinpath("index.md").exists())
        writer = ade.MarkdownReportWriter(report_path)
        context.with_writer(writer).render_report(result_repo)
        self.assertTrue(report_path.joinpath("index.md").exists())
    
    def test_results_repository(self):
        with tempfile.TemporaryDirectory() as tempdir:
            rr = ade.ResultRepository(pathlib.Path(tempdir))
            repositories = [rr]
            for r in repositories:
                self.assertEqual([], r.list_item_keys("x"))
                self.assertEqual([], r.list_dataset_keys())
                
                r.set_item_data("ds0", "a", {"message": "katt"})
                self.assertEqual(["ds0"], r.list_dataset_keys())
                self.assertEqual(["a"], r.list_item_keys("ds0"))
                self.assertEqual([], r.list_item_keys("ds1"))

                r.set_item_data("ds1", "b", {"message": "August"})
                self.assertEqual({"ds0", "ds1"}, set(r.list_dataset_keys()))
                self.assertEqual(["a"], r.list_item_keys("ds0"))
                self.assertEqual(["b"], r.list_item_keys("ds1"))

                self.assertEqual({"message": "August"}, r.get_item_data("ds1", "b"))
                self.assertEqual({"message": "katt"}, r.get_item_data("ds0", "a"))
                self.assertEqual(None, r.get_item_data("ds0", "b"))

                r.set_dataset_info("ds0", {"url": "mjao"})
                self.assertEqual({"url": "mjao"}, r.get_dataset_info("ds0"))
                self.assertEqual(None, r.get_dataset_info("ds1"))

            p = ade.mkdirp_val(rr.dataset_subdir("ds0", "debug"))
            self.assertTrue(p.exists())
            p2 = ade.mkdirp_val(rr.item_subdir("ds0", "a", "debug2"))
            self.assertTrue(p2.exists())



                
    
    def test_annotated_char(self):
        a = ade.AnnotatedCharacter(3, "abc", "keyword")
        a2 = ade.AnnotatedCharacter(3, "abc", "keyword")
        b = ade.AnnotatedCharacter(4, "abc", "keyword")

        to_compare = [a, a2, b]
 
        for x in to_compare:
            for y in to_compare:
                self.assertEqual((x < y), (x.character_index < y.character_index))
                self.assertEqual((x <= y), (x.character_index <= y.character_index))
                self.assertEqual((x == y), (x.character_index == y.character_index))
                self.assertEqual((x != y), (x.character_index != y.character_index))
                self.assertEqual((x > y), (x.character_index > y.character_index))
                self.assertEqual((x >= y), (x.character_index >= y.character_index))

        chars_a = ade.annotated_characters_from_annotations(annotations)
        chars_b = ade.annotated_characters_from_annotations(annotations[0:-1])

        self.assertEqual(1.0, JaccardAnalysis(chars_a, chars_a).jaccard_index.value)

        jab = JaccardAnalysis(chars_a, chars_b).jaccard_index.value
        self.assertTrue(0.0 < jab)
        self.assertTrue(jab < 1.0)

        chars_c = ade.annotated_characters_from_annotations(annotations[-1:])

        jac = JaccardAnalysis(chars_b, chars_c).jaccard_index.value

        self.assertEqual(0.0, jac)

        self.assertEqual(
            JaccardIndex(3, 9, 0),
            JaccardIndex(1, 4, 0) + JaccardIndex(2, 5, 0))
        
    def test_overlaps(self):
        a = ade.annotated_characters_from_annotations(annotations[1:])
        b = ade.annotated_characters_from_annotations(annotations[:-1])
        overlaps = ade.segment_annotated_character_overlaps(lambda c: c.concept_id, a, b)
        self.assertEqual(3, len(overlaps))
        self.assertEqual([x["concept-id"] for x in annotations],
                         [o.value for o in overlaps])

        s = JaccardIndex(0, 0, 0)

        for o in overlaps:
            s = s + o.compute_jaccard_index()

        s2 = JaccardAnalysis(a, b).jaccard_index
            
        self.assertEqual(s, s2)

    def test_path_difference(self):
        self.assertEqual(["a", "b"], ade.path_difference(
            pathlib.Path("/tmp"),
            pathlib.Path("/tmp/a/b")))

    def test_annotated_character(self):
        c = ade.AnnotatedCharacter(3, "the-id", "the-type")
        self.assertEqual("the-id", c.concept_id)
        self.assertEqual(["the-id"], c.get_concept_id_list())
        
        c = ade.AnnotatedCharacter(4, None, "the-type")
        self.assertEqual(None, c.concept_id)
        self.assertEqual([], c.get_concept_id_list())

    def test_dataset_item_reporter(self):
        with tempfile.TemporaryDirectory() as tempdir:
            result_repo = ade.InMemoryResultRepository()
            dataset = sample_repo.full_dataset()
            context = base_context.with_register_dataset(dataset)
            reporter = ade.DatasetItemReporter(dataset, sample_item, result_repo)
            report_butlast(reporter)

            results = context.summarize_results(result_repo)

            rstr = context.results_str(results)
            self.assertTrue(isinstance(rstr, str))
            assert(rstr in context.result_repo_str(result_repo))
            
            self.assertTrue(isinstance(results, ade.ResultSummary))

            self.check_report_rendering(context, result_repo, tempdir)
            
    def test_dataset_reporter(self):
        with tempfile.TemporaryDirectory() as tempdir:
            result_repo = ade.InMemoryResultRepository()
            ds = sample_repo.full_dataset()
            context = base_context.with_register_dataset(ds)

            reporter = ade.DatasetReporter(ds, result_repo)
            item = reporter.dataset[0]
            item_reporter = reporter.for_item(item)
            report_butlast(item_reporter)
            self.check_report_rendering(context, result_repo, tempdir)

    def test_algorithm(self):
        with tempfile.TemporaryDirectory() as tempdir:
            #root_path = pathlib.Path(tempdir)
            result_repo = ade.InMemoryResultRepository()
            ds = sample_repo.full_dataset()
            context = base_context.with_register_dataset(ds)

            reporter = ade.DatasetReporter(ds, result_repo)
            reporter.evaluate_algorithm(CheatingAlgorithm())

            self.check_report_rendering(context, result_repo, tempdir)
            
    def test_evaluate_algorithm(self):
        with tempfile.TemporaryDirectory() as tempdir:
            result_repo = ade.InMemoryResultRepository()
            ds = sample_repo.full_dataset()

            ade.evaluate_algorithm(result_repo, [ds], CheatingAlgorithm())

            context = base_context.with_register_dataset(ds)
            self.check_report_rendering(context, result_repo, tempdir)

            
    def test_reporter(self):
        with tempfile.TemporaryDirectory() as tempdir:
            root_path = pathlib.Path(tempdir)
            result_repo = ade.ResultRepository(root_path)
            ds = sample_repo.full_dataset()
            context = base_context.with_register_dataset(ds)
            item = ds[0]

            reporter = ade.Reporter(result_repo)
            ds_reporter = reporter.for_dataset(ds)
            item_reporter = ds_reporter.for_item(item)
            report_butlast(item_reporter)
            dbg = ade.mkdirp_val(item_reporter.debug_directory())
            self.assertTrue(dbg.exists())

            self.check_report_rendering(context, result_repo, tempdir)

    def test_token_classification_module(self):
        with tempfile.TemporaryDirectory() as tempdir:
            #root_path = pathlib.Path(tempdir)
            result_repo = ade.InMemoryResultRepository()
            
            m = tc.concept_type_token_class_mapper(
                {"occupation-name": "occupation",
                 "esco-occupation": "occupation"})
            token_mod = tc.TokenClassificationReportingModule("skill-occupation", m)

            ds = sample_repo.full_dataset()
            context = base_context.with_set_reporting_modules([token_mod]).with_register_dataset(ds)
            reporter = ade.Reporter(result_repo)
            dsrep = reporter.for_dataset(ds)
            dsrep.evaluate_algorithm(CheatingAlgorithm())

            self.check_report_rendering(context, result_repo, tempdir)
            copy_tree(tempdir, "/tmp/tokencl_test")

    def check_mentor_api_response(self, response):
        self.assertEqual(response.status_code, 200)
        self.assertTrue("annotations" in response.json)
        self.assertTrue(is_nonempty_string(response.json["sha1"]))
        self.assertTrue(is_nonempty_string(response.json["text"]))
        self.assertEqual(dummy_annotations, response.json["annotations"])
            
    def test_http_server(self):
        algo = ade.ConstantAlgorithm({"annotations": dummy_annotations})
        server = ade.MentorApiHttpServer(algo)
        app = server.make_test_app()
        client = app.test_client()
        self.check_mentor_api_response(call_by_method(client, "post", "/nlp/education-description", query_string={"text": "Mjao"}))
        self.check_mentor_api_response(call_by_method(client, "post", "/nlp/annotate-text",
                                                      data=json.dumps({"text": "Mjao"}),
                                                      content_type='application/json'))
                
        for adid in ["26903646", "asdfas"]:
            response = call_by_method(client, "post", "/nlp/job-ad", query_string={"id": adid})
            if response.status_code == 200:
                self.check_mentor_api_response(response)
        


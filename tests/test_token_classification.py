from conrec_utils.annotated_document_evaluation import AnnotatedCharacter
import conrec_utils.token_classification as tc
import unittest

def ac(concept_type):
    return AnnotatedCharacter(-1, None, concept_type)

class TestTokenClassification(unittest.TestCase):
    def test_concept_type_token_class_mapper(self):
        m = tc.concept_type_token_class_mapper(
            {"occupation-name": "occupation",
             "esco-occupation": "occupation"})
        self.assertEqual({"occupation-name", "esco-occupation", "occupation"}, m.source_classes())
        self.assertEqual({"occupation"}, m.destination_classes())
        annotation = ac("occupation-name")
        self.assertEqual("occupation", m.class_from_annotated_character(annotation))

    def test_composite(self):
        m = tc.CompositeTokenClassMapper([
            tc.concept_type_token_class_mapper({"occupation-name": "occupation"}),
            tc.concept_type_token_class_mapper({"esco-occupation": "occupation"})
        ])

        info = m.class_info()
        self.assertEqual(
            info,
            [{'class': 'occupation',
              'info_str': "From concept types 'occupation', 'occupation-name'"}])

        self.assertEqual("occupation", m.class_from_annotated_character(ac("occupation")))
        self.assertEqual("occupation", m.class_from_annotated_character(ac("occupation-name")))
        self.assertEqual("occupation", m.class_from_annotated_character(ac("esco-occupation")))
        
        
        
        
